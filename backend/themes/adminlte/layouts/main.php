<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppLteAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\widgets\Menu;

AppLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>
<div class="wrapper">

    <header class="main-header">
        <a href="<?= Url::to(['/site/index']); ?>" class="logo">
            <span class="logo-mini"><b>TI</b></span>
            <span class="logo-lg"><b>Turbo - Inst</b></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <?php ?>

                    <?php if (!Yii::$app->user->isGuest) { ?>
                        <li class="user user-menu">
                            <a href="<?= Url::to(['site/logout']) ?>" data-method="post">
                                <span class="hidden-xs">Выйти (<?= Yii::$app->user->identity->username ?>)</span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <?php echo Menu::widget([
                'options' => ['class' => 'sidebar-menu'],
                'labelTemplate' => '<p>{label}</p>',
                'encodeLabels' => false,
                'items' => [
                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Статические страницы</span>',
                        'url' => ['static-page/index']
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>FAQ</span>',
                        'url' => ['/faq/index']
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Статистика задач</span>',
                        'url' => ['/task-run/']
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Доступы</span>',
                        'url' => ['/admin']
                    ],
                    [
                        'label' => '<i class="fa fa-files-o"></i><span>Услуги</span>',
                        'url' => ['/service/index'],
                        'options' => ['class' => 'treeview'],
                        'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                        'items' => [
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Услуги',
                                'url' => ['/service/index']
                            ],
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Настройки услуг',
                                'url' => ['/settings']
                            ],
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-files-o"></i><span>Классы и формы</span>',
                        'url' => ['/service/index'],
                        'options' => ['class' => 'treeview'],
                        'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                        'items' => [
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Классы',
                                'url' => ['/classes/index']
                            ],
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Методы класса',
                                'url' => ['/classes-method/index']
                            ],
//                            [
//                                'label' => '<i class="fa fa-circle-o"></i> Критерии',
//                                'url' => ['/criteria/index']
//                            ],
//                            [
//                                'label' => '<i class="fa fa-circle-o"></i> Формы',
//                                'url' => ['/form-criteria/index']
//                            ],
                        ],
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Поддержка</span>',
                        'url' => ['/ticket/index']
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Платежная система</span>',
                        'url' => ['#'],
                        'options' => ['class' => 'treeview'],
                        'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                        'items' => [
                            [
                                'label' =>  '<i class="fa fa-circle-o"></i> Агрегаторы',
                                'url' => ['/billing-operation/agregator']
                            ],
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Настройка бонусов',
                                'url' => ['/bonus-pay']
                            ],
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Пополнение баланса',
                                'url' => ['/billing-operation/pay']
                            ],
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Документы прихода',
                                'url' => ['/billing-operation']
                            ],
                            [
                                'label' => '<i class="fa fa-circle-o"></i> Документы расхода',
                                'url' => ['/billing-operation-sale']
                            ]
                        ]
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Пользователи</span>',
                        'url' => ['/user/index']
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Карусель</span>',
                        'url' => ['/carousel']
                    ],
                    [
                        'label' => '<i class="fa fa-book"></i> <span>Заказы</span>',
                        'url' => ['/order']
                    ]
                   /* [
                        'label' => '<i class="fa fa-book"></i> <span>Купоны</span>',
                        'url' => ['/coupon'
                        ]
                    ],*/
                ],

            ]); ?>

        </section>
    </aside>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                <?= Html::encode($this->title) ?>
            </h1>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>
        <?= Alert::widget(); ?>
        <section class="content">
            <?= $content ?>
        </section>
    </div>
    <footer class="main-footer">
        <strong>Copyright &copy; 2015
    </footer>
</div>
<!-- ./wrapper -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
