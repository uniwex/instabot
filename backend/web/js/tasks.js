/**
 * Created by димон on 01.02.2016.
 */
function openInfo(user_id) {
    var data = {"user_id": user_id};
    $.post('task-run/userinfo', data, function (res) {
        if (res.result != false) {
            swal({
                title: "<small>" + res.result.username + "</small>",
                text: 'email : <span style="color:green">' + res.result.email + '</span><br>' +
                'skype : <span style="color:green">' + res.result.skype + '</span><br>' +
                'icq : <span style="color:green">' + res.result.isq + '</span><br>' +
                'телефон : <span style="color:green">' + res.result.phone + '</span><br>' +
                'зареган : <span style="color:green">' + res.result.created_at + '</span>',
                html: true
            });
        } else {
            swal("Ошибка в получении данных", "", "error");
        }
    }, 'json');
}

function ok(task_id) {
    var data = {"task_id": task_id};
    $.post('task-run/task-ok', data, function (res) {
        if (res.result) {
            $('#status' + task_id).html(res.status);
            swal("Задача одобрена и поставлена на выполнение", "", "success");
        } else {
            swal("Ошибка в получении данных", "", "error");
        }
    }, 'json');
}

function dismiss(task_id) {
    var data = {"task_id": task_id};

    $.post('task-run/task-dismiss', data, function (res) {
        if (res.result) {
            $('#status' + task_id).html(res.status);
            swal("Задача отклонена", "", "success");
        } else {
            swal("Ошибка в получении данных", "", "error");
        }
    }, 'json');
}

function edit(task_id) {
    swal({
            title: "Изменение объекта",
            text: "Новый объект:",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: $('#object' + task_id).html()
        },
        function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Объект не может быть пустым!");
                return false
            }
            var data = {"task_id": task_id, "new_object": inputValue};
            $.post('task-run/change-object', data, function (res) {
                if (res.result) {
                    $('#object' + task_id).html(inputValue);
                    swal("Объект изменен", "Новый объект: " + inputValue, "success");
                } else {
                    swal("Ошибка в получении данных", "", "error");
                }
            }, 'json');
        });
}