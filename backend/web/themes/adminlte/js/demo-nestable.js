// Nestable demo
// -----------------------------------


(function (window, document, $, undefined) {

    $(function () {

        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output'),
                data;
            if (window.JSON) {
                data = window.JSON.stringify(list.nestable('serialize'));
                output.val(data);//, null, 2));
                $.ajax({
                    url: '/backend/web/index.php?r=service-category/edit-tree',
                    data: {
                        json: data
                    }
                })
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        $('#nestable').nestable({
                group: 1
            })
            .on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $('.js-nestable-action').on('click', function (e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

    });

})(window, document, window.jQuery);