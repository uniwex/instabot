/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

  "use strict";

  //bootstrap WYSIHTML5 - text editor
  $(".textarea").wysihtml5();

  $('.help_ex2').popover({'title': 'Информация о профиле', placement: 'auto top',  html: true, container: 'body', content: function ()
  {
    var pid = $(this).data('pid');
    return $("#popover-"+pid).html();
  }});

  $("[data-do=clearDateRange2]").on('click', function()
  {
    var start = moment('2015-11-01').format('YYYY-MM-DD');
    var end  = moment().format('YYYY-MM-DD');

    $("input#dateFrom").val(start);
    $("input#dateTo").val(end);
    $('#reportrange2 span').html(start + ' - ' + end);
    return false;
  });

  $('#reportrange2 span').html(moment("2015-11-01").format('YYYY-MM-DD') + ' - ' + moment("2015-11-09").format('YYYY-MM-DD'));

  $('#reportrange2').daterangepicker({
        opens: 'left',

        startDate: moment("2015-11-01").format('YYYY-MM-DD'),
        endDate: moment("2015-11-09").format('YYYY-MM-DD'),

        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        ranges: {
          'Сегодня': [moment(), moment()],
          'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
          'Текущий месяц': [moment().startOf('month'), moment().endOf('month')],
          'Прошедший месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
          'ВСЕ ВРЕМЯ': [moment("2015-11-01"), moment()],
        },
        buttonClasses: ['btn'],
        applyClass: 'green',
        cancelClass: 'default',
        format: 'YYYY-MM-DD',
        separator: ' до ',
        locale: {
          applyLabel: 'Ок',
          cancelLabel: 'Отменить',
          fromLabel: 'с',
          toLabel: 'до',
          customRangeLabel: 'Выбрать вручную',
          daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
          monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
          firstDay: 1
        }
      },
      function (start, end) {
        console.log(start);
        $("input#filterform-p-post_created-min").val(start.format('YYYY-MM-DD'));
        $("input#filterform-p-post_created-max").val(end.format('YYYY-MM-DD'));
        $('#reportrange2 span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
      }
  );

/*  $('.daterange').daterangepicker({
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment()
  }, function (start, end) {
    window.alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });*/

  //jvectormap data
  var visitorsData = {
    "US": 398, //USA
    "SA": 400, //Saudi Arabia
    "CA": 1000, //Canada
    "DE": 500, //Germany
    "FR": 760, //France
    "CN": 300, //China
    "AU": 700, //Australia
    "BR": 600, //Brazil
    "IN": 800, //India
    "GB": 320, //Great Britain
    "RU": 3000 //Russia
  };
  //World map by jvectormap
  $('#world-map').vectorMap({
    map: 'world_mill_en',
    backgroundColor: "transparent",
    regionStyle: {
      initial: {
        fill: '#e4e4e4',
        "fill-opacity": 1,
        stroke: 'none',
        "stroke-width": 0,
        "stroke-opacity": 1
      }
    },
    series: {
      regions: [{
        values: visitorsData,
        scale: ["#92c1dc", "#ebf4f9"],
        normalizeFunction: 'polynomial'
      }]
    },
    onRegionLabelShow: function (e, el, code) {
      if (typeof visitorsData[code] != "undefined")
        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
    }
  });

  //Sparkline charts
  var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
  $('#sparkline-1').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });
  myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
  $('#sparkline-2').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });
  myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
  $('#sparkline-3').sparkline(myvalues, {
    type: 'line',
    lineColor: '#92c1dc',
    fillColor: "#ebf4f9",
    height: '50',
    width: '80'
  });

  //The Calender
  $("#calendar").datepicker();

  //SLIMSCROLL FOR CHAT WIDGET
  $('#chat-box').slimScroll({
    height: '250px'
  });

  /* The todo list plugin */
  $(".todo-list").todolist({
    onCheck: function (ele) {
      window.console.log("The element has been checked");
      return ele;
    },
    onUncheck: function (ele) {
      window.console.log("The element has been unchecked");
      return ele;
    }
  });

});
