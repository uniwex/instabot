<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class AddUserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $isq;
    public $skype;
    public $phone;
    public $referrer;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данное имя пользователя уже существует.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Данный Email уже зарегистрирован.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['referrer', 'number'],
            [['skype', 'isq', 'phone'], 'safe'],
            ['skype', 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$/ui',
                'message' => 'Имя пользователя Skype должно начинаться с буквы латинского алфавита
                 и содержать от 6 до 32 символов (только буквы латинского алфавита и цифры –
                 пробелы и специальные символы не допускаются)'
            ],
            ['phone', 'match', 'pattern' => '/\d+/ui'],
            ['phone', 'string', 'min' => 10, 'max' => 16],
            ['phone', 'filter', 'filter' => function ($value) {
                return preg_replace('/[^\d]/ui', '', $value);
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'referrer' => 'ID реферала',
            'username' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
            'isq' => 'ICQ',
            'phone' => 'Телефон',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->referrer = $this->referrer;

            $user->phone = $this->phone;
            $user->isq = $this->isq;
            $user->skype = $this->skype;

            $user->setPassword($this->password);
            $user->generateAuthKey();

            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
