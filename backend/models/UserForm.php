<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 *
 * @property User $user
 */
class UserForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $is_baned;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],

            ['password', 'string', 'min' => 6],
            ['is_baned', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Email',
            'is_baned' => 'Забанен'
        ];
    }

    public function setUser($user)
    {
        $this->id = $user->id;
        $this->username = $user->username;
        $this->email = $user->email;
        $this->is_baned = $user->is_baned;
    }


    /**
     * изменение данных пользователя
     * @return null|static
     */
    public function saveUser()
    {
        if ($this->validate()) {
            $user = User::findOne($this->id);
            $user->username = $this->username;
            $user->email = $this->email;
            $user->is_baned = (int) $this->is_baned;

            if ($this->password != '') $user->setPassword($this->password);
            if ($user->save()) return $user; else die(var_dump($user->getErrors()));
        }
        return null;
    }
}
