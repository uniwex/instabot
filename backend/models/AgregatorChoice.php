<?php
/**
 * Created by PhpStorm.
 * User: димон
 * Date: 25.04.2016
 * Time: 11:13
 */

namespace backend\models;


use yii\base\Model;

class AgregatorChoice extends Model
{
    //alias @common
    const STORAGE_FILE = '/config/advanced_options.json';

    public $robokassa;
    public $interkassa;
    public $wallet_one;

    public function rules(){
        return [
            [['robokassa','interkassa','wallet_one'],'required'],
            [['robokassa','interkassa','wallet_one'],'boolean']
        ];
    }

    public function save(){
        file_put_contents(\Yii::getAlias('@common').self::STORAGE_FILE,json_encode($this->attributes));
        return true;
    }

    public function loadSettings(){
        $settings = json_decode(file_get_contents(\Yii::getAlias('@common').self::STORAGE_FILE),1);
        if (isset($settings['robokassa'])) $this->robokassa = $settings['robokassa']; else $this->robokassa = 0;
        if (isset($settings['interkassa'])) $this->interkassa = $settings['interkassa']; else $this->interkassa = 0;
        if (isset($settings['wallet_one'])) $this->wallet_one = $settings['wallet_one']; else $this->wallet_one = 0;
    }
}