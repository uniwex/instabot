<?php

namespace backend\controllers;

use common\helpers\StatusHelper;
use common\models\User;
use mdm\admin\components\AccessControl;
use Yii;
use common\models\TaskRun;
use common\models\TaskRunSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TaskRunController implements the CRUD actions for TaskRun model.
 */
class TaskRunController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public $enableCsrfValidation = false;
    /**
     * Lists all TaskRun models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskRunSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionUserinfo(){
        if (Yii::$app->request->post('user_id')&&Yii::$app->request->isAjax){
            $user = User::find()->where("id=".Yii::$app->request->post('user_id'))->one();
            $return = [];
            $return['id'] = $user->id;
            $return['username'] = $user->username;
            if (isset($user->email))
                $return['email'] = $user->email;
            else
                $return['email'] = "Не указан";
            $return['created_at'] = date('Y-m-d H:i:s',$user->created_at);
            if (isset($user->skype))
                $return['skype'] = $user->skype;
            else
                $return['skype'] = "Не указан";

            if (isset($user->isq))
                $return['isq'] = $user->isq;
            else
                $return['isq'] = "Не указан";
            if (isset($user->phone))
                $return['phone'] = $user->phone;
            else
                $return['phone'] = "Не указан";

            return json_encode(['result'=>$return,'csrf'=>Yii::$app->request->csrfToken]);
        }
        return json_encode(['result'=>false,'csrf'=>Yii::$app->request->csrfToken]);
    }

    public function actionTaskDismiss(){
        if (Yii::$app->request->post('task_id')&&Yii::$app->request->isAjax) {
            $task = TaskRun::find()->where('id='.Yii::$app->request->post('task_id'))->one();
            $task->current_status = StatusHelper::CANCELED;
            $task->save();
            return json_encode(['result'=>true,'status'=>StatusHelper::getDescription(StatusHelper::CANCELED)]);
        } else return json_encode(['result'=>false]);
    }

    public function actionTaskOk(){
        if (Yii::$app->request->post('task_id')&&Yii::$app->request->isAjax) {
            $task = TaskRun::find()->where('id='.Yii::$app->request->post('task_id'))->one();
            $task->current_status = StatusHelper::DECLARED;
            $task->service_error = null;
            $task->error_count = 1;
            $task->save();
            return json_encode(['result'=>true,'status'=>StatusHelper::getDescription(StatusHelper::DECLARED)]);
        }
        return json_encode(['result'=>false]);
    }

    public function actionChangeObject(){
        if (Yii::$app->request->post('task_id')&&Yii::$app->request->isAjax) {
            $task = TaskRun::find()->where('id='.Yii::$app->request->post('task_id'))->one();
            $fields = json_decode($task->fields);
            $fields->field = Yii::$app->request->post('new_object');
            $task->fields = json_encode($fields);
            $task->save();
            return json_encode(['result'=>true]);
        }
        return json_encode(['result'=>false]);
    }

}
