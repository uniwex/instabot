<?php

namespace backend\controllers;

use common\components\UserClosureTable;
use mdm\admin\components\AccessControl;
use Yii;
use common\models\ServiceCategory;
use common\models\ServiceCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ServiceCategoryController implements the CRUD actions for ServiceCategory model.
 */
class ServiceCategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ServiceCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ServiceCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ServiceCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ServiceCategory();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->appendTo($model->new_owner);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $roots = ServiceCategory::find()->roots()->all();
            return $this->render('create', [
                'model' => $model,
                'roots' => $roots,
            ]);
        }
    }

    /**
     * Updates an existing ServiceCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $parent = $model->parent()->count();
            $children = $model->children()->count();

            if($parent)
                $model->moveTo($model->new_owner);
            else if(!$parent && !$children && $model->new_owner != $id && $model->new_owner != 0) {
                $parent = ServiceCategory::findOne($model->new_owner);
                $parent->prependTo($id);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $roots = ServiceCategory::find()->roots()->all();
            return $this->render('update', [
                'model' => $model,
                'roots' => $roots,
            ]);
        }
    }

    /**
     * Deletes an existing ServiceCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Изменение структуры дерева
     * @param $json
     */
    public function actionEditTree($json)
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax) {
            $nameTable = 'common\models\ServiceCategory';
            $json = json_decode($json, 1);
            UserClosureTable::editTree($json, $nameTable);
        }
    }

    /**
     * Изменение данных категории из дерева
     */
    public function actionUpdateTree($id)
    {
        if (!Yii::$app->user->isGuest) {
            $model = $this->findModel($id);
            if (Yii::$app->request->isAjax) {
                $roots = ServiceCategory::find()->roots()->all();
                return $this->renderAjax('/service-category/_form', [
                    'model' => $model,
                    'roots' => $roots,
                ]);
            } else {
                $data = Yii::$app->request->post();
                if(isset($data['ServiceCategory'])) {
                    if ($model->load(Yii::$app->request->post()) && $model->save()) {
                        $parent = $model->parent()->count();
                        $children = $model->children()->count();

                        if($parent) {
                            if($model->new_owner) // Изменяем родителя потомка
                                $model->moveTo($model->new_owner);
                            else // Удаляем потомков с ID модели, т.о. модель становится родителем
                                $model->deleteChildren();
                        }
                        else if(!$parent && !$children && $model->new_owner != $id) {
                            $parent = ServiceCategory::findOne($model->new_owner);
                            $parent->prependTo($id); // Делаем родителя потомком
                        }
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                }
            }
        }
    }

    public function actionDeleteTree($id, $confirm = 0)
    {
        if (!Yii::$app->user->isGuest && Yii::$app->request->isAjax) {
            $model = ServiceCategory::findOne($id);
            if($confirm) {
                $count = $model->children()->count();
                if($count) {
                    $models = $model->children()->all();
                    foreach($models as $category) {
                        $category->delete();
                    }
                }
                $model->delete();
            }
            else {
                return $this->renderAjax('/service-category/_remove', [
                    'model' => $model
                ]);
            }
        }
    }

    /**
     * Finds the ServiceCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ServiceCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ServiceCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
