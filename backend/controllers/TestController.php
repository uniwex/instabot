<?php

namespace backend\controllers;

use Yii;
use common\models\StaticPage;
use common\models\StaticPageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\helpers\SendHelper;

/**
 * StaticPageController implements the CRUD actions for StaticPage model.
 */
class TestController extends Controller
{
    public function behaviors()
    {
        return [
            'eauth' => array(
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => array('add-account'),
            ),
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex() {

    }

    public function actionCreate(){
        return $this->render('create');
    }

    public function actionAddAccount() {

        $serviceName = Yii::$app->getRequest()->getQueryParam('service');

        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            //die(var_dump($eauth));
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('/'));

                $act = "registration";
                if (!Yii::$app->user->getId()<1) {
                $act = "link";
            }
            try {
                if ($eauth->authenticate()) {
                    //die(var_dump($eauth->getAttributes()));
                  /* $sh = new SendHelper();
                    $sh->request('https://oauth.vk.com/authorize?client_id=1&display=page&redirect_uri=http://example.com/callback&scope=friends&response_type=code&v=5.44');
                    die(var_dump($sh->request('https://oauth.vk.com/access_token?client_id=5108346&client_secret=algMufc66DVbA2vBHMq2')));*/

                       // die(var_dump($eauth->getComponent()->));
                   /* if ($act == "registration") {
                        //die($eauth->getServiceName()." +++ ".$eauth->getAttribute('id')." ++ ".$eauth->getAttribute('name'));
                       /* if ($eauth->getServiceName() == 'vkontakte') {
                            $identity = UserHelper::checkVkId($eauth->getAttribute('id'));
                        } elseif ($eauth->getServiceName() == 'facebook') {
                            $identity = UserHelper::checkFbId($eauth->getAttribute('id'));
                        } elseif ($eauth->getServiceName() == 'odnoklassniki') {
                            $identity = UserHelper::checkOkId($eauth->getAttribute('id'));
                        } else {
                            $identity = false;
                        }
                        //die(var_dump($identity));
                        if ($identity) {
                            Yii::$app->getUser()->login($identity);
                            return $this->redirect('/',302);
                            // special redirect with closing popup window
                            // $eauth->redirect();
                        } else {
                            //var_dump($eauth);
                            $id = $eauth->getAttribute('id');
                            $name = $eauth->getAttribute('name');
                            Yii::$app->session->set('nameSocial',$name);
                            Yii::$app->session->set('idSocial',$id);
                            Yii::$app->session->set('serviceName',$eauth->getServiceName());
                            Yii::$app->session->set('Registration','register');
                            return $this->redirect('/',302);
                            //var_dump($eauth->getAttribute('id'));
                            //return $this->redirect('/auth/registersocial', 302);
                        }
                    } elseif ($act=="link"){
                        $result = User::findByEAuthLink($eauth);
                        if ($result == true)
                            return $this->goHome();
                        else
                            echo "Ошибка! Обратитесь в тех. поддержку";
                    }*/
                }
                else {
                    die("AUTORIZE FALSE");
                    //die("autorize false -- ".var_dump($eauth));
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            }
            catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: '.$e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        // default authorization code through login/password ..
    }

}
