<?php
/**
 * Created by PhpStorm.
 * User: димон
 * Date: 01.02.2016
 * Time: 16:39
 */

namespace backend\controllers;

use mdm\admin\components\AccessControl;
use Yii;
use yii\web\Controller;

class InfoRedactorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
        ];
    }

    public function actionIndex() {
        $file = file_get_contents(Yii::getAlias('@common')."\\html_settings.txt");
        $json = json_decode($file);
        $this->render('index',['json'=>$json]);
    }
}