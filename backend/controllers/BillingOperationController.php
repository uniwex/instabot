<?php

namespace backend\controllers;

use backend\models\AgregatorChoice;
use common\models\PartnerConfig;
use common\models\BillingOperationSearch;
use frontend\models\PaymentForm;
use mdm\admin\components\AccessControl;
use Yii;
use common\models\BillingOperation;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\helpers\PaymentTypeHelper;
/**
 * BillingOperationController implements the CRUD actions for BillingOperation model.
 */
class BillingOperationController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BillingOperation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BillingOperationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays a single BillingOperation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BillingOperation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BillingOperation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BillingOperation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BillingOperation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPay() {
        $payment = new PaymentForm();
        $payment->type = PaymentTypeHelper::PAYMENT_TYPE_MANUAL;
        if($payment->load(Yii::$app->request->post())) {
            $operation = new BillingOperation();
            $operation->setOwnerId($payment->userId);
            $operation->operation_sum = $payment->sum;
            $operation->payment_system = $payment->type;
            $operation->saveOperation() ?
                Yii::$app->session->setFlash('status', 'Оплата прошла успешно') :
                Yii::$app->session->setFlash('status', 'Ошибка при проведении платежа');
        }
        return $this->render('payment', ['model' => $payment]);
    }


    public function actionAgregator(){
        $model = new AgregatorChoice();
        if ($model->load(Yii::$app->request->post())){
            $model->save();
        }
        $model->loadSettings();
        return $this->render('agregator_choice',['model'=>$model]);
    }

    public function actionSettings() {
        $settings = PartnerConfig::checkRecord();

        if($settings->load(Yii::$app->request->post())) {
            $settings->save();
        }

        return $this->render('settings', ['model' => $settings]);
    }

    /**
     * Finds the BillingOperation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BillingOperation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BillingOperation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
