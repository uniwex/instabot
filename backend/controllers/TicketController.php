<?php
namespace backend\controllers;

use common\models\TicketMessage;
use DateTime;
use mdm\admin\components\AccessControl;
use Yii;
use common\models\Ticket;
use common\models\TicketSearch;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketController implements the CRUD actions for Ticket model.
 */
class TicketController extends Controller
{
   // public $layout = '@app/themes/theproject/layouts/left_sidebar';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ticket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ticket model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model =  $this->findModel($id);
        $modelMessage = new TicketMessage(['ticket_id'=>$id]);
        if ($modelMessage->load(Yii::$app->request->post()))
        {
            if($modelMessage->save()) {
                $model->message = $modelMessage->message;
                $model->status = Ticket::STATUS_ANSWERED;
                $model->save();
                $modelMessage = new TicketMessage(
                    [
                        'ticket_id' => $id,
                    ]
                );
            }
        }

        $query = TicketMessage::find()->ticket($id)->orderBy('created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'modelMessage' => $modelMessage,
        ]);
    }


    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
