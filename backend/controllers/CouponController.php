<?php

namespace backend\controllers;

use backend\models\CouponSearch;
use common\models\Coupon;
use mdm\admin\components\AccessControl;
use Yii;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * ClassesMethodController implements the CRUD actions for ClassesMethod model.
 */
class CouponController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
            ],
        ];
    }

    public function actionIndex(){
        $create_coupon = new DynamicModel(['count','price']);
        $create_coupon->addRule(['count'],'integer')->addRule(['price'],'number');
        if ($create_coupon->load(Yii::$app->request->post()) && $create_coupon->validate()) {
            for($i = 0; $i<$create_coupon->count;$i++) {
                $coupon = new Coupon();
                $coupon->price = $create_coupon->price;
                $coupon->save();
            }
        }

        $searchModel = new CouponSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'create_coupon' => $create_coupon,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDelete($id)
    {
        Coupon::find()->where(['id'=>$id])->one()->delete();
        return $this->redirect(['index']);
    }

}
