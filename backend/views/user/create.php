<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Добавление пользователя';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <div class="user-form">
        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'referrer') ?>
                <?= $form->field($model, 'phone') ?>
                <?= $form->field($model, 'skype') ?>
                <?= $form->field($model, 'isq') ?>
            </div>
        </div>





        <div class="form-group">
            <?= Html::submitButton('Добавить пользователя', ['class' => 'btn btn-success']) ?>
        </div>


        <?php ActiveForm::end(); ?>
    </div>

</div>
