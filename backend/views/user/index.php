<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'username',
            'balance',
            [
                'label' => 'Приход',
                'format' => 'raw',
                'value' => function($data){
                    return Html::a(
                        $data->debit,
                        ['/billing-operation','BillingOperationSearch[username]'=>$data->id]
                    );
                }
            ],
            [
                'label' => 'Расход',
                'format' => 'raw',
                'value' => function($data){
                    return Html::a(
                        $data->credit,
                        ['/billing-operation-sale','BillingOperationSaleSearch[username]'=>$data->id]
                    );
                }
            ],
            [
                'attribute'=>'created_at',
                'value' => function ($data) {
                    return Date('Y-m-d H:i:s',$data->created_at);
                }
            ],
            'email:email',
            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}'],
        ],
    ]); ?>
</div>
