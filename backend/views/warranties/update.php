<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Warranties */

$this->title = 'изменение: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Гарантии', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'изменение';
?>
<div class="warranties-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
