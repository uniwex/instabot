<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Warranties */

$this->title = 'Добавление гарантии';
$this->params['breadcrumbs'][] = ['label' => 'Гарантии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="warranties-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
