<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\WarrantiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Гарантии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="warranties-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить гарантию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'sort',
            [
                'class' => \yii\grid\DataColumn::className(),
                'attribute' => 'active',
                'format' => 'raw',
                'value' => function ($model, $index, $widget) {
                    return Html::checkbox('active[]', $model->active, ['value' => $index, 'disabled' => true]);
                },
            ],
            [
                'label' => 'Картинка',
                'format' => 'raw',
                'value' => function($data){
                    return Html::img($data->getImageFileUrl('image'),[
                        'alt'=>'',
                        'style' => 'width:100px;'
                    ]);
                },
            ],

            'description_ru:ntext',
            'button_link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
