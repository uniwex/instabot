<?php

use common\models\NewsCategories;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $categories NewsCategories */

$this->title = 'Create News';
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
    ]) ?>

</div>
