<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title_ru;
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить данную новость?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo Html::img($model->getImageFileUrl('image'),[ 'class'=>"img-responsive"]); ?>
    <?php echo $model->description_ru; ?>
    <?php echo $model->content_ru; ?>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_ru',
            'slug',
            'createdBy.username',
            'updatedBy.username',
            'created_at',
            'updated_at',
            'active',
        ],
    ]) ?>

</div>
