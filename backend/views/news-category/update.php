<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NewsCategories */

$this->title = 'изменение: ' . ' ' . $model->title_ru;
$this->params['breadcrumbs'][] = ['label' => 'Категории новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title_ru, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'изменение';
?>
<div class="news-categories-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
