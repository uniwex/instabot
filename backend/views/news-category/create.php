<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\NewsCategories */

$this->title = 'Добавление категории';
$this->params['breadcrumbs'][] = ['label' => 'Категории новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-categories-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
