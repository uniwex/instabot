<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NewsCategories */

$this->title = $model->title_ru;
$this->params['breadcrumbs'][] = ['label' => 'Категории новостей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-categories-view">

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите удалить данную категорию?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php echo Html::img($model->getImageFileUrl('image'),[ 'class'=>"img-responsive"]) ?>

    <?php echo $model->description_ru ?>



    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title_ru',
            'slug',
            //'description_ru:ntext',
            //'image',
            'created_by',
            'updated_by',
            'created_at',
            'updated_at',
            'active',
        ],
    ]) ?>

</div>
