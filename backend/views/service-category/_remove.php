<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceCategory */
/* @var $form yii\widgets\ActiveForm */
/* @var $roots common\models\ServiceCategory[] */
?>

    <div class="service-category-form">


        <div class="error-content">
            <h4><i class="fa fa-warning text-red"></i> Вы действительно хотите удалить "<?= $model->title_ru ?>"?</h4>
            <? $children = $model->children()->count(); ?>
            <? if($children) { ?>
                <? $name = 'Удалить, включая подкатегории'; ?>
            <p>
                При этом действии будут удалены все подкатегории.
            </p>
            <? } else $name = 'Удалить'; ?>
            <button type="submit"
                    name="deleteTree"
                    data-id="<?= $model->id ?>"
                    class="btn btn-danger btn-flat"
                    data-dismiss="modal">
                <?= $name ?>
            </button>
        </div>


    </div>

<?
$script = <<<JS
$('button[name="deleteTree"]').on('click', function(){
    var id = $(this).attr('data-id');
    $.ajax({
        url: '/admin/index.php?r=service-category%2Fdelete-tree',
        data: { id: id, confirm: 1 },
        success: function() {
            $('[class^=dd][data-id="' + id + '"]').remove();
        }
    })
});
JS;
$this->registerJs($script);
?>