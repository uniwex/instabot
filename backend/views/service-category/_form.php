<?php

use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceCategory */
/* @var $form yii\widgets\ActiveForm */
/* @var $roots common\models\ServiceCategory[] */
?>

<div class="service-category-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
    <?= $form->field($model, 'id', ['template' => '{input}'])->hiddenInput() ?>
    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?php
    $arrRoots = ArrayHelper::map($roots, 'id', 'title_ru');
    $arrRoots[0] = 'Корневой каталог';
    ?>

    <?= $form->field($model, 'new_owner')->widget(Select2::classname(), [
        'data' => $arrRoots,
        'options' => ['placeholder' => 'Выберите категории для данной новости'],
        'pluginOptions' => [
            'allowClear' => true,
            //'multiple' => true,
        ],
    ]);?>

    <?php $imageUrl = $model->getImageFileUrl('image'); ?>
    <?= $form->field($model, 'image')->
    widget(FileInput::classname(), [
            'name' => 'image',
            'pluginOptions' => [
                'initialPreview' => $imageUrl ? [Html::img($imageUrl, ['class' => 'file-preview-image']),] : [],
                'showCaption' => false,
                'showRemove' => false,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' => 'Изображение'
            ],
            'options' => ['accept' => 'image/*']
        ]);
    ?>

    <?= $form->field($model, 'description_ru')->widget(TinyMce::className(), [
        'options' => ['rows' => 6],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                "advlist autolink lists link charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]); ?>

    <?= $form->field($model, 'sort')->textInput(['value'=> 100]) ?>

    <?php echo $form->field($model, 'active')->widget(SwitchInput::classname(), []); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
