<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\StatusHelper;
use common\models\Service;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TaskRunSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerJsFile('@web/js/tasks.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/sweetalert.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('@web/css/sweetalert.css');

$this->title = 'Статистика задач';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-run-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?
    $statusSelect[''] = 'Не выбрано';
    foreach (StatusHelper::$labels as $key => $label) {
        $statusSelect[$key] = $label;
    }
    $services[''] = 'Не выбрано';
    $serv = ArrayHelper::map(Service::find()->asArray()->all(), 'id', 'title_ru');
    foreach ($serv as $id => $service) {
        $services[$id] = $service;
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'emptyText' => "Ничего не найдено",
        'columns' => [
            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::tag('i', '', [
                            'class' => 'fa fa-pencil-square-o',
                            'onclick' => "edit($model->id)",
                            'style' => 'color:blue;cursor:pointer'
                        ]);
                    },
                  /*  'delete' => function ($url, $model) {
                        return Html::tag('i', '', [
                            'class' => 'fa fa-thumbs-o-down',
                            'onclick' => "dismiss($model->id)",
                            'style' => 'color:red;cursor:pointer'
                        ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::tag('i', '', [
                            'class' => 'fa fa-thumbs-o-up',
                            'onclick' => "ok($model->id)",
                            'style' => 'color:green;cursor:pointer'
                        ]);
                    }*/

                ],
                'template' => '{update}' /*{delete} {view}*/,
            ],
            'id',
            [
                'attribute' => 'task_id',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->task_id == 0) {
                        return $model->id;
                    } else {
                        return $model->task_id;
                    }
                }
            ],
            [
                'attribute' => 'user.username',
                'filter' => Html::activeTextInput($searchModel, 'username', ['class' => 'form-control']),
                'label' => 'Имя пользователя'
               /* 'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(
                        $model->user->username,
                        ['/task-run', 'UserSearch[id]' => $model->user_id],
                        ['target' => '_blank']
                    );
                }*/
            ],
            [
                'attribute' => 'service_id',
                'filter' => Html::activeDropDownList($searchModel, 'service_id', $services, ['class' => 'form-control']),
                'value' => function ($model) {
                    return $model->service->title_ru;
                }
            ],
            [
                'label' => 'Количество',
                'value' => function ($model) {
                    $fields = json_decode($model->fields);
                    return $fields->count;
                }
            ],
            [
                'label' => 'Объект',
                'content' => function ($model) {
                    $fields = json_decode($model->fields);
                    return '<a href='.$fields->field.' target="_blank">'.$fields->field.'</a>';
                }
            ],
            [
                'label' => 'Цена',
                'value' => function ($model) {
                    $fields = json_decode($model->fields);
                    return $fields->summary_price;
                }
            ],
            [
                'label' => 'Было-стало',
                'value' => function ($model) {
                    //$fields = json_decode($model->fields, 1);
                    $fields = json_decode($model->task_info);
                    if((isset($fields->order_begin))&&($fields->order_end !='null'))
                    return $fields->order_begin.' - '.$fields->order_end;
                }
            ],
       /*     [
                'label' => 'Прогресс',
                'content' => function ($model) {
                    $jsonTaskInfo = json_decode($model->task_info);
                    $jsonFields = json_decode($model->fields);
                    if (isset($jsonTaskInfo->complete)) {
                        if ($jsonTaskInfo->complete == 0)
                            $percent = 0;
                        else
                            $percent = (100 * $jsonTaskInfo->complete) / $jsonFields->count;
                    } else $percent = 0;
                    return '<div class="progress">
                              <div class="progress-bar" role="progressbar" style="width:' . $percent . '%">
                                <span class="sr-only">' . $percent . '% Complete</span>
                              </div>
                            </div><span>'.$jsonTaskInfo->complete.' из '.$jsonFields->count.'</span>';
                }
            ],*/
            [
                'attribute' => 'current_status',
                'filter' => Html::activeDropDownList($searchModel, 'current_status', $statusSelect, ['class' => 'form-control']),
                'content' => function ($model) {
                    return "<span id='status" . $model->id . "'>" . StatusHelper::getDescription($model->current_status) . "</span>";
                }
            ],
            [
                //todo Жесткий костыль
                'attribute'=>'created_at',
                'filter'=> '',
                'value'=>function($model){
                    return Date('Y-m-d H:i:s',strtotime($model->created_at)+(3600*3));
                }
            ],
            'agregator_id',
            // 'current_status',
            // 'task_info',
            // 'created_at',
            // 'updated_at',
            // 'email:email',
            // 'last_run',
            // 'subscribe_info',
            // 'error_count',
            // 'subscribe_flag',
            // 'service_error:ntext',

        ],
    ]); ?>
</div>
