<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TaskRunSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-run-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'service_id') ?>



    <?php // echo $form->field($model, 'current_status') ?>

    <?php // echo $form->field($model, 'task_info') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'last_run') ?>

    <?php // echo $form->field($model, 'subscribe_info') ?>

    <?php // echo $form->field($model, 'error_count') ?>

    <?php // echo $form->field($model, 'subscribe_flag') ?>

    <?php // echo $form->field($model, 'task_id') ?>

    <?php // echo $form->field($model, 'service_error') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
