<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ClassesMethod */

$this->title = 'Create Classes Method';
$this->params['breadcrumbs'][] = ['label' => 'Classes Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-method-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes
    ]) ?>

</div>
