<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ClassesMethod */

$this->title = 'Update Classes Method: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Classes Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="classes-method-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes
    ]) ?>

</div>
