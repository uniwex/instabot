<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\ClassesMethod */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classes-method-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'id_class')->dropDownList(ArrayHelper::map($classes, 'id', 'title_ru')); ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'method')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
