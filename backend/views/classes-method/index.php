<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ClassesMethodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Методы классов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-method-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить метод класса', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_class',
            'title_ru',
            'method',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
