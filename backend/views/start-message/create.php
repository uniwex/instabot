<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StartMessage */

$this->title = 'Create Start Message';
$this->params['breadcrumbs'][] = ['label' => 'Start Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="start-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
