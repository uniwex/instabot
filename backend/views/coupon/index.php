<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\Coupon */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $create_coupon */

$this->title = 'Купоны';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="classes-method-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($create_coupon, 'count')->input('number', ['min' => 0, 'step' => 1]) ?>

    <?= $form->field($create_coupon, 'price')->input('number', ['min' => 0, 'step' => 0.1]) ?>

    <div class="form-group">
        <?= Html::submitButton('Создать купоны', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'coupon',
            'price',
            'used',
            [
                'class' => \yii\grid\ActionColumn::className(),
                'template'=> '{delete}'
            ]
        ],
    ]); ?>
</div>
