<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ServiceQueue */

$this->title = 'Create Service Queue';
$this->params['breadcrumbs'][] = ['label' => 'Service Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-queue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'services' => $services,
        'model' => $model
    ]) ?>

</div>
