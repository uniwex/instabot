<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ServiceQueue */

$this->title = 'Update Service Queue: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Service Queues', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="service-queue-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'services' => $services,
        'model' => $model
    ]) ?>

</div>
