<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FormCriteria */

$this->title = 'Create Form Criteria';
$this->params['breadcrumbs'][] = ['label' => 'Form Criterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-criteria-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'methods' => $methods,
        'criterias' => $criterias
    ]) ?>

</div>
