<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\FormCriteria */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-criteria-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_method')->dropDownList(ArrayHelper::map($methods,'id','title_ru')) ?>

    <?= $form->field($model, 'id_criteria')->dropDownList(ArrayHelper::map($criterias,'id','title_ru')) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
