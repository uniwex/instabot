<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Service;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'default_bonus')->input('number', ['step' => 0.1, 'min' => 0]) ?>

    <?/*= $form->field($model, 'premium_bonus')->input('number', ['step' => 0.1, 'min' => 0]) */?>

    <?= $form->field($model, 'premium_photo')->input('number', ['step' => 1, 'min' => 0]) ?>

    <?= $form->field($model, 'premium_followers')->input('number', ['step' => 1, 'min' => 0]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
