<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StaticPage */

$this->title = 'изменение: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Статические страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'изменение';
?>
<div class="static-page-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
