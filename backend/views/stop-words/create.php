<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StopWords */

$this->title = 'Create Stop Words';
$this->params['breadcrumbs'][] = ['label' => 'Stop Words', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stop-words-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
