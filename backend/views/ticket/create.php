<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = 'Создание тикета';
$this->params['breadcrumbs'][] = ['label' => 'Кабинет', 'url' => ['/personal/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Помощь и поддержка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
