<?php

use common\models\Ticket;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Помощь и поддержка';
$this->params['breadcrumbs'][] = ['label' => 'Кабинет', 'url' => ['/personal/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'format' => 'raw',
                /** @var $data  Ticket*/
                'value' => function($data){
                    $iconColor = 'text-gray';
                    switch ($data->status){
                        case Ticket::STATUS_WAITING:
                            $iconColor = 'text-red';
                            break;
                        case Ticket::STATUS_ANSWERED:
                            $iconColor = 'text-green';
                            break;
                        case Ticket::STATUS_READ:
                            $iconColor = 'text-green';
                            break;
                        default:
                            break;
                    };
                    return "<i class='fa fa-circle $iconColor'></i>";
                },
            ],
            'title',
            'createdBy.username',
            'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

</div>
