<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Кабинет', 'url' => ['/personal/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Помощь и поддержка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-view">

    <?= $this->render('_form_add_message',[
        'modelMessage' => $modelMessage,
    ]) ?>

    <?php Pjax::begin(['id' => 'notes']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'message:ntext',
            'createdBy.username',
            'created_at',
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
