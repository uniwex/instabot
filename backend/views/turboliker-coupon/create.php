<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TurbolikerCoupon */

$this->title = 'Create Turboliker Coupon';
$this->params['breadcrumbs'][] = ['label' => 'Turboliker Coupons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="turboliker-coupon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
