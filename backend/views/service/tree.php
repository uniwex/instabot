<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 10.12.2015
 * Time: 15:56
 */

namespace common\components;

use common\components\TreeWidget\TreeWidget;

?>

<div class="row">
    <div class="col-md-6">
        <?= TreeWidget::widget([
            'tableCategory' => '\common\models\ServiceCategory',
            'pk' => 'id',
            'title' => 'title_ru'
        ]); ?>
    </div>
</div>

