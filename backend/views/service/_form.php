<?php

use yii\helpers\Url;
use dosamigos\tinymce\TinyMce;
use kartik\file\FileInput;
use kartik\select2\Select2;
use kartik\switchinput\SwitchInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $form yii\widgets\ActiveForm */
/* @var $categories common\models\ServiceCategory[] */
/* @var $classes common\models\Classes */
/* @var $news common\models\News */

?>

<? $form = ActiveForm::begin([
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>
<? $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'id_class')->dropDownList(ArrayHelper::map($classes, 'id', 'title_ru'), ['id' => 'id_class']); ?>

<?= $form->field($model, 'id_method')->widget(DepDrop::classname(), [
    'options' => ['id' => 'subcat-id'],
    'pluginOptions' => [
        'depends' => ['id_class'],
        'placeholder' => 'Выберите метод',
        'url' => Url::to(['/service/method'])
    ]
]); ?>

<? $imageUrl = $model->getImageFileUrl('image'); ?>
<?= $form->field($model, 'image')->widget(FileInput::classname(), [
    'name' => 'image',
    'pluginOptions' => [
        'initialPreview' => $imageUrl ? [Html::img($imageUrl, ['class' => 'file-preview-image'])] : [],
        'showCaption' => false,
        'showRemove' => false,
        'showUpload' => false,
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' => 'Изображение'
    ],
    'options' => ['accept' => 'image/*']
]);
?>

<?= $form->field($model, 'description_ru')->widget(TinyMce::className(), [
    'options' => ['rows' => 6],
    'language' => 'ru',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
]); ?>

<?/*= $form->field($model, 'field_name')->textInput() */?>

<?= $form->field($model, 'regular_for_field')->textInput()?>

<?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'price_unreg')->textInput(['maxlength' => true]) ?>

<?/*= $form->field($model, 'criteria_price')->textInput(['maxlength' => true])*/ ?>

<?/*= $form->field($model, 'type_debit')->dropDownList([0 => 'Списывать сразу', 1 => 'Списывать в ходе выполнения'])*/ ?>

<?= $form->field($model, 'min_count')->textInput() ?>

<?/*= $form->field($model, 'min_members_count')->textInput() */?>

<?/*= $form->field($model, 'min_record_count')->textInput()*/ ?>

<?/*= $form->field($model, 'stop_words')->widget(SwitchInput::classname(), []);*/ ?>

<?/*= $form->field($model, 'sup_field')->widget(SwitchInput::classname(), []); */?>

<?/*= $form->field($model, 'subscribe')->widget(SwitchInput::classname(), []); */?>

<?/*= $form->field($model, 'type_subscribe')->dropDownList([
    0 => 'Раз в час',
    1 => 'Раз в день',
    2 => 'Раз в неделю',
    3 => 'Раз в месяц'
]) */?>

<?/*= $form->field($model, 'only_logged')->widget(SwitchInput::classname(), []); */?>

<?= $form->field($model, 'sort')->textInput() ?>

<?= $form->field($model, 'active')->widget(SwitchInput::classname(), []); ?>

<?= $form->field($model, 'is_hit')->widget(SwitchInput::classname(), []); ?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<? ActiveForm::end(); ?>


