<?php

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $categories common\models\ServiceCategory[] */
/* @var $classes common\models\Classes */
/* @var $news common\models\News */

$this->title = 'Создание услуги';
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="service-create">

    <?= $this->render('_form', [
        'model' => $model,
        'classes' => $classes
    ]) ?>

</div>
