<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $categories common\models\ServiceCategory[] */
/* @var $classes common\models\Classes*/
/* @var $news common\models\News*/

$this->title = 'Изменение услуги: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'изменение';
?>
<div class="service-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'classes'=>$classes,
        'news' => $news
    ]) ?>

</div>
