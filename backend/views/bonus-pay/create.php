<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BonusPay */

$this->title = 'Create Bonus Pay';
$this->params['breadcrumbs'][] = ['label' => 'Bonus Pays', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-pay-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
