<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BonusPay */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-pay-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sum')->input('number', ['min' => 0, 'step' => 0.1]) ?>

    <?= $form->field($model, 'percent')->input('number', ['min' => 0, 'step' => 0.1]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
