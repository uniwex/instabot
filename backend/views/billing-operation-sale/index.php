<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\BillingOperationSaleSearch */

$this->title = 'Документа расхода';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-operation-sale-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?//= Html::a('Create Billing Operation Sale', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'operation_time',
            [
                'attribute' => 'username',
                'label' => 'Пользователь',
                'format' => 'text',
                'content' => function ($data) {
                    return $data->username;
                },
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username')
            ],
            [
                'attribute' => 'account_id',
                'label' => 'Счет',
                'format' => 'text',
                'content' => function ($data) {
                    return $data->currencyId;
                },
            ],
            'operation_sum',
            'notes:ntext',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>

<?
$script = <<< JS
    $('input[name="BillingOperationSaleSearch[operation_time]"]').daterangepicker();
JS;
$this->registerJs($script);
?>
