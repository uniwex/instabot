<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BillingOperationSale */

$this->title = 'Create Billing Operation Sale';
$this->params['breadcrumbs'][] = ['label' => 'Billing Operation Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-operation-sale-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
