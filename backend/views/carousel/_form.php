<?php

use common\models\Classes;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\ArrayHelper;
use common\models\ClassesMethod;

/* @var $this yii\web\View */
/* @var $model common\models\Carousel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="carousel-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>

    <? $imageUrl = $model->getImageFileUrl('picture'); ?>
    <?= $form->field($model, 'picture')->
    widget(FileInput::classname(), [
        'name' => 'image',
        'pluginOptions' => [
            'initialPreview' => $imageUrl ? [Html::img($imageUrl, ['class' => 'file-preview-image']),] : [],
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' => 'Изображение'
        ],
        'options' => ['accept' => 'image/*']
    ]);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'button_text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'button_class')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'button_href')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'button_service')->dropDownList(ArrayHelper::map(\common\models\Service::find()->all(), 'id', 'title_ru')) ?>

    <?= $form->field($model, 'actually')->input('date')?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'sort')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
