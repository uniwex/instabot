<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TurbolikerAccount */

$this->title = 'Create Turboliker Account';
$this->params['breadcrumbs'][] = ['label' => 'Turboliker Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="turboliker-account-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
