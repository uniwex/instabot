<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Criteria;

/* @var $this yii\web\View */
/* @var $model common\models\Criteria */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="criteria-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList(Criteria::All_Criterias()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
