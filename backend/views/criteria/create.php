<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Criteria */

$this->title = 'Create Criteria';
$this->params['breadcrumbs'][] = ['label' => 'Criterias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="criteria-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
