<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\BillingOperation */

$this->title = 'Create Billing Operation';
$this->params['breadcrumbs'][] = ['label' => 'Billing Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-operation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
