<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\helpers\PaymentTypeHelper;

//use kartik\date\DatePicker;
//use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel common\models\BillingOperationSearch */
/* @var $paymentSystem array */

$this->title = 'Документы прихода';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="billing-operation-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Пополнить счет', ['pay'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'agregator_id',
            'operation_time',
            [
                'attribute' => 'username',
                'label' => 'Пользователь',
                'format' => 'text',
                'content' => function ($data) {
                    return $data->username;
                },
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username')
            ],
            [
                'attribute' => 'account_id',
                'label' => 'Счет',
                'format' => 'text',
                'content' => function ($data) {
                    return $data->currencyId;
                },
            ],
            'operation_sum',
            [
                'attribute' => 'payment_system',
                'content' => function ($data) {
                    return PaymentTypeHelper::getPaymentType()[$data->payment_system];
                },
                'filter' => PaymentTypeHelper::getPaymentType()
            ],
            'notes',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?
$script = <<< JS
    $('input[name="BillingOperationSearch[operation_time]"]').daterangepicker();
JS;
$this->registerJs($script);
?>
