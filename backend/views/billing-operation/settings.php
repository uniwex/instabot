<?
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PartnerConfig*/
/* @var $form ActiveForm */

$status = null;
$this->title = 'Настройка отчислений ПП';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="backend-views-BillingOperation-settings">
    <h1><?= Html::encode($this->title) ?></h1>
    <? $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'level1')->input('number', ['step' => 0.1]) ?>
        <?= $form->field($model, 'level2')->input('number', ['step' => 0.1]) ?>
        <?= $form->field($model, 'level3')->input('number', ['step' => 0.1]) ?>
        <?= $form->field($model, 'level4')->input('number', ['step' => 0.1]) ?>
        <?= $form->field($model, 'level5')->input('number', ['step' => 0.1]) ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <? ActiveForm::end(); ?>

</div><!-- backend-views-BillingOperation-settings -->
