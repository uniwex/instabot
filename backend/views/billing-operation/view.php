<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\BillingOperation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Billing Operations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-operation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'account_id',
            'operation_sum',
            'operation_time',
            'payment_system',
            'author_id',
            'notes:ntext',
        ],
    ]) ?>

</div>
