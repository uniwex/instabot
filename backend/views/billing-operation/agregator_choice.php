<div class="form">

    <?php use yii\bootstrap\ActiveForm;
    use yii\bootstrap\Html;


    $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'robokassa')->checkbox() ?>
<?= $form->field($model, 'interkassa')->checkbox() ?>
<?= $form->field($model, 'wallet_one')->checkbox() ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>

</div><!-- form -->