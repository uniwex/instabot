<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 12.01.2016
 * Time: 15:19
 */
use \yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\models\PaymentForm;
use \yii\helpers\ArrayHelper;
use common\models\User;

//use common\models\BillingOperation;
/* @var $model PaymentForm */

$status = null;
$this->title = 'Пополнение баланса';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="billing-operation-payment">

    <h1><?= Html::encode($this->title) ?></h1>

    <? $form = ActiveForm::begin(); ?>
    <? Yii::$app->session->hasFlash('status') ? $status = Yii::$app->session->getFlash('status') : $status = null; ?>
    <?= $status ?>
    <?= $form->field($model, 'sum')->input('number', ['min' => 1]) ?>
    <?= $form->field($model, 'userId')->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'username')) ?>

    <?= Html::submitButton('Пополнить', ['class' => 'btn btn-primary']) ?>

    <? ActiveForm::end(); ?>
</div>
