<?php
/**
 * Created by PhpStorm.
 * User: димон
 * Date: 27.01.2016
 * Time: 10:55
 */

namespace console\controllers;

use common\helpers\PaymentTypeHelper;
use common\models\ServiceQueue;
use yii\console\Controller;
use common\models\TaskRun;
use common\models\RunModel;
use common\models\BillingOperationSale;
use common\helpers\StatusHelper;
use common\models\servicemodels\BaseRunClass;

class PeriodController extends Controller
{
    public static function actionSupertest($id_task)
    {
        $task = TaskRun::find()->where('id=' . $id_task)->one();
        $class = $task->service->class->runClass;
        $runMethod = $task->service->method->method;
        if ($class->runMethod($runMethod, $task)) {
            $task->current_status = StatusHelper::AT_WORK;
            $task->error_count = 0;
            $task->save();
        } else {
            $task->error_count = $task->error_count + 1;
            if ($task->error_count > 10) {
                if ($task->user_id != 1) {
                    $fields = json_decode($task->fields);
                    $sum = $fields->summary_price;
                    $operation = new BillingOperation();
                    $operation->setOwnerId($task->user_id);
                    $operation->operation_sum = $sum;
                    $operation->payment_system = PaymentTypeHelper::PAYMENT_TYPE_MANUAL;
                    if ($operation->saveOperation()) {
                        $task->delete();
                    }
                } else {
                    $task->current_status = StatusHelper::MODERATED;
                }
            } else {
                $task->current_status = StatusHelper::ERROR;
            }
            $task->save();
        }
    }


    public function actionSubscribeTask()
    {
        $tasks = TaskRun::find()->where(['current_status' => StatusHelper::ERROR])->all();
        foreach ($tasks as $task) {
            if ($task->error_count != 0) {
                exec("php -f /var/www/insta/data/www/turbo-inst.ru/yii period/supertest " . $task->id . "");
            }
        }
    }
}