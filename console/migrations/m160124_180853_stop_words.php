<?php

use yii\db\Migration;

class m160124_180853_stop_words extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%stop_words}}", [
            'id' => $this->primaryKey(),
            'word' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable("{{%stop_words}}");
    }
}
