<?php

use yii\db\Migration;
use common\models\User;

class m160123_170812_create_record_guest extends Migration
{
    public function up()
    {
        $user = new User();
        $user->setPassword('RJ1iY1ywUY');
        $user->generateAuthKey();

        $this->insert('{{%user}}', [
            'username' => 'Guest',
            'email' => 'guest@smm-lab.xyz',
            'password_hash' => $user->password_hash,
            'auth_key' => $user->auth_key
        ]);

        $this->insert('{{%billing_account}}', [
            'account_type' => Yii::$app->params['account_type'],
            'owner_type' => $user::className(),
            'owner_id' => User::findByUsername('Guest')->id
        ]);

        $this->addColumn('{{%billing_operation_sale}}', 'email', $this->string());
        $this->addColumn('{{%billing_operation_sale}}', 'token', $this->string());
    }

    public function down()
    {
        return;
    }
}
