<?php

use yii\db\Migration;

class m160130_133611_average_statitistic extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%average_statistic}}", [
            'id' => $this->primaryKey(),
            'unique' => $this->integer(),
            'new_users' => $this->integer(),
            'debit_users' => $this->decimal(10, 2)->notNull()->defaultValue('0.00'),
            'debit_guest' => $this->decimal(10, 2)->notNull()->defaultValue('0.00'),
            'total_debit' => $this->decimal(10, 2)->notNull()->defaultValue('0.00'),
            'statistic_date' => $this->date()
        ], $tableOptions);

        $this->createTable("{{%day_ip}}",[
            'id' => $this->primaryKey(),
            'ip' => $this->string()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%average_statistic}}");
        $this->dropTable("{{%day_ip}}");
    }
}
