<?php

use yii\db\Migration;

class m160113_192454_tasks extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        /*  $this->createTable('{{%service}}', [
              'id' => $this->primaryKey(),
              'run_class_name' => $this->string()->notNull(),
              'name' => $this->string()->notNull(),
          ], $tableOptions);
  */
        $this->createTable('{{%task_run}}', [
            'id'=> $this->primaryKey(),
            'user_id' => $this->integer(),
            'service_id' => $this->integer(),
            'fields' => $this->string(),
            'run_time' => $this->dateTime(),
            'current_status' => $this->string(),
            'process_id' => $this->string(255),
            'active' => $this->integer()->defaultValue(1)
        ], $tableOptions);

    }

    public function down()
    {
        /* $this->dropTable("{{%service}}");*/
        $this->dropTable("{{%task_run}}");
    }
}
