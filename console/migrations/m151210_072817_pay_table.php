<?php

use yii\db\Schema;
use yii\db\Migration;

class m151210_072817_pay_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        //счета пользователя
        $this->createTable('{{%user_account}}', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer()->notNull(),

            'transaction_type' => $this->string(255)->notNull(),
            'transaction_id' => $this->integer()->notNull(),

            'debit' => $this->money()->notNull(),//увеличение счета
            'credit' => $this->money()->notNull(),//уменьшение счета

            'active' => $this->boolean()->defaultValue(true),
        ], $tableOptions);

        //пополнение
        $this->createTable('{{%replenish}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),

            'owner_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp(),
            'amount' => $this->money()->notNull(),

            'payment_system' => $this->integer()->notNull(),

            'active' => $this->boolean()->defaultValue(true),
        ], $tableOptions);

        //заказ
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),

            'owner_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp(),
            'amount' => $this->money()->notNull(),

            'active' => $this->boolean()->defaultValue(true),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%user_account}}");
        $this->dropTable("{{%replenish}}");
        $this->dropTable("{{%order}}");
    }
}
