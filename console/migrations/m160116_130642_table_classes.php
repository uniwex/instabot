<?php

use yii\db\Migration;

class m160116_130642_table_classes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%classes}}", [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),
            'path' => $this->string(255)
        ], $tableOptions);

        $this->insert('{{%classes}}', [
            'title_ru' => 'Вручную',
        ]);
    }

    public function down()
    {
        $this->dropTable("{{%classes}}");
    }
}
