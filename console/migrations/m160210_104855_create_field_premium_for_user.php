<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_104855_create_field_premium_for_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'is_premium', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'is_premium');
    }
}
