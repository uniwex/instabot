<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_100432_create_table_billing_currency extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%billing_currency}}', [
            'id' => $this->string(3)->notNull(),
            'amount_cnt' => $this->integer()->notNull()->defaultValue(1),
            'amount' => $this->decimal(18, 4)->notNull()->defaultValue(1.0000),
            'current_base_rate' => $this->decimal(26, 12)->notNull()->defaultValue(1.000000000000),
            'base' => $this->boolean()->notNull()->defaultValue(false),
            'created' => $this->timestamp(),
            'updated' => $this->timestamp(),
            'created_by' => $this->integer()->notNull()->defaultValue(0),
            'modified_by' => $this->integer(),
            'sort' => $this->integer()->notNull()->defaultValue(0),
            'PRIMARY KEY `id` (`id`)',
        ], $tableOptions);

        $this->insert('{{%billing_currency}}', [
            'id' => 'RUB',
            'base' => true,
            'created_by' => 0,
            'modified_by' => 0
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%billing_currency}}');
    }
}
