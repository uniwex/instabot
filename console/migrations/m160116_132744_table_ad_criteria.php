<?php

use yii\db\Migration;

class m160116_132744_table_ad_criteria extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%form_criteria}}", [
            'id' => $this->primaryKey(),
            'id_method' => $this->integer(),
            'id_criteria' => $this->integer()
        ], $tableOptions);

        $this->addForeignKey("FK_criteria_child_form_criteria",
            "{{%form_criteria}}", "id_criteria",
            "{{%criteria}}", "id",
            "CASCADE"
        );

        $this->addForeignKey("FK_classes_method_child_form_criteria",
            "{{%form_criteria}}", "id_method",
            "{{%classes_method}}", "id",
            "CASCADE"
        );
    }

    public function down()
    {
        $this->dropForeignKey("FK_criteria_child_form_criteria", "{{%form_criteria}}");
        $this->dropForeignKey("FK_classes_method_child_form_criteria", "{{%form_criteria}}");
        $this->dropTable("{{%form_criteria}}");
    }
}
