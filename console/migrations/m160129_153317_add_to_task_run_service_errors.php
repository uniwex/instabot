<?php

use yii\db\Migration;

class m160129_153317_add_to_task_run_service_errors extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}','service_error',$this->text());
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}','service_error');
    }
}
