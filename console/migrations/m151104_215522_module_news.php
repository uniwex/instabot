<?php

use yii\db\Schema;
use yii\db\Migration;

class m151104_215522_module_news extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        //категории новостей
        $this->createTable('{{%news_categories}}', [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),
            'slug' => $this->string(255),

            'description_ru' => $this->text(),
            'image'=> $this->string(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),

            'active' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        //новости
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),
            'slug' => $this->string(255),

            'description_ru' => $this->text(),
            'content_ru' => $this->text(),

            'image'=> $this->string(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),

            'active' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createTable('{{%news_categories_list}}', [
            'news_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'PRIMARY KEY `id` (news_id, category_id)',
        ], $tableOptions);

        $this->addForeignKey('news_categories_list_news_fk_constraint', '{{%news_categories_list}}', 'news_id', '{{%news}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('news_categories_list_news_categories_fk_constraint', '{{%news_categories_list}}', 'category_id', '{{%news_categories}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('news_categories_created_by_fk_constraint', '{{%news_categories}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('news_categories_updated_by_fk_constraint', '{{%news_categories}}', 'updated_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('news_created_by_fk_constraint', '{{%news}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('news_updated_by_fk_constraint', '{{%news}}', 'updated_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('news_categories_list_news_fk_constraint', '{{%news_categories_list}}');
        $this->dropForeignKey('news_categories_list_news_categories_fk_constraint', '{{%news_categories_list}}');
        $this->dropForeignKey('news_categories_created_by_fk_constraint', '{{%news_categories}}');
        $this->dropForeignKey('news_categories_updated_by_fk_constraint', '{{%news_categories}}');
        $this->dropForeignKey('news_created_by_fk_constraint', '{{%news}}');
        $this->dropForeignKey('news_updated_by_fk_constraint', '{{%news}}');

        $this->dropTable('{{%news_categories}}');
        $this->dropTable('{{%news}}');
        $this->dropTable('{{%news_categories_list}}');
    }

}
