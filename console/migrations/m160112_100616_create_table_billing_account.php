<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_100616_create_table_billing_account extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%billing_account}}', [
            'id' => $this->primaryKey(),
            'account_type' => $this->integer()->notNull(),
            'owner_type' => $this->string()->notNull(),
            'owner_id' => $this->integer()->notNull(),
            'current_budget' => $this->decimal(10, 2)->notNull()->defaultValue('0.0'),
            'locked' => $this->boolean()->notNull()->defaultValue(false),
            'date_locked' => $this->timestamp(),
            'author_id' => $this->integer()->notNull()->defaultValue(0),
            'sort' => $this->integer()->notNull()->defaultValue(0),
            'notes' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey(
            'billing_account_billing_account_type_fk_constraint',
            '{{%billing_account}}',
            'account_type',
            '{{%billing_account_type}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        $this->addForeignKey(
            'billing_account_user_fk_constraint',
            '{{%billing_account}}',
            'owner_id',
            '{{%user}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropForeignKey('billing_account_billing_account_type_fk_constraint', '{{%billing_account}}');
        $this->dropForeignKey('billing_account_user_fk_constraint', '{{%billing_account}}');
        $this->dropTable('{{%billing_account}}');
    }
}
