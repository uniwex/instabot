<?php

use yii\db\Schema;
use yii\db\Migration;

class m151129_135533_service_add_price extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service}}', 'price', $this->money()->notNull()->defaultValue(0));
        $this->addColumn('{{%service}}', 'min_count', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('{{%service}}', 'image', $this->string());
        $this->addColumn('{{%service}}', 'slug', $this->string(255));

        $this->addColumn('{{%service_category}}', 'slug', $this->string(255));
        $this->addColumn('{{%service_category}}', 'image', $this->string());


        $this->addForeignKey('service_created_by_fk_constraint', '{{%service}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('service_updated_by_fk_constraint', '{{%service}}', 'updated_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('service_created_by_fk_constraint', '{{%service}}');
        $this->dropForeignKey('service_updated_by_fk_constraint', '{{%service}}');

        $this->dropColumn('{{%service}}', 'price');
        $this->dropColumn('{{%service}}', 'min_count');
        $this->dropColumn('{{%service}}', 'image');
        $this->dropColumn('{{%service}}', 'slug');

        $this->dropColumn('{{%service_category}}', 'image');
        $this->dropColumn('{{%service_category}}', 'slug');
    }
}
