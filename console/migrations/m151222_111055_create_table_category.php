<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_111055_create_table_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        // Категории
        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),
            'description_ru' => $this->text(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'sort' => $this->integer()->defaultValue(100),
            'active' => $this->boolean()->defaultValue(false),
            'slug' => $this->string(255),
            'image' => $this->string(255),
            'type' => $this->string(255)
        ], $tableOptions);

        // Дерево
        $this->createTable('{{%category_tree}}', [
            'parent' => $this->integer()->notNull(),
            'child' => $this->integer()->notNull(),
            'depth' => $this->integer()->defaultValue(0),
            'PRIMARY KEY `id` (parent, child)'
        ], $tableOptions);

        $this->addForeignKey('fk1', '{{%category_tree}}', 'parent', '{{%category}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk2', '{{%category_tree}}', 'child', '{{%category}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%category_tree}}');
        $this->dropTable('{{%category}}');
    }
}
