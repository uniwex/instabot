<?php

use yii\db\Migration;

class m160126_090041_add_to_task extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}','subscribe_info',$this->string()->defaultValue(null));
        $this->addColumn('{{%task_run}}','error_count',$this->integer()->defaultValue(0));
        $this->addColumn('{{%task_run}}','subscribe_flag',$this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}','subscribe_flag');
        $this->dropColumn('{{%task_run}}','subscribe_info');
        $this->dropColumn('{{%task_run}}','error_count');
    }
}
