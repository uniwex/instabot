<?php

use yii\db\Schema;
use yii\db\Migration;

class m151107_170932_module_service extends Migration
{
    public $closureTbl = "service_category_tree";
    public $relativeTbl = "service_category";
    public $elementTbl = "service";

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        //дерево
        $this->createTable("{{%{$this->closureTbl}}}", [
            'parent' => $this->integer()->notNull(),
            'child' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        //Категории услуг
        $this->createTable("{{%{$this->relativeTbl}}}", [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),

            'description_ru' => $this->text(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),

            'sort' => $this->integer()->defaultValue(100),
            'active' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        //Услуги
        $this->createTable("{{%{$this->elementTbl}}}", [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),
            'type_debit' => $this->integer()->notNull()->defaultValue(1),

            'parent_id' => $this->integer(),
            'run_class_name' => $this->string()->notNull(),
            'description_ru' => $this->text(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),

            'sort' => $this->integer()->defaultValue(100),
            'active' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->addPrimaryKey("PK_{$this->closureTbl}", "{{%{$this->closureTbl}}}", ["parent", "child"]);
        $this->createIndex("FK_{$this->closureTbl}_child_{$this->relativeTbl}", "{{%{$this->closureTbl}}}", "child");

        $this->addForeignKey("FK_{$this->closureTbl}_child_{$this->relativeTbl}",
            "{{%{$this->closureTbl}}}", "child",
            "{{%{$this->relativeTbl}}}", "id",
            "CASCADE"
        );
        $this->addForeignKey("FK_{$this->closureTbl}_parent_{$this->relativeTbl}",
            "{{%{$this->closureTbl}}}", "parent",
            "{{%{$this->relativeTbl}}}", "id",
            "CASCADE"
        );
    }

    public function down()
    {
        $this->dropForeignKey("FK_{$this->closureTbl}_parent_{$this->relativeTbl}", "{{%{$this->closureTbl}}}");
        $this->dropForeignKey("FK_{$this->closureTbl}_child_{$this->relativeTbl}", "{{%{$this->closureTbl}}}");
        $this->dropTable("{{%{$this->closureTbl}}}");
        $this->dropTable("{{%{$this->relativeTbl}}}");
        $this->dropTable("{{%{$this->elementTbl}}}");
    }

}
