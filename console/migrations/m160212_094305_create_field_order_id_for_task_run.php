<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_094305_create_field_order_id_for_task_run extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}', 'order_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}', 'order_id');
    }
}
