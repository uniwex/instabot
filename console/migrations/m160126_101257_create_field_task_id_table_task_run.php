<?php

use yii\db\Migration;

class m160126_101257_create_field_task_id_table_task_run extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}', 'task_id', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}', 'task_id');
    }
}
