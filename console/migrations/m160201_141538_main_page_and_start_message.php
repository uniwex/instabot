<?php

use yii\db\Migration;

class m160201_141538_main_page_and_start_message extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%main_page}}", [
            'id' => $this->primaryKey(),
            'picture' => $this->string(),
            'button_text' => $this->string(),
            'button_class' => $this->string(),
            'text' => $this->text(),
        ], $tableOptions);

        $this->createTable("{{%start_message}}",[
            'id' => $this->primaryKey(),
            'tittle' => $this->string(),
            'text' => $this->text(),
            'action_button' => $this->text()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%main_page}}");
        $this->dropTable("{{%start_message}}");
    }
}
