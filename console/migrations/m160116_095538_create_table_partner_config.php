<?php

use yii\db\Schema;
use yii\db\Migration;

class m160116_095538_create_table_partner_config extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%partner_config}}', [
            'id' => $this->primaryKey(),
            'level1' => $this->float()->notNull()->defaultValue(0),
            'level2' => $this->float()->notNull()->defaultValue(0),
            'level3' => $this->float()->notNull()->defaultValue(0),
            'level4' => $this->float()->notNull()->defaultValue(0),
            'level5' => $this->float()->notNull()->defaultValue(0),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%partner_config}}');
    }
}
