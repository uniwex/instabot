<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_075514_create_field_button_service_for_carousel extends Migration
{
    public function up()
    {
        $this->addColumn('{{%carousel}}', 'button_service', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%carousel}}', 'button_service');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
