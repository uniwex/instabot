<?php

use yii\db\Migration;

class m160121_100804_TurboLiker extends Migration
{
    public function up()
{
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    }

    $this->createTable("{{%turboliker_account}}", [
        'id' => $this->primaryKey(),
        'login' => $this->string(),
        'password' => $this->string()
    ], $tableOptions);

    $this->createTable("{{%turboliker_coupon}}", [
        'id' => $this->primaryKey(),
        'coupon' => $this->string(),
        'not_work' => $this->integer()->defaultValue(0)
    ], $tableOptions);

}

    public function down()
    {
        $this->dropTable("{{%turboliker_coupon}}");
        $this->dropTable("{{%turboliker_account}}");
    }
}
