<?php

use yii\db\Migration;

class m160119_082935_add_criteria_price_and_regular_to_service extends Migration
{
    public function up()
    {
        $this->addColumn('{{%service}}', 'criteria_price', $this->decimal(10,2)->defaultValue(0.0));
        $this->addColumn('{{%service}}', 'regular_for_field',$this->string());
        $this->addColumn('{{%service}}', 'field_name', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%service}}','field_name');
        $this->dropColumn('{{%service}}','regular_for_field');
        $this->dropColumn('{{%service}}', 'criteria_price');
    }
}
