<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_100836_create_table_billing_register_balance extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%billing_register_balance}}', [
            'id' => $this->primaryKey(),
            'operation_type' => $this->string()->notNull(),
            'operation_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'debit' => $this->decimal(10, 2)->notNull()->defaultValue('0.0'),
            'credit' => $this->decimal(10, 2)->notNull()->defaultValue('0.0'),
            'balance' => $this->decimal(10, 2)->notNull()->defaultValue('0.0'),
        ], $tableOptions);

        $this->addForeignKey(
            'billing_register_balance_billing_account_fk_constraint',
            '{{%billing_register_balance}}',
            'account_id',
            '{{%billing_account}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropForeignKey('billing_register_balance_billing_account_fk_constraint', '{{%billing_register_balance}}');
        $this->dropTable('{{%billing_register_balance}}');
    }
}
