<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_121333_create_field_ban_delete_for_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'is_baned', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'is_baned');
    }
}
