<?php

use yii\db\Migration;

class m160407_123412_agregators extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}','agregator_id',$this->integer()->defaultValue(0));
        $this->addColumn('{{%billing_operation}}','agregator_id',$this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}','agregator_id');
        $this->dropColumn('{{%billing_operation}}','agregator_id');
    }
}
