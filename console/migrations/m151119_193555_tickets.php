<?php

use yii\db\Schema;
use yii\db\Migration;

class m151119_193555_tickets extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        //тикеты
        $this->createTable('{{%ticket}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),

            'status' => $this->integer(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),

            'active' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        //сообщения
        $this->createTable('{{%ticket_message}}', [
            'id' => $this->primaryKey(),
            'message' => $this->string(512)->notNull(),

            'ticket_id' => $this->integer(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),

            'active' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->addForeignKey("FK_ticket_message_ticket",
            "{{%ticket_message}}", "ticket_id",
            "{{%ticket}}", "id",
            "CASCADE"
        );

        $this->addForeignKey("FK_ticket_created_by",
            "{{%ticket}}", "created_by",
            "{{%user}}", "id",
            "CASCADE"
        );

        $this->addForeignKey("FK_ticket_updated_by",
            "{{%ticket}}", "updated_by",
            "{{%user}}", "id",
            "CASCADE"
        );

        $this->addForeignKey("FK_ticket_message_created_by",
            "{{%ticket_message}}", "created_by",
            "{{%user}}", "id",
            "CASCADE"
        );

        $this->addForeignKey("FK_ticket_message_updated_by",
            "{{%ticket_message}}", "updated_by",
            "{{%user}}", "id",
            "CASCADE"
        );
    }

    public function down()
    {
        $this->dropForeignKey("FK_ticket_created_by", "{{%ticket}}");
        $this->dropForeignKey("FK_ticket_updated_by", "{{%ticket}}");

        $this->dropForeignKey("FK_ticket_message_ticket", "{{%ticket_message}}");
        $this->dropForeignKey("FK_ticket_message_created_by", "{{%ticket_message}}");
        $this->dropForeignKey("FK_ticket_message_updated_by", "{{%ticket_message}}");

        $this->dropTable("{{%ticket}}");
        $this->dropTable("{{%ticket_message}}");
    }
}
