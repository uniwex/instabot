<?php

use yii\db\Migration;

class m160122_083546_add_task_info extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}', 'task_info', $this->string());

        $this->addColumn('{{%service}}','id_news',$this->integer());
        $this->addColumn('{{%service}}','price_unreg',$this->money()->notNull()->defaultValue(0.00));
        $this->addColumn('{{%service}}','min_members_count',$this->integer());
        $this->addColumn('{{%service}}','min_record_count',$this->integer());
        $this->addColumn('{{%service}}','stop_words',$this->smallInteger());
        $this->addColumn('{{%service}}','sup_field',$this->text());
        $this->addColumn('{{%service}}','subscribe',$this->integer());
        $this->addColumn('{{%service}}','only_logged',$this->smallInteger());

        $this->addColumn('{{%task_run}}','created_at',$this->timestamp());
        $this->addColumn('{{%task_run}}','updated_at',$this->timestamp());
        $this->addColumn('{{%task_run}}','email',$this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}', 'task_info');

        $this->dropColumn('{{%service}}','id_news');
        $this->dropColumn('{{%service}}','price_unreg');
        $this->dropColumn('{{%service}}','min_members_count');
        $this->dropColumn('{{%service}}','min_record_count');
        $this->dropColumn('{{%service}}','stop_words');
        $this->dropColumn('{{%service}}','sup_field');
        $this->dropColumn('{{%service}}','subscribe');
        $this->dropColumn('{{%service}}','only_logged');

        $this->dropColumn('{{%task_run}}','created_at');
        $this->dropColumn('{{%task_run}}','updated_at');
        $this->dropColumn('{{%task_run}}','email');

    }
}
