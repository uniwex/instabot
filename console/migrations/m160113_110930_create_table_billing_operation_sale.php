<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_110930_create_table_billing_operation_sale extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%billing_operation_sale}}', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer()->notNull(),
            'operation_sum' => $this->decimal(10, 2)->notNull()->defaultValue('0.0'),
            'operation_time' => $this->timestamp()->notNull(),
            'notes' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey(
            'billing_operation_sale_billing_account_fk_constraint',
            '{{%billing_operation_sale}}',
            'account_id',
            '{{%billing_account}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropForeignKey('billing_operation_sale_billing_account_fk_constraint', '{{%billing_operation_sale}}');
        $this->dropTable('{{%billing_operation_sale}}');
    }
}
