<?php

use yii\db\Migration;

class m160116_130824_table_criteria extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%criteria}}", [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),
            'image' => $this->string(255),
            'type' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%criteria}}");
    }
}
