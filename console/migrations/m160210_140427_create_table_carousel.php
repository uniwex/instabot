<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_140427_create_table_carousel extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%carousel}}", [
            'id' => $this->primaryKey(),
            'picture' => $this->string(),
            'title'=> $this->string(),
            'button_text' => $this->string(),
            'button_class' => $this->string(),
            'button_href' => $this->string(),
            'text' => $this->text(),
            'sort' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%carousel}}");
    }
}
