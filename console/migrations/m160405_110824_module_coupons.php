<?php

use yii\db\Migration;

class m160405_110824_module_coupons extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%coupon}}', [
            'id' => $this->primaryKey(),
            'coupon' => $this->string()->notNull()->unique(),
            'price' => $this->money()->notNull(),
            'used' => $this->boolean()->defaultValue(0),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%coupon}}');
    }
}
