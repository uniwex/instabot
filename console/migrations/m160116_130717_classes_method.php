<?php

use yii\db\Migration;

class m160116_130717_classes_method extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%classes_method}}", [
            'id' => $this->primaryKey(),
            'id_class' => $this->integer()->notNull(),
            'title_ru' => $this->string(255)->notNull(),
            'method' => $this->string(255)
        ], $tableOptions);

        $this->addForeignKey("FK_classes_child_classes_method",
            "{{%classes_method}}", "id_class",
            "{{%classes}}", "id",
            "CASCADE"
        );
    }

    public function down()
    {
        $this->dropForeignKey("FK_classes_child_classes_method", "{{%classes_method}}");
        $this->dropTable("{{%classes_method}}");
    }
}
