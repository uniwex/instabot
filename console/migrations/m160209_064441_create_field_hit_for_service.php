<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_064441_create_field_hit_for_service extends Migration
{
    public function up()
    {
        $this->addColumn('{{%service}}', 'is_hit', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('{{%service}}', 'is_hit');
    }
}
