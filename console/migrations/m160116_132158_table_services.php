<?php

use yii\db\Migration;

class m160116_132158_table_services extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service}}', 'id_class', $this->integer());
        $this->addColumn('{{%service}}', 'id_method', $this->integer());

        $this->addForeignKey('service_id_class_fk_constraint', '{{%service}}', 'id_class', '{{%classes}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('service_id_method_fk_constraint', '{{%service}}', 'id_method', '{{%classes_method}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('service_id_class_fk_constraint', '{{%service}}');
        $this->dropForeignKey('service_id_method_fk_constraint', '{{%service}}');

        $this->dropColumn('{{%service}}', 'id_method');
        $this->dropColumn('{{%service}}', 'id_class');
    }
}
