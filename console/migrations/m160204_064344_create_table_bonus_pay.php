<?php

use yii\db\Migration;

class m160204_064344_create_table_bonus_pay extends Migration
{
    public function up()
    {
        $this->createTable('bonus_pay', [
            'id' => $this->primaryKey(),
            'sum' => $this->decimal(10, 2)->notNull(),
            'percent' => $this->float()->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('bonus_pay');
    }
}
