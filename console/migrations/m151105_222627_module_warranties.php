<?php

use yii\db\Schema;
use yii\db\Migration;

class m151105_222627_module_warranties extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        //Гарантии
        $this->createTable('{{%warranties}}', [
            'id' => $this->primaryKey(),
            'title_ru' => $this->string(255)->notNull(),

            'description_ru' => $this->text(),
            'button_link' => $this->string(255),
            'image'=> $this->string(),

            'sort' => $this->integer()->defaultValue(100),
            'active' => $this->boolean()->defaultValue(false),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%warranties}}');
    }
}
