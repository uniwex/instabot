<?php

use yii\db\Migration;

class m160125_110901_task_time extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}','last_run',$this->timestamp());
        $this->addColumn('{{%service}}','type_subscribe',$this->smallInteger());
    }

    public function down()
    {
        $this->dropColumn('{{%service}}','type_subscribe');
        $this->dropColumn('{{%task_run}}','last_run');
    }
}
