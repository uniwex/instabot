<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_083958_change_fields_for_order extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%order}}', 'active', 'is_paid');
        $this->dropColumn('{{%order}}', 'title');
        $this->addColumn('{{%order}}', 'token', $this->string());
    }

    public function down()
    {
        $this->renameColumn('{{%order}}', 'is_paid', 'active');
        $this->addColumn('{{%order}}', 'title', $this->string());
        $this->dropColumn('{{%order}}', 'token');
    }
}
