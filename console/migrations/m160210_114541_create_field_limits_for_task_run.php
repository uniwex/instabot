<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_114541_create_field_limits_for_task_run extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}', 'limit1', $this->integer());
        $this->addColumn('{{%task_run}}', 'limit5', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}', 'limit1');
        $this->dropColumn('{{%task_run}}', 'limit5');
    }
}
