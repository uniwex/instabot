<?php

use yii\db\Migration;

class m160123_153254_create_field_access_token_table_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'access_token', $this->string()->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'access_token');
    }
}
