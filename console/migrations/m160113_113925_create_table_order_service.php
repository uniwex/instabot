<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_113925_create_table_order_service extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order_service}}', [
            'operation_id' => $this->integer(),
            'service_id' => $this->integer(),
            'PRIMARY KEY (`operation_id`, `service_id`)'
        ], $tableOptions);

        $this->addForeignKey(
            'order_service_operation_id_fk_constraint',
            '{{%order_service}}',
            'operation_id',
            '{{%billing_operation_sale}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        $this->addForeignKey(
            'order_service_service_id_fk_constraint',
            '{{%order_service}}',
            'service_id',
            '{{%service}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropForeignKey('order_service_operation_id_fk_constraint', '{{%order_service}}');
        $this->dropForeignKey('order_service_service_id_fk_constraint', '{{%order_service}}');
        $this->dropTable('{{%order_service}}');
    }
}
