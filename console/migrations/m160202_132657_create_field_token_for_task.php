<?php

use yii\db\Migration;

class m160202_132657_create_field_token_for_task extends Migration
{
    public function up()
    {
        $this->addColumn('{{%task_run}}', 'token', $this->string(15)->defaultValue(null));
    }

    public function down()
    {
        $this->dropColumn('{{%task_run}}', 'token');
    }
}
