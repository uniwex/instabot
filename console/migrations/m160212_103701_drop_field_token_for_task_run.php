<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_103701_drop_field_token_for_task_run extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%task_run}}', 'token');
    }

    public function down()
    {
        $this->addColumn('{{%task_run}}', 'token', $this->string());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
