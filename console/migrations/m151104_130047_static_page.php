<?php

use yii\db\Schema;
use yii\db\Migration;

class m151104_130047_static_page extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        //статические страницы
        $this->createTable('{{%static_page}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'slug' => $this->string(255),

            'content' => $this->text(),

            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
        ], $tableOptions);

        $this->addForeignKey('static_page_created_by_fk_constraint', '{{%static_page}}', 'created_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('static_page_updated_by_fk_constraint', '{{%static_page}}', 'updated_by', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('static_page_created_by_fk_constraint', '{{%static_page}}');
        $this->dropForeignKey('static_page_updated_by_fk_constraint', '{{%static_page}}');
        $this->dropTable('{{%static_page}}');
    }
}
