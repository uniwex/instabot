<?php

use yii\db\Migration;

class m160129_151834_service_queue extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable("{{%service_queue}}", [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'service_id' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable("{{%service_queue}}");
    }

}
