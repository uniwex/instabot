<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_100602_create_table_billing_account_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%billing_account_type}}', [
            'id' => $this->primaryKey(),
            'currency_id' => $this->string(3)->notNull(),
            'base' => $this->boolean()->notNull()->defaultValue(false),
            'auto_creation' => $this->boolean()->notNull()->defaultValue(false),
            'creation_time' => $this->timestamp(),
            'author_id' => $this->integer()->notNull()->defaultValue(0),
            'sort' => $this->integer()->notNull()->defaultValue(0),
            'notes' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey(
            'billing_account_type_billing_currency_fk_constraint',
            '{{%billing_account_type}}',
            'currency_id',
            '{{%billing_currency}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        $this->insert('{{%billing_account_type}}', [
            'currency_id' => 'RUB',
            'base' => true,
            'creation_time' => $this->timestamp(),
            'author_id' => 0
        ]);
    }

    public function down()
    {
        $this->dropForeignKey('billing_account_type_billing_currency_fk_constraint', '{{%billing_account_type}}');
        $this->dropTable('{{%billing_account_type}}');
    }
}
