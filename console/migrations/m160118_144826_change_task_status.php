<?php

use yii\db\Migration;

class m160118_144826_change_task_status extends Migration
{
    public function safeUp()
    {

        $this->dropColumn('{{%task_run}}', 'current_status');
        $this->addColumn('{{%task_run}}', 'current_status', $this->integer());
    }

}
