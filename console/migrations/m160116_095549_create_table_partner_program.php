<?php

use yii\db\Schema;
use yii\db\Migration;

class m160116_095549_create_table_partner_program extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%partner_program}}', [
            'referrer_id' => $this->integer()->notNull(),
            'referral_id' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addPrimaryKey('pk', '{{%partner_program}}', ['referrer_id', 'referral_id']);

        $this->addForeignKey(
            'partner_program_referrer_user_fk_constraint',
            '{{%partner_program}}',
            'referrer_id',
            '{{%user}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );

        $this->addForeignKey(
            'partner_program_referral_user_fk_constraint',
            '{{%partner_program}}',
            'referral_id',
            '{{%user}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropForeignKey('partner_program_referrer_user_fk_constraint', '{{%partner_program}}');
        $this->dropForeignKey('partner_program_referral_user_fk_constraint', '{{%partner_program}}');
        $this->dropTable('{{%partner_program}}');
    }
}
