<?php

use yii\db\Migration;

class m160216_130628_actually_carousel extends Migration
{
    public function up()
    {
        $this->addColumn('{{%carousel}}', 'actually', $this->date());
    }

    public function down()
    {
        $this->dropColumn('{{%carousel}}', 'actually');
    }
}
