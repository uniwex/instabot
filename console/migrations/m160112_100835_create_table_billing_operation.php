<?php

use yii\db\Schema;
use yii\db\Migration;

class m160112_100835_create_table_billing_operation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%billing_operation}}', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer()->notNull(),
            'operation_sum' => $this->decimal(10, 2)->notNull()->defaultValue('0.0'),
            'operation_time' => $this->timestamp()->notNull(),
            'payment_system' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull()->defaultValue(0),
            'notes' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey(
            'billing_operation_billing_account_fk_constraint',
            '{{%billing_operation}}',
            'account_id',
            '{{%billing_account}}',
            'id',
            'RESTRICT',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropForeignKey('billing_operation_billing_account_fk_constraint', '{{%billing_operation}}');
        $this->dropTable('{{%billing_operation}}');
    }
}
