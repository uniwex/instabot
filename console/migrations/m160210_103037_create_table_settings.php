<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_103037_create_table_settings extends Migration
{
    public function up()
    {
        $this->createTable('{{%settings}}', [
            'service_id' => $this->primaryKey(),
            'default_bonus' => $this->float(),
            'premium_bonus' => $this->float(),
            'premium_photo' => $this->integer(),
            'premium_followers' => $this->integer(),
        ]);

        $this->addForeignKey(
            'settings_service_id_service_id',
            '{{%settings}}',
            'service_id',
            '{{%service}}',
            'id'
        );
    }

    public function down()
    {
        $this->dropForeignKey('settings_service_id_service_id', '{{%settings}}');
        $this->dropTable('{{%settings}}');
    }
}
