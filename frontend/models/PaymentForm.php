<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 13.01.2016
 * Time: 13:00
 */

namespace frontend\models;

use Yii;
use yii\base\Model;

class PaymentForm extends Model
{
    public $sum;
    public $type;
    public $userId;

    public function rules() {
        return [
            [['sum'], 'required'],
            [['sum', 'type', 'userId'], 'number', 'integerOnly' => true]
        ];
    }

    public function attributeLabels()
    {
        return [
            'sum' => 'Сумма пополнения',
            'type' => 'Способ оплаты',
            'userId' => 'Пользователь'
        ];
    }
}