<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 03.02.2016
 * Time: 11:42
 */

namespace frontend\models;


use yii\base\Model;

class BuyForm extends Model
{
    public $type;

    public function rules()
    {
        return [
            ['type', 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => 'Способ оплаты'
        ];
    }
}