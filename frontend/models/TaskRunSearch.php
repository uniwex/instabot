<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TaskRun;

/**
 * TaskRunSearch represents the model behind the search form about `common\models\TaskRun`.
 */
class TaskRunSearch extends TaskRun
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'service_id'], 'integer'],
            [['fields', 'run_time', 'current_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskRun::find()->where('user_id='.Yii::$app->user->id)->orderBy('id desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'service_id' => $this->service_id,
            'run_time' => $this->run_time,
        ]);

        $query->andFilterWhere(['like', 'fields', $this->fields])
            ->andFilterWhere(['like', 'current_status', $this->current_status]);

        return $dataProvider;
    }
}
