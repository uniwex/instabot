<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 05.02.2016
 * Time: 10:33
 */
return [
//    'personal/default' => 'personal/default/index',
    'personal/profile' => 'personal/default/profile',
    'personal/payment' => 'personal/default/payment',
    'services' => 'services/index',
    'services/order/<slug:[\w-_]+>' => 'services/service',
    'static-page/<slug:[\w-_]+>' => 'static-page/view'
];