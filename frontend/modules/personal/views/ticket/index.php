<?php

use common\models\Ticket;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Помощь и поддержка';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Кабинет',
        'url' => Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'rel' => 'v: ' . Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'property' => 'v:Кабинет'
    ],
    [
        'label' => $this->title
    ]
];
?>
<div class="ticket-index">

    <p>
        <?= Html::a('Написать тикет <i class="fa icon-comment-1"></i>', ['create'], ['class' => 'btn btn-animated btn-gray-transparent']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'status',
            [
                'label' => 'Статус',
                'format' => 'raw',

                /** @var $data  Ticket*/
                'value' => function($data){
                    $iconColor = 'text-gray';
                    switch ($data->status){
                        case Ticket::STATUS_WAITING:
                            $iconColor = 'text-danger';
                            break;
                        case Ticket::STATUS_ANSWERED:
                            $iconColor = 'text-success';
                            break;
                        case Ticket::STATUS_READ:
                            $iconColor = 'text-primary';
                            break;
                        default:
                            break;
                    };
                    return "<i class='fa fa-circle $iconColor'></i>";
                },
            ],
            'title',
            'updated_at',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

</div>
