<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = $model->title;

$this->params['breadcrumbs'] = [
    [
        'label' => 'Кабинет',
        'url' => Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'rel' => 'v: ' . Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'property' => 'v:Кабинет'
    ],
    [
        'label' => 'Помощь и поддержка',
        'url' => Yii::$app->urlManager->createAbsoluteUrl(['/personal/ticket/index']),
        'rel' => 'v:' . Yii::$app->urlManager->createAbsoluteUrl(['/personal/ticket/index']),
        'property' => 'v:Помощь и поддержка'
    ],
    [
        'label' => $this->title
    ]
];
?>
<div class="ticket-view">

    <?= $this->render('_form_add_message', [
        'modelMessage' => $modelMessage,
    ]) ?>

    <?php Pjax::begin(['id' => 'notes']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'message:ntext',
            [
                'attribute' => 'createdBy.username',
                'label' => 'Отправитель'
            ],
            'created_at',
        ],
    ]); ?>
    <?php Pjax::end() ?>
</div>
