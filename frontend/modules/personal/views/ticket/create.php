<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Ticket */

$this->title = 'Создание тикета';
$this->params['breadcrumbs'] = [
    [
        'label' => 'Кабинет',
        'url' => Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'rel' => 'v: ' . Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'property' => 'v:Кабинет'
    ],
    [
        'label' => 'Помощь и поддержка',
        'url' => Yii::$app->urlManager->createAbsoluteUrl(['/personal/ticket/index']),
        'rel' => 'v:' . Yii::$app->urlManager->createAbsoluteUrl(['/personal/ticket/index']),
        'property' => 'v:Помощь и поддержка'
    ],
    [
        'label' => $this->title
    ]
];
?>
<div class="ticket-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
