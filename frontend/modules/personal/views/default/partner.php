<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 20.01.2016
 * Time: 16:40
 */

$this->title = 'Партнерская программа';

$this->params['breadcrumbs']= [
    [
        'label' => 'Кабинет',
        'url' => Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'rel' => 'v: ' . Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'property' => 'v:Кабинет'
    ],
    [
        'label' => $this->title
    ]
];
?>
<div class="personal-partner">
    <table class="table table-striped">
        <tr>
            <td>Пользователь</td>
            <td>Прибыль</td>
        </tr>
    <? foreach($balance as $user => $sum): ?>
        <tr>
            <td><?= $user ?></td>
            <td><?= $sum ?></td>
        </tr>
    <? endforeach;?>
    </table>
</div>