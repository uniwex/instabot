<?php

use app\modules\personal\models\ProfileForm;
use common\models\User;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\MaskedInput;

/**
 * @var $profileForm ProfileForm
 * @var $user_model User
 */


$this->title = 'Профиль';

$this->params['breadcrumbs'] = [
    [
        'label' => 'Кабинет',
        'url' => Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'rel' => 'v: ' . Yii::$app->urlManager->createAbsoluteUrl(['/personal/index']),
        'property' => 'v:Кабинет'
    ],
    [
        'label' => $this->title
    ]
];
?>

<div class="personal-default-index">
    <div class="row">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin([
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
            ]); ?>

            <? if (Yii::$app->session->hasFlash('password')) { ?>
                <div class="alert alert-danger">
                    <?= Yii::$app->session->getFlash('password') ?>
                </div>
            <? } ?>

            <?= $form->errorSummary($profileForm, ['class' => 'alert alert-danger']) ?>

            <?= $form->field($profileForm, 'email')->textInput() ?>

            <?= $form->field($profileForm, 'oldPassword')->passwordInput() ?>

            <?= $form->field($profileForm, 'newPassword')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>