<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 12.01.2016
 * Time: 15:19
 */
use \yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\models\PaymentForm;
use common\helpers\PaymentTypeHelper;
/* @var $model PaymentForm */

$status = null;
?>
<? $form = ActiveForm::begin(['options' => ['class' => 'form-group']]); ?>

<? Yii::$app->session->hasFlash('status') ? $status = Yii::$app->session->getFlash('status') : $status = null; ?>
<?= $status ?>
<?= $form->field($model,'type')->dropDownList(PaymentTypeHelper::getUserPaymentType());?>
<?= $form->field($model, 'sum')->input('number', ['min' => 1]) ?>
<?= Html::submitButton('Пополнить', ['class' => 'btn btn-primary']) ?>
<? ActiveForm::end(); ?>
