<?php
use bookin\walletone\WalletOneButton;

/**
 * @var $wallet_options array
 */

echo WalletOneButton::widget([
    'walletOptions' => $wallet_options
]);

