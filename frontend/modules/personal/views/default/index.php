<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\StatusHelper;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TaskRunSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $profile app\modules\personal\models\ProfileForm */

$this->title = 'Кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-run-index">

    <? if (User::isBaned()) { ?>

        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> Ваш аккаунт заблокирован. Часть функционала недоступна.
        </div>

    <? } ?>

    <h2>Заказы</h2>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => '',
        'columns' => [
            'id',
            [
                'label' => 'Название услуги',
                'content' => function ($model) {
                    return $model->service->title_ru;
                }
            ],
            [
                'label' => 'Объект',
                'content' => function ($model) {
                    $fields = json_decode($model->fields);
                    return "<a target='_blank' href='" . $fields->field . "'>Ссылка</a>";
                }
            ],
            [
                'label' => 'Количество',
                'content' => function ($model) {
                    $fields = json_decode($model->fields);
                    if (isset($fields->count))
                        return $fields->count;
                    else return 0;
                }
            ],
            [
                'label' => 'Стоимость',
                'content' => function ($model) {
                    $json = json_decode($model->fields);
                    if (isset($json->summary_price)) {
                        return $json->summary_price;
                    }
                    return 0;
                }
            ],
           /* [
                'label' => 'Критерии',
                'content' => function ($model) {
                    $json = json_decode($model->fields);
                    if (isset($json->criteria->values)) {
                        return "Да";
                    } else {
                        return "Нет";
                    }
                }
            ],*/
         /*   [
                'label' => 'Прогресс',
                'content' => function ($model) {
                    $jsonTaskInfo = json_decode($model->task_info);
                    $jsonFields = json_decode($model->fields);
                    if (isset($jsonTaskInfo->complete)) {
                        if ($jsonTaskInfo->complete == 0) {
                            $percent = 0;
                        } else {
                            $percent = (100 * $jsonTaskInfo->complete) / $jsonFields->count;
                        }

                    } else $percent = 0;
                    return '<div class="progress">
                              <div class="progress-bar" role="progressbar" style="width:' . $percent . '%">
                                <span class="sr-only">' . $percent . '% Complete</span>
                              </div>
                            </div>';
                }
            ],*/
            [
                'label' => 'Статус',
                'content' => function ($model) {
                    return StatusHelper::getDescription($model->current_status);
                }
            ],
            //['task_info']
        ],
    ]); ?>

    <h2>Баланс</h2>

    <div class="form-group">
        <h3><?= $profile->balance ?> <i class="fa fa-rub"></i> <a href="/personal/payment" class="btn btn-success">Пополнить
                баланс</a></h3>
    </div>
</div>
