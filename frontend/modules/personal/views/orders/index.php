<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TaskRunSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Кабинет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-run-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'service.title_ru',
            'run_time',
            'current_status',
        ],
    ]); ?>
</div>
