<?php

namespace app\modules\personal;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\personal\controllers';
    public $layout = '@frontend/views/layouts/profile';
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
