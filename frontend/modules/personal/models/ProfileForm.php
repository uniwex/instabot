<?php
namespace app\modules\personal\models;

use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;


class ProfileForm extends Model
{
    public $oldPassword;
    public $newPassword;
    public $email;
    public $balance;

    /**
     * @var \common\models\User
     */
    private $_user;


    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->_user = Yii::$app->user->identity;
        if (!$this->_user) {
            throw new InvalidParamException('Пожалуйста авторизуйтесь в системе');
        }
        $this->email = $this->_user->email;
        $this->balance = $this->_user->balance;
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oldPassword', 'newPassword'], 'string', 'min' => 6],
            [['email'], 'email']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldPassword' => 'Старый пароль',
            'newPassword' => 'Новый пароль'
        ];
    }


    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        return $user->save(false);
    }
}
