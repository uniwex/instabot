<?php

namespace app\modules\personal\controllers;

use app\modules\personal\models\ProfileForm;
use bookin\walletone\WalletOne;
use common\helpers\PaymentTypeHelper;
use common\models\BillingAccount;
use common\models\BillingOperation;
use common\models\BillingRegisterBalance;
use common\models\BonusPay;
use common\models\Order;
use common\models\PartnerProgram;
use common\models\User;
use frontend\models\PaymentForm;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use frontend\models\TaskRunSearch;

class DefaultController extends Controller
{

    public $layout = '@frontend/views/layouts/profile.php';

    public function actionIndex()
    {   /*$value =  new Expression('NOW()');
        die(var_dump($value));
        */try {
            $profileForm = new ProfileForm();
        }
        catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $searchModel = new TaskRunSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=10;
        //die(var_dump($dataProvider));
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'profile' => $profileForm
        ]);
    }

    public function actionProfile()
    {

        $param = [];
        try {
            $profileForm = new ProfileForm();
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if (Yii::$app->request->post()) {
            $oldPass = Yii::$app->request->post()['ProfileForm']['oldPassword'];
            $newPass = Yii::$app->request->post()['ProfileForm']['newPassword'];

            $model = User::find()->where(['id' => Yii::$app->user->identity->getId()])->one();
            $model->attributes = Yii::$app->request->post()['ProfileForm'];
            if ($oldPass && $newPass) {
                if ($model->validatePassword($oldPass)) $model->setPassword($newPass);
                else Yii::$app->session->setFlash('password', 'Пароли не совпадают!');
            }

            if ($model->validate()) {
                $model->save();
                return $this->redirect(['/personal/profile']);
            }
        }
        if ($profileForm->load(Yii::$app->request->post()) && $profileForm->validate()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
        }
        $param['profileForm'] = $profileForm;

        return $this->render('profile', $param);
    }

    public function actionPayment()
    {
        if (!Yii::$app->user->isGuest) {
            $this->view->title = 'Пополнение баланса';
            $payment = new PaymentForm();
            if ($payment->load(Yii::$app->request->post())) {
                if ($payment->type == PaymentTypeHelper::PAYMENT_TYPE_ROBOKASSA){
                    $mrh_login = Yii::$app->params['robokassa'][0];
                    $mrh_pass1 = Yii::$app->params['robokassa'][1];
                    $order = new Order();
                    $order->owner_id = Yii::$app->user->id;
                    $order->amount = $payment->sum;
                    $order->token = "popolnenie_balanca";
                    $order->save();
                    $inv_id = $order->id;
                    $inv_desc = 'Пополнение баланса Turbo - Inst.ru';
                    $out_summ = $payment->sum;
                    $id = Yii::$app->user->id;

                    $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:shp_id=$id");
                    return $this->redirect(
                        "https://auth.robokassa.ru/Merchant/Index.aspx?" .
                        "MrchLogin=$mrh_login&OutSum=$out_summ&InvId=$inv_id&Desc=$inv_desc&SignatureValue=$crc&shp_id=$id");
                }
                if ($payment->type == PaymentTypeHelper::PAYMENT_TYPE_WALLET_ONE) {
                    $order = new Order();
                    $order->owner_id = Yii::$app->user->id;
                    $order->amount = $payment->sum;
                    $order->token = "popolnenie_balanca";
                    $order->save();
                    $wallet_options = [
                        'WMI_PAYMENT_AMOUNT'=>$payment->sum,
                        'WMI_CURRENCY_ID'=>WalletOne::CurrencyID('RUB'),
                        'WMI_DESCRIPTION'=>'TurboInst payment',
                        'WMI_PAYMENT_NO'=>$order->id,
                    ];
                    return $this->render('walletone',['wallet_options'=>$wallet_options]);
                }
                if ($payment->type == PaymentTypeHelper::PAYMENT_TYPE_INTERKASSA) {
                    $order = new Order();
                    $order->owner_id = Yii::$app->user->id;
                    $order->amount = $payment->sum;
                    $order->token = "popolnenie_balanca";
                    $order->save();
                    $params = [
                        "ik_co_id" => "56d07c6e3b1eaf16288b4567",
                        "ik_pm_no" => $order->id,
                        "ik_am" => $payment->sum,
                        "ik_cur" => "RUB",
                        "ik_desc" => "Пополнение баланса Turbo-inst",
                        "ik_fal_u" => "http://turbo-inst.ru/interkassa/fail",
                        "ik_suc_u" => "http://turbo-inst.ru/interkassa/success",
                        "ik_pnd_u" => "http://turbo-inst.ru/interkassa/pending",
                        "ik_ia_u" => "http://turbo-inst.ru/interkassa/interaction",
                    ];
                    //todo for sahasu вынести секретный ключ в config params
                    $secret_key = 'sYTVegRJDMaqDXyp';
                    $dataSet = $params;
                    unset($dataSet['ik_sign']);//удаляем из данных строку подписи
                    ksort($dataSet, SORT_STRING); // сортируем по ключам в алфавитном порядке элементы массива
                    array_push($dataSet, $secret_key); // добавляем в конец массива "секретный ключ"
                    $signString = implode(':', $dataSet); // конкатенируем значения через символ ":"
                    $sign = base64_encode(md5($signString, true)); // берем MD5 хэш в бинарном виде по сформированной строке и кодируем в BASE64
                    $params['ik_sign'] = $sign;
                    $url = "https://sci.interkassa.com/?".http_build_query($params);
                    return $this->redirect($url);
                }
            }
            return $this->render('payment', ['model' => $payment]);
        } else return $this->goHome();
    }
}