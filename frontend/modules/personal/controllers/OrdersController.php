<?php

namespace app\modules\personal\controllers;

use Yii;
use common\models\TaskRun;
use yii\web\Controller;
use yii\filters\VerbFilter;
use frontend\models\TaskRunSearch;
use yii\web\NotFoundHttpException;

class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Finds the TaskRun model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $user_id
     * @param integer $service_id
     * @return TaskRun the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($user_id, $service_id)
    {
        if (($model = TaskRun::findOne(['user_id' => $user_id, 'service_id' => $service_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
