<?php
namespace frontend\controllers;

use common\models\RunModel;
use Yii;
use common\components\BasketManager;
use common\models\Service;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use common\models\BillingOperationSale;

/**
 * Class BasketController
 * @package app\controllers
 */
class BasketController extends Controller
{

    /**
     * Экшн добавления товара в корзину
     * @param $id
     * @param int $count
     * @return array|string
     */
    public function actionAdd($id, $count = 1)
    {
        $this->_getBasketManager()->add($id, $count);

        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

            $res = [
                'success' => true,
            ];

            return $res;
        }

        return $this->redirect(['index']);
    }

    public function actionBuy()
    {
        foreach (Yii::$app->basket->basket as $item) {
            if ($item->service->type_debit == 0) {
                $payment = new BillingOperationSale();
                $payment->itemList = $item;
                $payment->operation_sum = $item->totalPrice;
                $payment->saveOperation(true);
            }
            $item->save();
        }

        Yii::$app->basket->flush();
        return $this->redirect(['/personal']);
    }

    /**
     * Экшн удаления товара из корзины
     * @param $id
     * @return array|string
     */
    public function actionDelete($id)
    {
        $this->_getBasketManager()->delete($id);

        /*if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

            $res = [
                'success' => true,
                'models' => ArrayHelper::toArray(
                    Service::find()->where(['id' => $this->_getBasketManager()->listIds()])->select('id, title_ru, price')->all(), [
                        'common\models\Service' => ['id', 'title_ru', 'price',
                            'count' => function ($product) {
                                return $this->redirect(['/services/index']);
                            },
                        ],
                    ]
                ),
            ];

            return $res;
        }*/
        $url = Yii::$app->request->referrer;
        return $this->redirect($url);
    }

    /**
     * Экшн указания количества товара в корзине
     * @param $id
     * @param $count
     * @return string|array
     */
    public function actionSet($id, $count)
    {
        $this->_getBasketManager()->set($id, $count);

        if (\Yii::$app->request->isAjax) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

            $res = [
                'success' => true,
                'models' => ArrayHelper::toArray(
                    Service::find()->where(['id' => $this->_getBasketManager()->listIds()])->select('id, title_ru, price')->all(), [
                        'common\models\Service' => ['id', 'title_ru', 'price',
                            'count' => function ($product) {
                                return \Yii::$app->basket->getCount($product->id);
                            },
                        ],
                    ]
                ),
            ];

            return $res;
        }

        return $this->redirect(['index']);
    }

    /**
     * Экшн просмотра корзины
     * @return string
     */
    public function actionIndex()
    {
        $basket = $this->_getBasketManager();
        $models = Service::findAll($basket->listIds());
        return $this->render('view', ['basket' => $basket, 'models' => $models]);
    }

    /**
     * @return BasketManager
     */
    private function _getBasketManager()
    {
        return \Yii::$app->basket;
    }
}