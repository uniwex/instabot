<?php

namespace frontend\controllers;

use common\models\BillingOperation;
use common\models\BillingOperationSale;
use common\models\Order;
use common\models\TaskRun;
use console\controllers\PeriodController;
use frontend\models\PaymentForm;
use frontend\models\PaymentInterkassaForm;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use common\helpers\PaymentTypeHelper;


class InterkassaController extends Controller
{

    //неудачная оплата
    public function actionFail()
    {
        return $this->render('fail');
    }

    //ожидание
    public function actionPending()
    {
        return $this->render('pending');
    }

    //удачная оплата
    public function actionSuccess()
    {
        return $this->render('success');
    }
    //удачная гостевая оплата
    public function actionSuccessguest()
    {
        $dataPost = array();
        if (isset($_POST)) {
            foreach ($_POST as $key => $value) {
                $dataPost[$key] = $value;
            }
        }

        $order = Order::find()->where(['id' => $dataPost['ik_pm_no']])->one();
        $token = $order->token;
        $url = Yii::$app->urlManager->createAbsoluteUrl(['/order/view', 'token' => $token]);
        Yii::$app->session->setFlash(
            'link', 'Статистика по Вашему заказу доступна по ссылке: ' .
            Html::a($url, $url, ['target' => '_blank']));

        return $this->render('successguest');
    }

    public function actionInteraction()
    {
        $dataPost = array();
        if (isset($_POST)) {
            foreach ($_POST as $key => $value) {
                $dataPost[$key] = $value;
            }
        }
        //сикретный ключ незобыть поненять
        //вормирование цифровой подписи
        //сикретный ключ
        $secret_key = 'sYTVegRJDMaqDXyp';
        $dataSet = $dataPost;
        unset($dataSet['ik_sign']);//удаляем из данных строку подписи
        ksort($dataSet, SORT_STRING); // сортируем по ключам в алфавитном порядке элементы массива
        array_push($dataSet, $secret_key); // добавляем в конец массива "секретный ключ"
        $signString = implode(':', $dataSet); // конкатенируем значения через символ ":"
        $sign = base64_encode(md5($signString, true)); // берем MD5 хэш в бинарном виде по сформированной строке и кодируем в BASE64
        if ($sign == $dataPost['ik_sign']) {
            $order = Order::find()->where(['id' => $dataPost['ik_pm_no']])->one();
            $order->is_paid = 1;
            $order->save();

            $operation = new BillingOperation();
            $operation->operation_sum = $dataPost['ik_am'];
            $operation->ownerId = $order->owner_id;
            $operation->payment_system = PaymentTypeHelper::PAYMENT_TYPE_INTERKASSA;
            $operation->saveOperation();

            $tasks = TaskRun::find()->where(['order_id' => $dataPost['ik_pm_no'],'current_status'=>0])->all();
            if (isset($tasks)) {
                foreach ($tasks as $task) {
                    // $price = json_decode($task->fields)->summary_price;
                    $payment = new BillingOperationSale();
                    $payment->ownerId = $order->owner_id;
                    $payment->operation_sum = $dataPost['ik_am'];
                    $task->agregator_id = PaymentTypeHelper::PAYMENT_TYPE_INTERKASSA;
                    $task->save();
                    $payment->saveOperation();
                    PeriodController::actionSupertest($task->id);
                }
            }
        } else {
            exit();
        }
        //то что приходит в пост
        /*array (
            'ik_co_id' => '570e19383b1eaf0c218b456f',
            'ik_co_prs_id' => '405603459304',
            'ik_inv_id' => '48157005',
            'ik_inv_st' => 'success',
            'ik_inv_crt' => '2016-04-15 14:37:56',
            'ik_inv_prc' => '2016-04-15 14:37:56',
            'ik_trn_id' => '',
            'ik_pm_no' => '1',
            'ik_pw_via' => 'test_interkassa_test_xts',
            'ik_am' => '1',
            'ik_co_rfn' => '1',
            'ik_ps_price' => '1.03',
            'ik_cur' => 'RUB',
            'ik_desc' => 'Event Description',
            'ik_sign' => 'di9gJz/SVFn63+KXH+ht6A==',
        )
        */
    }
}

