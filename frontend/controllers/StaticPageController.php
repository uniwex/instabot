<?php

namespace frontend\controllers;

use Yii;
use common\models\StaticPage;
use common\models\StaticPageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



class StaticPageController extends Controller
{
    public $layout = 'left_sidebar';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ]
        ];
    }

    /**
     * @param $slug
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($slug)
    {
        return $this->render('view', [
            'model' => $this->findModel($slug),
        ]);
    }


    /**
     * @param $slug
     * @throws NotFoundHttpException
     */
    protected function findModel($slug)
    {
        if (($model = StaticPage::find()->where(['slug'=>$slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }
}
