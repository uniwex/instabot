<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 03.02.2016
 * Time: 10:36
 */

namespace frontend\controllers;

use bookin\walletone\WalletOne;
use common\helpers\PaymentTypeHelper;
use common\models\BillingOperation;
use common\models\Coupon;
use console\controllers\PeriodController;
use common\models\BillingOperationSale;
use common\models\Order;
use common\models\TaskRun;
use common\models\TaskRunSearch;
use common\models\User;
use frontend\models\BuyForm;
use Yii;
use yii\base\DynamicModel;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

class OrderController extends Controller
{
    const LOGGING = true;

    public $layout = 'left_sidebar';

    /**
     * @return string
     * @throws \bookin\walletone\WalletOneException
     */
    public function actionIndex()
    {
        $form = new BuyForm();

        $totalPrice = Yii::$app->basket->priceBasket;
        if (!isset(Yii::$app->session['guest']) && Yii::$app->user->isGuest)
            Yii::$app->session['guest'] = User::getPassword(15);

        if ($form->load(Yii::$app->request->post())) {
            $token = isset(Yii::$app->session['guest']) ? Yii::$app->session['guest'] : null;
            $order = new Order();
            $order->owner_id = Yii::$app->user->isGuest ? Yii::$app->params['guest_id'] : Yii::$app->user->id;
            $order->amount = $totalPrice;
            $order->token = $token;
            if ($order->save()) {
                $orderId = $order->id;
                foreach (Yii::$app->basket->basket as $item) {
                    $item->order_id = $orderId;
                    $item->saveUnreg();
                }
                Yii::$app->basket->flush();
                Yii::$app->session->remove('guest');

                if ($form->type == PaymentTypeHelper::PAYMENT_TYPE_ROBOKASSA) {
                    $mrh_login = Yii::$app->params['robokassa'][0];
                    $mrh_pass1 = Yii::$app->params['robokassa'][1];
                    $inv_id = $order->id;
                    $inv_desc = 'Оплата заказа Turbo - Inst.ru';
                    $def_sum = $totalPrice;
                    $id = Yii::$app->user->id ? Yii::$app->user->id : Yii::$app->params['guest_id'];

                    $crc = md5("$mrh_login:$def_sum:$inv_id:$mrh_pass1:shp_id=$id:shp_orderId=$orderId:shp_token=$token");
                    return $this->redirect(
                        "https://auth.robokassa.ru/Merchant/Index.aspx?" .
                        "MrchLogin=$mrh_login&OutSum=$def_sum&InvId=$inv_id" .
                        "&Desc=$inv_desc&SignatureValue=$crc&shp_id=$id&shp_orderId=$orderId&shp_token=$token" . "");
                }

                if($form->type == PaymentTypeHelper::PAYMENT_TYPE_INTERKASSA){
                    //die('vedetsi testirovanie');
                    //id кассы незобыть поненять
                    $params = [
                        "ik_co_id" => "56d07c6e3b1eaf16288b4567",
                        "ik_pm_no" => $order->id,
                        "ik_am" => $totalPrice,
                        "ik_cur" => "RUB",
                        "ik_desc" => "Пополнение баланса Turbo-inst гостевей платеж",
                        "ik_fal_u" => "http://turbo-inst.ru/interkassa/fail",
                        "ik_suc_u" => "http://turbo-inst.ru/interkassa/successguest",
                        "ik_pnd_u" => "http://turbo-inst.ru/interkassa/pending",
                        "ik_ia_u" => "http://turbo-inst.ru/interkassa/interaction",
                    ];
                    //сикретный ключ незобыть поненять
                    //вормирование цифровой подписи

                    $secret_key = 'sYTVegRJDMaqDXyp';
                    $dataSet = $params;
                    unset($dataSet['ik_sign']);//удаляем из данных строку подписи
                    ksort($dataSet, SORT_STRING); // сортируем по ключам в алфавитном порядке элементы массива
                    array_push($dataSet, $secret_key); // добавляем в конец массива "секретный ключ"
                    $signString = implode(':', $dataSet); // конкатенируем значения через символ ":"
                    $sign = base64_encode(md5($signString, true)); // берем MD5 хэш в бинарном виде по сформированной строке и кодируем в BASE64
                    $params['ik_sign'] = $sign;
                    $url = "https://sci.interkassa.com/?".http_build_query($params);
                    return $this->redirect($url);
                }

                if($form->type == PaymentTypeHelper::PAYMENT_TYPE_WALLET_ONE) {
                   // $totalPrice = 1;
                    $wallet_options = [
                        'WMI_PAYMENT_AMOUNT'=>$totalPrice,
                        'WMI_CURRENCY_ID'=>WalletOne::CurrencyID('RUB'),
                        'WMI_DESCRIPTION'=>'TurboInst payment',
                        'WMI_PAYMENT_NO'=>$order->id,
                        'token' => $order->token
                    ];
                    return $this->render('@frontend/modules/personal/views/default/walletone',['wallet_options'=>$wallet_options]);
                }
            }
        }

        $coupon_form = new DynamicModel(['coupon']);
        $coupon_form->addRule(['coupon'],'string',['max'=>29]);
        if ($coupon_form->load(Yii::$app->request->post()) && $coupon_form->validate()) {
            $coupon = Coupon::find()->where(['coupon'=>$coupon_form->coupon,'used'=>0])->one();
            if (isset($coupon)) {
                $coupon->used = 1;
                $coupon->save();
            } else {
                Yii::$app->session->setFlash('error_coupon','Извините купон использован или не существует');
            }
        }
        $models = Yii::$app->session['basket'];
        return $this->render('index', [
            'models' => $models,
            'model' => $form,
            'totalPrice' => $totalPrice,
            'coupon_form' => $coupon_form
        ]);
    }

    public function actionRobokassa()
    {
        $mrh_pass2 = Yii::$app->params['robokassa'][2];
        file_put_contents('/var/www/insta/data/www/turbo-inst.ru/log.txt', var_export($_REQUEST, 1), FILE_APPEND);
        if (isset($_REQUEST['shp_orderId']) && !empty($_REQUEST['shp_orderId'])) {
            // HTTP parameters:
            $out_summ = $_REQUEST["OutSum"];
            $inv_id = $_REQUEST["InvId"];
            $shpId = $_REQUEST['shp_id']; // User ID
            $orderId = $_REQUEST['shp_orderId']; // Order ID
            $token = $_REQUEST['shp_token']; // Token
            $crc = $_REQUEST["SignatureValue"];
            $crc = strtoupper($crc);

            $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:shp_id=$shpId:shp_orderId=$orderId:shp_token=$token"));

            if (strtoupper($my_crc) != strtoupper($crc)) {
                echo "bad sign\n";
                exit();
            }
            echo "OK$inv_id\n";


            $tasks = TaskRun::find()->where(['order_id' => $orderId])->all();
            $log = "\n\n";

            $order = Order::find()->where(['id' => $orderId])->one();
            $order->is_paid = 1;
            if ($order->save())
                $log .= 'Заказ обновлен' . "\n";
            else
                $log .= var_export($order->getErrors(), 1) . "\n";

            $log .= 'Всего услуг: ' . count($tasks) . ' Номер заказа: ' . $orderId . "\n";
            foreach ($tasks as $task) {
                $price = json_decode($task->fields)->summary_price;
                $payment = new BillingOperationSale();
                $payment->ownerId = $shpId;
                $payment->operation_sum = $price;
                $task->agregator_id = $inv_id;
                $task->save();
                $log .= 'Услуга: ' . $task->service->title_ru . '; Стоимость: ' . $payment->operation_sum . "\n";
                if ($payment->saveOperation()) $log .= 'Операция сохранена' . "\n";
                else $log .= 'Операция не сохранена' . "\n";
                PeriodController::actionSupertest($task->id);
            }
        } else {
            // HTTP parameters:
            $out_summ = $_REQUEST["OutSum"];
            $inv_id = $_REQUEST["InvId"];
            $shpId = $_REQUEST['shp_id']; // User ID
            $crc = $_REQUEST["SignatureValue"];
            $crc = strtoupper($crc);
            $order = Order::find()->where(['id' => $inv_id])->one();
            $order->is_paid = 1;
            $order->save();
            $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:shp_id=$shpId"));
            file_put_contents('/var/www/insta/data/www/turbo-inst.ru/log.txt', "\n$my_crc\n$crc\n", FILE_APPEND);
            if (strtoupper($my_crc) != strtoupper($crc)) {
                echo "bad sign\n";
                exit();
            }
            echo "OK$inv_id\n";

            $operation = new BillingOperation();
            $operation->operation_sum = $out_summ;
            $operation->ownerId = $shpId;
            $operation->payment_system = PaymentTypeHelper::PAYMENT_TYPE_ROBOKASSA;
            $operation->saveOperation();
            file_put_contents('/var/www/insta/data/www/turbo-inst.ru/log.txt', $operation->id, FILE_APPEND);
        }
    }

    public function actionWalletOne(){
        $post = Yii::$app->request->post();
        $this->logging(implode('|',$post));
        $walletone = Yii::$app->walletone;
        try{
            if($walletone->checkPayment($post)){
                $this->logging($post['WMI_PAYMENT_NO']);
                $order = Order::find()->where(['id'=>$post['WMI_PAYMENT_NO']])->one();
                if (isset($post['token'])){ //Гостевой заказ
                    $tasks = TaskRun::find()->where(['order_id' => $post['WMI_PAYMENT_NO'],'current_status'=> 0])->all();
                    foreach ($tasks as $task) {
                        $task->agregator_id = $post['WMI_PAYMENT_NO'];
                        $task->save();
                        $price = json_decode($task->fields)->summary_price;
                        $payment = new BillingOperationSale();
                        $payment->ownerId = $order->owner_id;
                        $payment->operation_sum = $price;
                        $payment->saveOperation();
                        $this->logging('start task '.$post['WMI_PAYMENT_NO']);
                        PeriodController::actionSupertest($task->id);
                    }
                } else {
                    $operation = new BillingOperation();
                    $operation->operation_sum = $post['WMI_PAYMENT_AMOUNT'];
                    $operation->agregator_id = $post['WMI_PAYMENT_NO'];
                    $operation->ownerId = $order->owner_id;
                    $operation->payment_system = PaymentTypeHelper::PAYMENT_TYPE_WALLET_ONE;
                    $operation->saveOperation();
                }
            }
        }catch (ErrorException $c){
            $this->logging($c->getMessage());
            return 'WMI_RESULT=RETRY&WMI_DESCRIPTION='.$c->getMessage();
        }
        return 'WMI_RESULT=OK';
    }

    public function actionSuccess()
    {
        if (!isset(Yii::$app->request->get()['shp_token'])) {
            if (!isset(Yii::$app->request->post()['token'])) {
                Yii::$app->session->setFlash('status', 'Оплата прошла успешно');
                return $this->redirect(['/personal/default/payment']);
            } else {
                $token = isset(Yii::$app->request->post()['token']) ? Yii::$app->request->post()['token'] : '';
                $url = Yii::$app->urlManager->createAbsoluteUrl(['/order/view', 'token' => $token]);
                Yii::$app->session->setFlash(
                    'link', 'Статистика по Вашему заказу доступна по ссылке: ' .
                    Html::a($url, $url, ['target' => '_blank'])
                );
            }
        }
        else {
            $token = isset(Yii::$app->request->get()['shp_token']) ? Yii::$app->request->get()['shp_token'] : '';
            $url = Yii::$app->urlManager->createAbsoluteUrl(['/order/view', 'token' => $token]);
            Yii::$app->session->setFlash(
                'link', 'Статистика по Вашему заказу доступна по ссылке: ' .
                Html::a($url, $url, ['target' => '_blank'])
            );
        }
        return $this->render('success');
    }
    
    public function actionFail()
    {
        return $this->render('fail');
    }

    public function actionView($token)
    {
        $model = Order::find()->where('BINARY [[token]] = :token', ['token' => $token])->one();
        $model = TaskRun::find()->where('order_id = :id', ['id' => $model->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model
        ]);
        return $this->render('view', ['dataProvider' => $dataProvider]);
    }

    private function logging($text) {
        if (self::LOGGING) {
            $text = Date("Y-m-d H:i:s") . " - " . $text . " \n";
            file_put_contents(Yii::getAlias('@webroot') . '/testpaymentlog.txt', $text, FILE_APPEND);
        }
    }
}