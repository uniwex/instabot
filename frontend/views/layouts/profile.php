<?php
/* @var $this \yii\web\View */
use yii\bootstrap\Html;
use common\widgets\SideBarBlock;

$this->beginContent('@frontend/views/layouts/main.php'); ?>
    <section class="wrap main-container">

        <div class="container">
            <div class="row">
                <div class="main col-md-8 col-lg-offset-1 col-md-push-4 col-lg-push-3">
                    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>

                    <div class="separator-2"></div>
                    <?php echo $content; ?>
                </div>

                <!-- sidebar start -->
                <!-- ================ -->
                <aside class="col-md-4 col-lg-3 col-md-pull-8 col-lg-pull-9">
                    <div class="sidebar">
                        <? SideBarBlock::begin(['title' => 'Профиль']) ?>
                        <div class="media sidebar-service-margin">
                            <div class="media-body sidebar-service-name">
                                <h6 class="media-heading">
                                    <?php echo Html::a('Редактирование профиля',['/personal/default/profile']) ?>
                                </h6>
                            </div>
                        </div>
                        <div class="media sidebar-service-margin">
                            <div class="media-body sidebar-service-name">
                                <h6 class="media-heading">
                                    <?php echo Html::a('Тикеты',['/personal/ticket/']) ?>
                                </h6>
                            </div>
                        </div>
                        <div class="media sidebar-service-margin">
                            <div class="media-body sidebar-service-name">
                                <h6 class="media-heading">
                                    <?php echo Html::a('Пополнение баланса',['/personal/default/payment']) ?>
                                </h6>
                            </div>
                        </div>
                        <? SideBarBlock::end() ?>
                    </div>
                </aside>
                <!-- sidebar end -->

            </div>
        </div>
    </section>

<?php $this->endContent(); ?>