<?php
/* @var $this \yii\web\View */
//use common\models\ServiceCategory;
//use common\widgets\SideBarBlock;
//use common\widgets\SideBarNews;
//use common\widgets\SideBarWarrant;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;

$this->beginContent('@frontend/views/layouts/main.php'); ?>
    <section class="wrap main-container">

        <div class="container">
            <div class="row">
                <div class="main col-md-10 col-lg-offset-1">
                    <h1 class="page-title"><?= Html::encode($this->title) ?></h1>
                    <?php echo $content; ?>
                </div>

                <!-- sidebar start -->
                <!-- ================ -->

                <!-- sidebar end -->

            </div>
        </div>
    </section>

<?php $this->endContent(); ?>