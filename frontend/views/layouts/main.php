<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?//подтветждение владения интеркассой?>
    <meta name="interkassa-verification" content="dd77ab05086046bfeff085ef0aa0909c" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<?php
NavBar::begin([
    'brandLabel' => Html::img(['/img/logo.png']),
    'brandUrl' => Yii::$app->homeUrl,
    'clientOptions' => [
        ''
    ],
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
$menuItemsHeader = [
    ['label' => 'Услуги', 'url' => ['/services/index']],
    ['label' => 'FAQ', 'url' => ['/site/faq']],
    ['label' => 'Правила', 'url' => ['/static-page/view', 'slug' => 'rules']],
    ['label' => 'Корзина', 'url' => ['/order']],
    ['label' => 'Бонусы', 'url' => ['/site/bonus']],
    ['label' => 'Контакты', 'url' => ['/static-page/view','slug'=>'kontakty']]
];
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right navbar-menu'],
    'items' => $menuItemsHeader,
]);

if (Yii::$app->user->isGuest) {
    $menuItems = [
        [
            'label' => Html::tag('i', '', ['class' => 'fa fa-user']) . ' Войти',
            'url' => ['/site/login'],
            'encode' => false
        ],
        [
            'label' => Html::tag('i', '', ['class' => 'fa fa-home']) . ' Зарегистрироваться',
            'url' => ['/site/signup'],
            'encode' => false
        ]
    ];
} else {
    $menuItems = [
        [
            'label' => 'Личный кабинет',
            'url' => ['/personal']
        ],
        [
            'label' => Html::tag('i', '', ['class' => 'fa fa-sign-out']) . ' Выйти (' . Yii::$app->user->identity->username . ')',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post'],
            'encode' => false
        ]
    ];
}
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right navbar-login'],
    'items' => $menuItems
]);

NavBar::end();

echo $content;
?>

<footer class="footer">
    <div class="container">
        <div class="row logo">
            <div class="col-md-12 text-center">
                <img src="/img/logo-footer.png" alt="">
            </div>
        </div>
        <div class="row social">
            <div class="text-center col-md-12">
                <?=\frontend\widgets\WebMoneyAccountWidget::widget(['bl_url'=>'//bl.wmtransfer.com/img/bl/507570065339?w=45h=18bg=0XDBE2E9'])?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-xs-9 col-xs-offset-1">
                <?= Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right navbar-menu','style'=>'margin-left:0px;'],
                    'items' => $menuItemsHeader
                ]) ?>
            </div>
        </div>
        <div class="row social">
            <div class="text-center col-md-12">
                <a href="">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="">
                    <i class="fa fa-facebook"></i>
                </a>
            </div>
        </div>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter36285770 = new Ya.Metrika({
                            id:36285770,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/36285770" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
        <div class="row copyright">
            <div class="text-center col-md-12">
                Copyright &copy; <?= date('Y') ?> Developed by <a href="http://red-flag.ru" target="_blank">Red-flag.ru</a>
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
