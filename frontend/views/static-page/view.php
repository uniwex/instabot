<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StaticPage */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="conainer wrap">
<?= $model->content; ?>
</div>


