<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 11.02.2016
 * Time: 15:29
 */
$this->title = 'Заказ успешно оплачен'
?>
<div class="form-group">
    <? if (Yii::$app->session->hasFlash('link')): ?>
        <div class="alert alert-info">
            <?= Yii::$app->session->getFlash('link'); ?>
        </div>
    <? endif; ?>
</div>
