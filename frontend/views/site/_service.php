<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 09.02.2016
 * Time: 9:53
 */

use yii\helpers\Html;

/* @var $model common\models\Service */

$counter = Yii::$app->session['serviceCounter'];
if ($counter % 2 == 0) $class = 'service col-md-5 col-xs-12';
else $class = 'service col-md-5 col-md-offset-2 col-xs-12';
$counter++;
Yii::$app->session['serviceCounter'] = $counter;

?>

<div class="<?= $class ?>">
    <? if ($model->is_hit) echo Html::img(['/img/hit.png']) ?>
    <p><?= $model->title_ru ?></p>
    <?= Html::button('КУПИТЬ', [
        'class' => 'btn btn-buy',
        'data-toggle' => 'modal',
        'data-target' => '.bs-example-modal-lg',
        'data-method' => $model->id
    ]) ?>
</div>