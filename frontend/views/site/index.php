<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\ListView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $carousel [] common\models\Carousel */

$this->title = 'TurboInsta';
$this->registerJsFile('@web/js/form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$active = 'active';
$i = 0;
?>

<section id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner">

        <? foreach ($carousel as $item): ?>
            <div class="item <?= $i == 0 ? $active : '' ?>">
                <?= Html::img($item->getImageFileUrl('picture')) ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center" style="height: 600px">
                            <p class="offer-1 hidden-xs">
                                <?= $item['text'] ?>
                            </p>

                            <h1 class="offer-2"><?= $item['title'] ?></h1>

                            <p class="text-white">Актуально до <?= $item['actually']?></p>
                            <button class="<?= $item['button_class'] ?>" data-toggle="modal"
                                    data-target="<?= $item['button_href'] ?>"
                                    data-method="<?= $item['button_service'] ?>"
                                    data-tittle="<?= \common\models\Service::find()->where('id='.$item['button_service'])->one()->title_ru?>"
                                    data-mincount="<?= \common\models\Service::find()->where('id='.$item['button_service'])->one()->min_count?>"
                                    data-regular="<?= \common\models\Service::find()->where('id='.$item['button_service'])->one()->regular_for_field?>">
                                <?= $item['button_text'] ?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <? $i++; ?>
        <? endforeach; ?>
    </div>

    <!-- Controls -->

    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <img src="/img/arrow-left.png" alt="">
    </a>


    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <img src="/img/arrow-right.png" alt="">
    </a>

</section>

<section id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="text-center">
                    Сервис turbo-insta.ru предлагает услуги по самой быстрой и незатратной офферной накрутке
                    инстаграм.<br>
                    Работая с августа 2012 года, мы успешно выполнили более 120 тыс. заказов. У нас самые низкие цены
                    в<br>
                    рунете, самые высокие скорости и качественные офферы!
                </p>

                <h1 class="text-center">НАШИ РАБОТЫ</h1>
            </div>
        </div>

        <div class="row slick">

            <?
            $pictures = [];
            $count = 0;
            foreach (glob(Yii::getAlias('@webroot')."/img/our_works/*.png") as $Picture)
            {
                $pictures[$count]['pic_url'] = str_replace(Yii::getAlias('@webroot'),'',$Picture);
                $pictures[$count]['name'] = str_replace('.png','',str_replace(Yii::getAlias('@webroot')."/img/our_works/",'',$Picture));
                $pictures[$count]['insta_link'] = "https://www.instagram.com/".$pictures[$count]['name']."/";
                $count++;
            } ?>
            <?foreach($pictures as $pic):?>
            <article class="col-md-4 col-xs-12">
                <?
                    $img = Html::img($pic['pic_url'],['style'=>'height:310px;']);
                    $name = Html::tag('div',Html::tag('h1',$pic['name']),['class'=>'work-info','style'=>'cursor:pointer;']);
                    echo Html::a($img,$pic['insta_link'],['target'=>'_blank','style'=>'text-decoration:none;']);
                    echo $name;
                ?>
            </article>
            <?endforeach;?>
        </div>
    </div>
</section>

<section id="service">
    <div class="container">
        <div class="row">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '/services/_service',
                'layout' => '{items}'
            ]) ?>
        </div>
    </div>
</section>


<?
$script = <<<JS
$('.slick').slick({
  autoplay: true,
  arrows: false,
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
JS;
$this->registerJs($script);
?>

<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <? if (Yii::$app->user->isGuest || !User::isBaned()) { ?>
                <div class="modal-body">
                    <h4 id="service_tittle">На этой странице вы можете заказать накрутку Instagram</h4>
                    <? $form = ActiveForm::begin([
                        'id' => 'buy_form'
                    ]) ?>
                    <div class="row no-margin">
                        <div class="col-md-5">


                            <?= Html::activeHiddenInput($model, 'service_id') ?>

                            <?= $form->field($model, 'name')->textInput() ?>

                            <?= $form->field($model, 'field')->textInput(['placeholder' => 'https://www.instagram.com/name.account/'])->label('Куда накрутить') ?>

                            <?= $form->field($model, 'count')->input('number', ['id'=>'min_count','min' => 0, 'step' => 1])->label('Сколько накрутить') ?>

                            <!-- <div class="form-group modal-garanty">
                                                            <? //= $form->field($model, 'guaranty')
                                //                                ->radioList(['Без гарантии', '30 суток', '60 суток'], [
                                //                                    'class' => 'col-md-6 radio',
                                //                                    'item' => function ($index, $label, $name, $checked, $value) {
                                //                                        $attr = '';
                                //                                        if($index == 0) $attr = 'checked';
                                //                                        $return = '<input type="radio" id="option' . $index . '" name="' . $name . '" value="' . $value . '" '.$attr.'>';
                                //                                        $return .= '<label for="option' . $index . '">';
                                //                                        $return .= $label;
                                //                                        $return .= '</label>';
                                //                                        return $return;
                                //                                    }
                                //                                ])
                                //                                ->label(null, ['class' => 'col-md-6']) ?>
                                <p class="modal-notice">
                                    Минимальный заказ услуг "С гарантией" - <br>3000 подписчиков! Подробнее здесь.
                                </p>
                            </div> -->
                        </div>
                        <div class="col-md-7">
                            <div class="panel panel-default">
                                <p>
                                    Воспользуйтесь бесплатно нашей новой уникальной услугой —
                                    выставление лимитов на выполнение заданий. Выставив лимиты,
                                    люди будут выполнять Ваше задание равномерно в течение суток.
                                    Таким образом накрутка будет похожа на естественные подписки
                                    заинтересованных пользователей.<br>
                                    Как правильно выставить лимиты — читайте <?=Html::a('тут',['/site/faq'])?>.
                                </p>
                                <?= $form->field($model, 'limit1')->input('number') ?>
                                <?= $form->field($model, 'limit5')->input('number') ?>
                            </div>
                            <div class="checkbox">
                                <?= $form->field($model, 'rules', ['template' => "{input}\n{label}\n{error}"])->input('checkbox'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row no-margin">
                        <div class="col-md-5">
                            <div class="form-group">
                                <h3 class="text-center">Сумма заказа: <span id="summary_money">0</span> руб.</h3>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <h3>
                                    <?= Html::submitButton('ЗАКАЗАТЬ', ['class' => 'btn btn-buy']) ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <? ActiveForm::end() ?>
                </div>
            <? } elseif(User::isBaned()) { ?>
                <div class="modal-body">
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"></i> Ваш аккаунт заблокирован.
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
</div>