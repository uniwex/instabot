<?php

/* @var $this yii\web\View */

use common\models\Faq;
use yii\helpers\Html;

$this->title = 'Часто задаваемые вопросы (FAQ)';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel-group collapse-style-2" id="accordion-2">
    <?php
    $numberElement = 0;
    /* @var $faq Faq */
    foreach($model as $faq):
    $numberElement++;
    ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse"
                   data-parent="#accordion-<?php echo $numberElement;?>"
                   href="#collapseTwo-<?php echo $numberElement;?>"
                   class="collapsed"
                   aria-expanded="false">
                    <i class="fa fa-question-circle pr-10"></i>
                    <?php echo strip_tags($faq->question);?>
                </a>
            </h4>
        </div>
        <div id="collapseTwo-<?php echo $numberElement;?>"
             class="panel-collapse collapse"
             aria-expanded="false"
             style="height: 0px;">
            <div class="panel-body">
                <?php echo $faq->answer;?>
            </div>
        </div>
    </div>
    <?php endforeach;?>
</div>
