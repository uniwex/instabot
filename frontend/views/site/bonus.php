<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 12.02.2016
 * Time: 17:41
 */
use yii\grid\GridView;

?>

<div class="container wrap">
    <div class="row">
        <div class="col-md-12">
            <h1>Таблица бонусов</h1>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'summary' => '',
                'columns' => [
                    'sum',
                    [
                        'attribute' =>'percent',
                        'value' => function($data){
                            return $data->percent." %";
                        }
                    ]

                ]
            ]) ?>
        </div>
    </div>
    <p>Описание бонусов смотрите <?=\yii\helpers\Html::a('тут',['/static-page/opisanie-bonsuov'])?></p>
</div>
