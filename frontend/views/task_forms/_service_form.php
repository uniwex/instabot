<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use common\models\Criteria;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('@web/js/form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->title = 'Оформление заказа';
?>
<div class="container">
    <h1><?= $this->title ?></h1>
    <? if ($model->service->only_logged && Yii::$app->user->isGuest): ?>
        <div class="row">
            <div class="col-md-6 col-xs-6 col-sm-6">
                <p>Услуга только для зарегистрированных</p>
            </div>
        </div>
    <? else: ?>
        <?= \common\widgets\Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ]
        ]) ?>
        <?php $form = ActiveForm::begin([
            'id' => 'buy_form',
            'action' => ['services/start-task']
        ]); ?>
        <div class="row">
            <div class="col-md-6 col-xs-6 col-sm-6">
                <?= $form->field($model, 'field')->textInput()->label($model->service->field_name ? $model->service->field_name : 'Наименование') ?>

                <?= $form->field($model, 'count')->input('integer') ?>

                <?= $form->field($model, 'service_id')->hiddenInput()->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="form-group">
                    Сумма к оплате: <span id="summary_money">0.00</span> руб.
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-success', 'id' => 'service_send']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    <? endif; ?>
</div>