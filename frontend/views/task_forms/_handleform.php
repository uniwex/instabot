<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nkovacs\datetimepicker\DateTimePicker;
use common\models\servicemodels\AddOptions;
use common\models\Criteria;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model */
/* @var $form yii\widgets\ActiveForm */

$this->registerJsFile('@web/js/form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="service-form">
    <?php $form = ActiveForm::begin([
        'id' => 'buy_form',
        'action' => ['services/start-task']
    ]); ?>
    <div class="row">
        <div class="col-md-6 col-xs-6 col-sm-6">
            <?= $form->field($model, 'field')->textInput(['id' => 'custom_field'])->label($model->service->field_name ? $model->service->field_name : 'Наименование') ?>

            <?= $form->field($model, 'count')->input('integer', ['id' => 'service_count']) ?>

            <? if (Yii::$app->user->isGuest): ?>
                <?= $form->field($model, 'email')->textInput() ?>
            <? else : ?>
                <? $model->email = Yii::$app->user->identity->email ?>
                <?= $form->field($model, 'email')->hiddenInput()->label(false) ?>
            <? endif; ?>

            <?= $form->field($model, 'description')->textarea() ?>

            <?= $form->field($model, 'service_id')->hiddenInput()->label(false) ?>
        </div>
        <? if ($model->service->isset_time): ?>
            <div class="col-md-6 col-xs-6 col-sm-6">
                <?= $form->field($model, 'run_time')->widget(DateTimePicker::className(), [
                    'format' => 'Y-MM-dd HH:mm',
                ]) ?>
            </div>
        <? endif; ?>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12">
            <? if (isset($model->criteria[0])): ?>
                <?= $form->field($model, 'add_criteria')->checkbox(['id' => 'showparams']) ?>
            <? endif; ?>
            <div class="addparams" style="<? echo ($model->add_criteria) ? 'display: block;' : 'display: none;' ?>">
                <? foreach ($model->criteria as $criteria): ?>
                    <? if ($criteria->type == Criteria::SIMPLE_TEXT) { ?>
                        <?= $form->field($model, "criteria[values][$criteria->id]")->textInput() ?>
                    <? } else { ?>
                        <? if ($criteria->type == Criteria::COUNTRY) { ?>
                            <? echo $form->field($model, "criteria[values][$criteria->id]")->dropDownList(Criteria::getOptionArray($criteria->type), ['id' => 'id_country']); ?>
                        <? } else { ?>
                            <? if ($criteria->type == Criteria::CITY) { ?>
                                <? echo $form->field($model, "criteria[values][$criteria->id]")->widget(DepDrop::classname(), [
                                    'options' => ['id' => 'subcity-id'],
                                    'pluginOptions' => [
                                        'depends' => ['id_country'],
                                        'placeholder' => 'Выберите город',
                                        'url' => Url::to(['/services/city'])
                                    ]
                                ]); ?>
                            <? } else { ?>
                                <?= $form->field($model, "criteria[values][$criteria->id]")->dropDownList(Criteria::getOptionArray($criteria->type)); ?>
                            <? }
                        } ?>
                    <? } ?>
                <? endforeach; ?>
            </div>
            <div class="form-group">
                Сумма к оплате: <span id="summary_money">0.00</span> руб.
            </div>

            <div class="form-group">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>