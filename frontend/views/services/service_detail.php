<?php
use common\widgets\ServiceForm;

$this->title = 'Оформление заказа';
?>
<div class="wrap">
    <?= ServiceForm::widget(['service' => $service]); ?>
</div>