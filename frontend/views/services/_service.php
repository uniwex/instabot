<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 12.02.2016
 * Time: 18:03
 */

use yii\helpers\Html;

$counter = Yii::$app->session['counter'];
if ($counter % 2 == 0) $class = 'service col-md-5 col-xs-12';
else $class = 'service col-md-5 col-md-offset-2 col-xs-12';
$counter++;
Yii::$app->session['counter'] = $counter;

?>

<div class="<?= $class ?>">
    <? if ($model->is_hit) echo Html::img(['/img/hit.png']) ?>
    <p style="height: 100px"><?= $model->title_ru ?>
         <?/*= Yii::$app->user->isGuest ? round($model->price_unreg, 1) : round($model->price, 1) */?> <!--<i
            class="fa fa-rub"></i>--></p>
    <?//= Html::a('КУПИТЬ', ['/services/order/' . $model->slug], ['class' => 'btn btn-buy']) ?>
    <?= Html::button('КУПИТЬ', [
        'class' => 'btn btn-buy',
        'data-toggle' => 'modal',
        'data-target' => '.bs-example-modal-lg',
        'data-method' => $model->id,
        'data-tittle' => $model->title_ru,
        'data-mincount' => $model->min_count,
        'data-regular' => $model->regular_for_field,
    ]) ?>
</div>
