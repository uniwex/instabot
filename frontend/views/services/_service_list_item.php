<?php

use common\models\Service;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $service Service */
$price = Yii::$app->user->isGuest ? $service->price_unreg : $service->price;
?>

<div class="col-md-4 col-xs-4 col-sm-4 masonry-grid-item">
    <div class="listing-item white-bg bordered mb-20">
        <div class="overlay-container">
            <?php echo Html::img($service->getImageFileUrl('image')) ?>
            <?= Html::a('<i class="fa fa-link"></i>', ['services/order/' . $service->slug], ['class' => 'overlay-link']) ?>
        </div>
        <div class="body">
            <h3><?= Html::a($service->title_ru, ['services/order/' . $service->slug]) ?></h3>

            <div class="elements-list clearfix">
                <div class="col-md-4 col-xs-12 col-sm-12 no-padding">
                    <span class="price"><?= round($price, 2) ?> <i class="fa fa-rub"></i></span>
                </div>
                <div class="col-md-8 col-xs-12 col-sm-12 no-padding">
                    <?= Html::a('Заказать услугу<i class="fa fa-shopping-cart"></i>',
                        ['services/order/' . $service->slug],
                        ['class' => 'margin-clear btn btn-sm btn-default-transparent btn-animated']) ?>
                </div>
            </div>
        </div>
    </div>
</div>