<?php


use common\models\Service;
use common\models\ServiceCategory;


/* @var $this yii\web\View */
/* @var $services Service[] */
/* @var $category ServiceCategory */
?>

<div class="tab-pane <?= $active ?>" id="<?= $category->slug ?>">
    <div class="row masonry-grid-fitrows grid-space-10">
        <?php
        /* @var $service Service */
        foreach ($services as $service): ?>
            <?= $this->render('_service_list_item', [
                'service' => $service,
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>
