<?php
use common\models\User;
use common\widgets\SelectService;
use common\models\Service;
use common\models\ServiceCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

/**
 * @var $services Service
 * @var $dataProvider yii\data\ActiveDataProvider
 */
$this->title = 'Услуги';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/form.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<!--<div class="row">-->
<!--    --><?//= ListView::widget([
//        'dataProvider' => $dataProvider,
//        'itemView' => '_index',
//        'layout' => '{items}',
//        'options' => [
//            'tag' => 'div',
//            'class' => 'main col-md-12'
//        ],
//        'itemOptions' => [
//            'tag' => 'div',
//            'class' => 'image-box style-4'
//        ]
//    ]) ?>
<!--</div>-->

<section id="service-2" class="wrap">
    <div class="container">
        <div class="row">
            <h1 class="text-white text-center">Услуги</h1>
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_service',
                'layout' => '{items}'
            ]) ?>
        </div>
    </div>
</section>

<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <? if (Yii::$app->user->isGuest || !User::isBaned()) { ?>
                <div class="modal-body" style="padding-right:50px;padding-left:50px;">
                    <h4 id="service_tittle">На этой странице вы можете заказать накрутку Instagram</h4>
                    <? $form = ActiveForm::begin([
                        'id' => 'buy_form'
                    ]) ?>
                    <div class="row no-margin">
                        <div class="col-md-5">


                            <?= Html::activeHiddenInput($model, 'service_id') ?>

                            <?= $form->field($model, 'name')->textInput() ?>

                            <?= $form->field($model, 'field')->textInput(['placeholder' => 'https://www.instagram.com/name.account/'])->label('Куда накрутить') ?>

                            <?= $form->field($model, 'count')->input('number', ['id'=>'min_count','min' => 0, 'step' => 1])->label('Сколько накрутить') ?>

                            <!--       <div class="form-group modal-garanty">
                                                     <? //= $form->field($model, 'guaranty')
                                //                                ->radioList(['Без гарантии', '30 суток', '60 суток'], [
                                //                                    'class' => 'col-md-6 radio',
                                //                                    'item' => function ($index, $label, $name, $checked, $value) {
                                //                                        $attr = '';
                                //                                        if($index == 0) $attr = 'checked';
                                //                                        $return = '<input type="radio" id="option' . $index . '" name="' . $name . '" value="' . $value . '" '.$attr.'>';
                                //                                        $return .= '<label for="option' . $index . '">';
                                //                                        $return .= $label;
                                //                                        $return .= '</label>';
                                //                                        return $return;
                                //                                    }
                                //                                ])
                                //                                ->label(null, ['class' => 'col-md-6']) ?>
                            </div>  -->
                        </div>
                        <div class="col-md-7">
                            <div class="panel panel-default">
                                <p>
                                    Воспользуйтесь бесплатно нашей новой уникальной услугой —
                                    выставление лимитов на выполнение заданий. Выставив лимиты,
                                    люди будут выполнять Ваше задание равномерно в течение суток.
                                    Таким образом накрутка будет похожа на естественные подписки
                                    заинтересованных пользователей.<br>
                                    Как правильно выставить лимиты — читайте <?=Html::a('тут',['/site/faq'])?>.
                                </p>
                                <?= $form->field($model, 'limit1')->input('number') ?>
                                <?= $form->field($model, 'limit5')->input('number') ?>
                            </div>
                            <div class="checkbox">
                                <?= $form->field($model, 'rules', ['template' => "{input}\n{label}\n{error}"])->input('checkbox'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row no-margin">
                        <div class="col-md-5">
                            <div class="form-group">
                                <h3 class="text-center">Сумма заказа: <span id="summary_money">0</span> руб.</h3>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <h3>
                                    <?= Html::submitButton('ЗАКАЗАТЬ', ['class' => 'btn btn-buy']) ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <? ActiveForm::end() ?>
                </div>
            <? } elseif(User::isBaned()) { ?>
                <div class="modal-body">
                    <div class="alert alert-danger">
                        <i class="fa fa-exclamation-triangle"></i> Ваш аккаунт заблокирован.
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
</div>