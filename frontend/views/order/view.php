<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 03.02.2016
 * Time: 14:23
 */
use yii\grid\GridView;
use common\helpers\StatusHelper;

/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Просмотр заказа';
$this->params['breadcrumbs'][] = [
    'label' => $this->title
]
?>

<div class="order-view">
    <? if ($dataProvider->getCount()): ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'service.title_ru',
                    'content' => function ($model) {
                        return $model->service->title_ru;
                    }
                ],
                [
                    'label' => 'Объект',
                    'content' => function ($model) {
                        $fields = json_decode($model->fields);
                        return "<a target='_blank' href='" . $fields->field . "'>Ссылка</a>";
                    }
                ],
                [
                    'label' => 'Количество',
                    'content' => function ($model) {
                        $fields = json_decode($model->fields);
                        return isset($fields->count) ? $fields->count : 0;
                    }
                ],
                [
                    'label' => 'Стоимость',
                    'content' => function ($model) {
                        $json = json_decode($model->fields);
                        return isset($json->summary_price) ? $json->summary_price : 0;
                    }
                ],
             /*   [
                    'label' => 'Прогресс',
                    'content' => function ($model) {
                        $jsonTaskInfo = json_decode($model->task_info);
                        $jsonFields = json_decode($model->fields);
                        if (isset($jsonTaskInfo->complete)) {
                            $jsonTaskInfo->complete == 0 ?
                                $percent = 0 :
                                $percent = (100 * $jsonTaskInfo->complete) / $jsonFields->count;
                        } else $percent = 0;
                        return '<div class="progress">
                              <div class="progress-bar" role="progressbar" style="width:' . $percent . '%">
                                <span class="sr-only">' . $percent . '% Complete</span>
                              </div>
                            </div>';
                    }
                ],*/
                [
                    'label' => 'Статус',
                    'content' => function ($model) {
                        return StatusHelper::getDescription($model->current_status);
                    }
                ],
            ],
        ]); ?>
    <? else: ?>
        <p>Заказ не найден</p>
    <? endif?>

</div>
