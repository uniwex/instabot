<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 03.02.2016
 * Time: 10:57
 */
use bookin\walletone\WalletOne;
use bookin\walletone\WalletOneButton;
use common\models\RunModel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\PaymentTypeHelper;

/* @var $models [] common\models\RunModel */
/* @var $totalPrice float */

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = [
    'label' => $this->title
];
?>
<div class="order-index">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Название услуги</th>
            <th>Количество</th>
            <th>Стоимость</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <? if (count($models) > 0): ?>

            <? foreach($models as $key => $service): ?>
                <tr>
                    <td><?= $service->service->title_ru ?></td>
                    <td><?= $service->count ?></td>
                    <td><?= $service->count * (Yii::$app->user->isGuest ? $service->service->price_unreg :
                            $service->service->price) ?></td>
                    <td><?= Html::a('<i class="fa fa-times"></i>', ['/basket/delete', 'id' => $key]) ?></td>
                </tr>
            <? endforeach; ?>
        <? else: ?>
            <tr>
                <td colspan="5">Корзина пуста</td>
            </tr>
        <? endif; ?>
        <thead>
        <tr>
            <th class="text-right" colspan="5">Всего к оплате: <?= $totalPrice ?> <i class="fa fa-rub"></i></th>
        </tr>
        </thead>
    </table>
    <? if (count($models) > 0): ?>
        <div class="row">
            <div class="col-md-12">
                <? if(Yii::$app->user->isGuest):?>
                <div class="form-group">
                    <? $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'type')->dropDownList(PaymentTypeHelper::getUserPaymentType()) ?>
                    <?= Html::submitButton('Оплатить', ['class' => 'btn btn-success']) ?>
                    <? ActiveForm::end(); ?>
                </div>
                <? else:?>
                <div class="form-group">
                    <?= Html::a('Оплатить',['basket/buy'],['class' => 'btn btn-success'])?>
                </div>
                <? endif;?>
               <!-- <div class="form-group">
                    <?/* $form = ActiveForm::begin(); */?>
                    <?/*= $form->field($coupon_form, 'coupon')->textInput(['max'=>29]) */?>
                    <?/*= Html::submitButton('Оплатить купоном', ['class' => 'btn btn-success']) */?>
                    <?/* ActiveForm::end(); */?>
                </div>-->
            </div>
        </div>

    <? endif; ?>
</div>





