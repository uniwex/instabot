/**
 * Created by димон on 15.01.2016.
 */
$('#min_count, #service_id').change(function() {
    change();
});

function change(){
    data = $('#buy_form').serializeArray();

    $.post('/services/sum-money', data, function (res) {
        $('#summary_money').html(res);
    });
}

var flag = false;

$('#service_send').click(function(event) {
    event.preventDefault();
    data = $('#buy_form').serializeArray();
    if (flag == false) {
        flag = true;
        $.post('/services/check-task', data, function (res) {
            if (res.result == "ok") {
                flag = false;
                $('#buy_form').submit();
            } else {
                flag = false;
                alert(res.result);
                return false;
            }
        }, 'json');
    }
});

$('button[data-toggle=modal]').click(function(){
    console.log('run');
    $('#buy_form')[0].reset();
    $('#summary_money').html(0);
    $('#runmodel-service_id').val($(this).attr('data-method'));
    $('#service_tittle').html($(this).attr('data-tittle'));
    $('#min_count').prop('min',$(this).attr('data-mincount'));
    $('#runmodel-field').prop('pattern',$(this).attr('data-regular'));
    console.log('end');
    //$.post('/services/sum-money', { id: $(this).attr('data-method') }, function(data) {
    //    data = JSON.parse(data);
    //    var option = '', i;
    //    for(i = 0; i < data.length; i++) {
    //        option += '<option value="' + data[i].id + '">' + data[i].title + '</option>';
    //    }
    //    if($('select#service_id').length == 0) {
    //        $('.modal-garanty').prepend('<div class="form-group"><label for="service_id" class="control-label">С гарантией</label>' +
    //            '<select class="form-control" id="service_id" onchange="change()" name="RunModel[service_id]">' + option + '</select></div>');
    //    }
    //});
});