$(document).ready(function(){
    $('.previews img').click(function(e) {
        var id = $(this).data('id');
        $('.main-preview img').hide();
        $('.main-preview').find('img[data-id="'+id+'"]').show();
        e.preventDefault();
    });

    $('[data-action="basket"]').click(function(e){
        e.preventDefault();
        var id = parseInt($(this).data('id'));
        var type = $(this).data('type');
        var count = 1;

        if ($('[data-type="quantity"]').length > 0) {
            var c = parseInt($('[data-type="quantity"]').val());
            if (c > 0)
                count = c;
        }

        if (type) {
            count = parseInt($(this).parents('.product-item').find('input[data-type="quantity"]').val());
        }
        addItem(id, count);
    });

    $('[data-type="count"]').click(function(e){
        e.preventDefault();

        var id = $(this).data('id');
        var count = $(this).val();
        updateCount(id, count);
    }).keypress(function(event) {
        if (event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46) {
            return true;
        }
        else if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    }).keyup(function(e){
        var id = $(this).data('id');
        var count = $(this).val();
        updateCount(id, count);
    });

    $('[data-type="basket-remove"]').click(function(e) {
        var id = $(this).parents('tr[data-type="basket-tr"]:eq(0)').data('id');
        deleteItem(id);
        e.preventDefault();
    });

    $('.selector input').keyup(function(e) {
        var quantity = $(this).val();
        var parent = $(this).parent().parent();
        var height = parent.data('height');
        var width = parent.data('width');
        var count = parent.data('count');

        if ($(this).data('type') == 'square') {
            var itemCount = getItemCount(height, width, count, quantity);
            parent.find('input[data-type="quantity"]').val(itemCount);
        } else {
            var s = getSquare(height, width, count, quantity);
            parent.find('input[data-type="square"]').val(s);
        }
        e.preventDefault();
    });

    $('.product-selector').each(function(){
        var elm = $(this).find('.double-selector');
        var height = elm.data('height');
        var width = elm.data('width');
        var count = elm.data('count');
        $(this).find('input[data-type="square"]').val(
            getSquare(height, width, count, 1)
        );
        $(this).find('input[data-type="quantity"]').val(1);
    });

    $('[data-id="order-create-form"]').submit(function(e){
        if ('yaCounter33028509' in window) {
            yaCounter33028509.reachGoal('order_event');
            yaCounter33028509.reachGoal('order_create');
        }
    });

    $('[data-id="order-create-button"]').click(function(e){
        if ('yaCounter33028509' in window) {
            yaCounter33028509.reachGoal('order');
        }
    });

    $('tr[data-type="basket-tr"] input[data-calc="1"]').each(function(i){
        var s = parseFloat($(this).data('square'));
        s = Math.ceil((s * $(this).val())*100)/100;
        $(this).parent().find('.square-label').text(s + ' м2');
    }).keyup(function(e){
        var s = parseFloat($(this).data('square'));
        s = Math.ceil((s * $(this).val())*100)/100;
        $(this).parent().find('.square-label').text(s + ' м2');
    }).change(function(e){
        var s = parseFloat($(this).data('square'));
        s = Math.ceil((s * $(this).val())*100)/100;
        $(this).parent().find('.square-label').text(s + ' м2');
    });

    $('a[data-toggle="filters"]').click(function(e){
        $('.category-filter').toggleClass('hidden-xs hidden-sm');
        e.preventDefault();
    });

    $('.block-list').find('input[type="checkbox"]:checked').each(function(){
        var parent = $(this).parent().parent();
        $(this).parent().prependTo(parent);
    });
});

function maxHeight(selector) {
    return Math.max.apply(null, $(selector).map(function () {
        return $(this).height();
    }).get());
}

function deleteItem(id) {
    $.get('/basket/delete/',{'id':id}, function(data){
        var elm = $('tr[data-type="basket-tr"][data-id="'+id+'"]');
        if (elm.length > 0) {
            elm.remove();
        }
        parseResponse(data);
    }, 'json');
}

function updateCount(id, count) {
    $.get('/basket/set/',{'id':id, 'count':count}, function(data){
        parseResponse(data);
    }, 'json');
}

function addItem(id, count) {
    count = count || 1;
    if ('yaCounter33028509' in window) {
        yaCounter33028509.reachGoal('basket_event');
        yaCounter33028509.reachGoal('basket');
    }
    $.get('/basket/add/',{'id':id, 'count':count}, function(data){
        if (data.success)
            showMessage('Товар успешно добавлен в корзину');

        parseResponse(data);
    }, 'json');
}

function parseResponse(data) {
    if (!data.success)
        return false;

    var sum = 0;

    if (data.models) {
        for (var i in data.models) {
            var sub = data.models[i].price * data.models[i].count;
            var id = data.models[i].id;
            var tr = $('[data-type="basket-tr"][data-id="'+id+'"]');
            tr.find('[data-type="count"]').val(data.models[i].count);
            tr.find('[data-type="total"] strong span').html(number_format(sub, 0, '.', ' '));
            sum += sub;
        }
        $('[data-type="order-sum"] strong span').html(number_format(sum, 0, '.', ' '));
    }
}

function getSquare(length, width, count, quantity) {
    count = count || false;
    quantity = quantity || false;
    if (!count && !quantity) {
        return (length * width).toFixed(2);
    } else {
        var len = length / 1000;
        var wid = width / 1000;
        return (len * wid * count * quantity).toFixed(2);
    }
}

function getItemCount(length, width, count, quantity) {
    var len = length / 1000;
    var wid = width / 1000;
    var s = len * wid * count;
    return Math.ceil(quantity / s);
}

function showMessage(message) {
    $.toast({
        text : message,
        heading: 'Уведомление:',
        bgColor: '#2ecc71',
        textColor: '#fff',
        position: 'top-right'
    });
}

function number_format( number, decimals, dec_point, thousands_sep ) {
    var i, j, kw, kd, km;
    if( isNaN(decimals = Math.abs(decimals)) ){
        decimals = 2;
    }
    if( dec_point == undefined ){
        dec_point = ",";
    }
    if( thousands_sep == undefined ){
        thousands_sep = ".";
    }
    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
    if( (j = i.length) > 3 )
        j = j % 3;
    else
        j = 0;
    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
    return km + kw + kd;
}