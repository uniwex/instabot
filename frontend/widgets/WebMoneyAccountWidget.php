<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets;

class WebMoneyAccountWidget extends \yii\bootstrap\Widget
{
    const IMAGE_ATTESTAT = '/img/bat130.png';

    public $bl_url;

    public $wm_id = '507570065339';

    public function run()
    {
        ?>
        <table style="margin: auto">
            <tr>
                <td>
                    <img src="<?= \Yii::getAlias('@web') . self::IMAGE_ATTESTAT ?>" width="57" height="57">
                </td>
                <td>
                    <!-- begin WebMoney Transfer : accept label -->
                    <a href="http://www.megastock.ru/" target="_blank"><img src="/img/acc_blue_on_white_ru.png"
                                                                            alt="www.megastock.ru" border="0"/></a>
                    <!-- end WebMoney Transfer : accept label -->
                </td>
                <td>
                    <!-- begin WebMoney Transfer : attestation label -->
                    <a href="https://passport.webmoney.ru/asp/certview.asp?wmid=<?=$this->wm_id?>" target="_blank"><img
                            src="/img/v_blue_on_white_ru.png"
                            alt="Здесь находится аттестат нашего WM идентификатора <?=$this->wm_id?>" border="0"/>
                        <br/>
                    </a>
                    <!-- end WebMoney Transfer : attestation label -->
                </td>
                <td>
                    <a href="https://www.interkassa.com/" title="INTERKASSA" target="_blank">
                        <img src="https://www.interkassa.com/img/ik_88x31_01.gif" alt="INTERKASSA" /></a>
                </td>
            </tr>
            <tr>
                <td>
                    BL: <img src="<?= $this->bl_url ?>" width="45" height="18" style="background-color: #ffffff">
                </td>
                <td>

                </td>
                <td>

                </td>
            </tr>

        </table>

        <?
    }
}
