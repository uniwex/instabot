<?php

namespace common\widgets;



use yii\bootstrap\Widget;
use yii\helpers\Html;

class SideBarBlock extends Widget
{

    public $title;
    public $titleTag = 'h3';

    public $titleOptions = [
        'class'=>'title',
    ];


    public $separatorContent = '';
    public $separatorTag = 'div';
    public $separatorClass = 'separator-2';

    public function init()
    {
        parent::init();

        echo '<div class="block clearfix">';
        echo Html::tag($this->titleTag,$this->title,$this->titleOptions);
        echo Html::tag($this->separatorTag,$this->separatorContent,['class'=>$this->separatorClass]);
    }

    public function run(){
        echo '</div>';
    }
}
