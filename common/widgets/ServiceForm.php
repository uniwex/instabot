<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets;

use common\models\User;
use Yii;
use common\models\Service;
use common\models\TaskRun;
use common\models\RunModel;

class ServiceForm extends \yii\bootstrap\Widget
{
    /**
     * @var $service Service
     */
    public $service;

    public function init()
    {
        // todo Переделать
        parent::init();
        $model = $this->service->returnModel();
        echo $this->render($model::FORM_PATH, ['model' => $model]);
    }
}
