<?php


namespace common\helpers;


use yii\helpers\Html;

class InstagramHelper
{
    public $userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 6.2; en-US; Valve Steam Tenfoot/1431729692; ) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.104 Safari/537.36';
    public $thisUser = 'test';
    public $noProxy = true;
    public $proxy = '';
    public $proxyType = 'SOCKS5';


    public function getCount($url){
        preg_match('/(http|https)(:\/\/)(www\.)?instagram\.com\/p\/.*/', $url, $url_instagram_post);
        preg_match('/(http|https):\/\/(www\.)?instagram\.com\/[\w\.\_]{2,}\/.*/', $url, $url_instagram_profile);

        if(isset($url_instagram_post[1])){
            preg_match('/"likes":{"count":([0-9]+)/', $this->request($url)[1], $matches);
            if (isset($matches[1])) {
                return $matches[1];
            } else return false;
        }elseif(isset($url_instagram_profile[1])){
            preg_match('/"followed_by":{"count":([0-9]+)}/', $this->request($url)[1], $matches);
            if (isset($matches[1])) {
                return $matches[1];
            } else return false;
        }else{
            return false;
        }

    }

    public function request($url, $post = false, $referer = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($referer != null) {
            curl_setopt($ch, CURLOPT_REFERER, $referer);
            curl_setopt($ch, CURLOPT_USERAGENT,
                'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36');
        } else
            curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if (!$this->noProxy) {
            if ($this->proxy) {
                if (preg_match('/@/', $this->proxy)) {
                    $temp = explode('@', $this->proxy);
                    curl_setopt($ch, CURLOPT_PROXY, $temp[1]);
                    curl_setopt($ch, CURLOPT_PROXYUSERPWD, $temp[0]);
                } else {
                    curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
                }
                curl_setopt($ch, CURLOPT_PROXYTYPE, $this->proxyType);
            }
        }
        if ($post) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }

        $resp = curl_exec($ch);
        $header_len = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($resp, 0, $header_len);
        $body = substr($resp, $header_len);
        curl_close($ch);
        return array($header, $body);
    }
}