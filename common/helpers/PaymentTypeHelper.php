<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 15.01.2016
 * Time: 13:05
 */

namespace common\helpers;

class PaymentTypeHelper
{
    //alias @common
    const STORAGE_FILE = '/config/advanced_options.json';

    CONST PAYMENT_TYPE_ROBOKASSA = 0;
    CONST PAYMENT_TYPE_WALLET_ONE = 1;
    CONST PAYMENT_TYPE_INTERKASSA = 2;
    CONST PAYMENT_TYPE_MANUAL = 3;
    CONST PAYMENT_TYPE_PARTNER = 4;

    private static $paymentType = [
        'RoboKassa',
        'Wallet One',
        'InterKassa',
        'Ручное пополнение',
        'Бонус от Партнерской программы',
    ];

    public static function getPaymentType()
    {
        return self::$paymentType;
    }

    public static function getUserPaymentType()
    {
        $type = self::$paymentType;

        $settings = json_decode(file_get_contents(\Yii::getAlias('@common').self::STORAGE_FILE),1);
        if (isset($settings['robokassa'])) { if ($settings['robokassa'] == 0) unset($type[0]); } else unset($type[0]);
        if (isset($settings['interkassa'])) { if ($settings['interkassa'] == 0) unset ($type[2]); } else unset($type[2]);
        if (isset($settings['wallet_one'])) { if ($settings['wallet_one'] == 0) unset($type[1]); } else unset($type[1]);
        /*unset($type[1]);
        unset($type[2]);*/
        unset($type[3]);
        unset($type[4]);
        return $type;
    }
}

