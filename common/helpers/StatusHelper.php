<?php

namespace common\helpers;

use Yii;


class StatusHelper
{
    const DECLARED = 0;
    const AT_WORK = 1;
    const FINISHED = 2;
    const BANNED = 3;
    const CANCELED = 4;
    const FREEZED = 5;
    const ERROR = 6;
    const MODERATED = 7;
    const PAUSED = 8;
    const AT_HANDLE_WORK = 10;

    public static $labels = [
        self::DECLARED => 'Обьявлена',
        self::AT_WORK => 'Выполняется',
        self::FINISHED => 'Выполнена',
        self::BANNED => 'Забанена',
        self::CANCELED => 'Отклонена модератором',
        self::FREEZED => 'Заморожена',
        self::ERROR => 'Ошибка сервиса',
        self::MODERATED => 'На модерации',
        self::PAUSED => 'Приостановлено',
        self::AT_HANDLE_WORK => 'Выполняется'
    ];

    public static function getDescription($status){
        return self::$labels[$status];
    }
}

?>

