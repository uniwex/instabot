<?php

namespace common\components\sms;

use yii\base\Component;
use Yii;

class ProstorSmsManager extends Component
{
    public $host = 'gate.prostor-sms.ru';
    public $port = '80';

    public $login;
    public $password;

    public $sender = false;
    public $wapurl = false;

    public function init()
    {
        parent::init();
    }

    function send($phone, $text, $sender = false, $wapurl = false )
    {
        $fp = fsockopen($this->host, $this->port, $errno, $errstr);
        if (!$fp) {
            return "errno: $errno \nerrstr: $errstr\n";
        }
        fwrite($fp, "GET /send/" .
            "?phone=" . rawurlencode($phone) .
            "&text=" . rawurlencode($text) .
            ($sender ? "&sender=" . rawurlencode($sender) : "") .
            ($wapurl ? "&wapurl=" . rawurlencode($wapurl) : "") .
            " HTTP/1.0\n");
        fwrite($fp, "Host: " . $this->host . "\r\n");
        if ($this->login != "") {
            fwrite($fp, "Authorization: Basic " .
                base64_encode($this->login. ":" . $this->password) . "\n");
        }
        fwrite($fp, "\n");
        $response = "";
        while(!feof($fp)) {
            $response .= fread($fp, 1);
        }
        fclose($fp);
        list($other, $responseBody) = explode("\r\n\r\n", $response, 2);
        return $responseBody;
    }



    function status($sms_id)
    {
        $fp = fsockopen($this->host, $this->port, $errno, $errstr);
        if (!$fp) {
            return "errno: $errno \nerrstr: $errstr\n";
        }
        fwrite($fp, "GET /status/" .
            "?id=" . $sms_id .
            " HTTP/1.0\n");
        fwrite($fp, "Host: " . $this->host . "\r\n");
        if ($this->login != "") {
            fwrite($fp, "Authorization: Basic " .
                base64_encode($this->login. ":" . $this->password) . "\n");
        }
        fwrite($fp, "\n");
        $response = "";while(!feof($fp)) {
        $response .= fread($fp, 1);
    }
        fclose($fp);
        list($other, $responseBody) = explode("\r\n\r\n", $response, 2);
        return $responseBody;
    }
}
