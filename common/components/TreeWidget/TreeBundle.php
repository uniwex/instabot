<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 22.12.2015
 * Time: 12:44
 */

namespace common\components\TreeWidget;

class TreeBundle extends \yii\web\AssetBundle
{
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('css', ['css/treewidget']);
        $this->setupAssets('js', ['js/jquery.nestable']);
        parent::init();
    }

    /**
     * Set up CSS and JS asset arrays based on the base-file names
     *
     * @param string $type whether 'css' or 'js'
     * @param array $files the list of 'css' or 'js' basefile names
     */
    protected function setupAssets($type, $files = [])
    {
        $srcFiles = [];
        $minFiles = [];
        foreach ($files as $file) {
            $srcFiles[] = "{$file}.{$type}";
        }
        $this->$type = YII_DEBUG ? $srcFiles : $minFiles;
    }

    /**
     * Sets the source path if empty
     *
     * @param string $path the path to be set
     */
    protected function setSourcePath($path)
    {
        $this->sourcePath = $path;
    }
}