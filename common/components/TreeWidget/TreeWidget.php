<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 10.12.2015
 * Time: 20:27
 */

namespace common\components\TreeWidget;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\web\View;
use common\components\TreeWidget\models\Category;

class TreeWidget extends Widget
{
    /**
     * @var string - ID виджета
     */
    public $id = 'nestable';

    /**
     * @var string required - название таблицы категорий
     */
    public $tableCategory;

    /**
     * @var string required - название категории
     */
    public $title;

    /**
     * @var string required - название первичного ключа
     */
    public $pk;

    /**
     * @var string - тег для дерева
     */
    public $tagName = 'ol';

    /**
     * @var array - HTML - атрибуты
     */
    public $htmlOptions = [];

    /**
     * @var string - тег для элемента дерева
     */
    public $itemTagName = 'li';

    /**
     * @var array - HTML - атрибуты тега
     */
    public $itemOptions = [];

    /**
     * @var bool - отображение кнопок Показать / Скрыть
     */
    public $enableExpandBtn = true;

    public function init()
    {
        parent::init();
        if ($this->tableCategory === null) {
            throw new InvalidConfigException('The "tableCategory" property must be set.');
        }
        if ($this->pk === null) {
            throw new InvalidConfigException('The "pk" property must be set.');
        }
        if ($this->title === null) {
            throw new InvalidConfigException('The "title" property must be set.');
        }
        $this->itemOptions = array_merge($this->defaultItemOptions(), $this->itemOptions);
        $this->htmlOptions = array_merge($this->defaultHtmlOptions(), $this->htmlOptions);
    }

    public function run()
    {
        $nameTree = $this->tableCategory;
        $pk = $this->pk;
        $title = $this->title;
        $categories = $nameTree::find()->all();
        $countCategories = count($categories);

        if ($countCategories) {

            $this->registerTreeBundle();
            $this->registerAssets();
            $htmlOptions = $this->htmlOptions;

            if ($this->enableExpandBtn) echo $this->renderButton();

            echo Html::beginTag('div', ['id' => $this->id, 'class' => 'dd']);
            echo Html::beginTag($this->tagName, $htmlOptions); // ol
            for ($i = 0; $i < $countCategories; $i++) {
                $node = $this->hasChildren($categories[$i]);
                if ($node) {
                    echo Html::beginTag($this->itemTagName, array_merge($this->itemOptions, ['data-id' => $categories[$i]->$pk])); //li
                    echo Html::tag('div', 'Drag', ['class' => 'dd-handle dd3-handle']);
                    echo Html::tag('div', $categories[$i]->$title, ['class' => 'dd3-content']);
                    echo $this->renderEditButton($categories[$i]->$pk);
                    echo Html::beginTag($this->tagName, $htmlOptions);
                    foreach ($node as $item) {
                        echo $this->renderItem($item);
                    }
                    echo Html::endTag($this->tagName);
                    echo Html::endTag($this->itemTagName);
                } else {
                    $node = $this->hasParent($categories[$i]);
                    if ($node == false) echo $this->renderItem($categories[$i]);
                }
            }
            echo Html::endTag($this->tagName);
            echo Html::hiddenInput('', '', ['id' => $this->id . '-output']);
            echo Html::endTag('div');

            echo $this->renderModal();
        } else echo Html::tag('p', 'Категории не найдены', ['class' => 'text-center']);
    }

    public function renderButton()
    {
        $content =
        '<div class="js-'. $this->id .'-action">
            <a data-action="expand-all" class="btn btn-default btn-sm mr-sm">Показать всё</a>
            <a data-action="collapse-all" class="btn btn-default btn-sm">Скрыть всё</a>
        </div>';
        return $content;
    }

    public function renderEditButton($id)
    {
        $options = [
            'data-toggle' => 'modal',
            'data-target' => '#changeCategory'
        ];
        $content = Html::tag(
            'div',
            Html::tag('i', '', array_merge($options, ['class' => 'fa fa-pencil'])) .
            Html::tag('i', '', array_merge($options, ['class' => 'fa fa-times'])),
            [
                'class' => 'dd-edit',
                'data-id' => $id
            ]
        );
        return $content;
    }

    public function renderModal()
    {
        return '
        <div class="modal fade" id="changeCategory">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body"></div>
            </div>
          </div>
        </div>';
    }

    public function renderItem($item)
    {
        $pk = $this->pk;
        $title = $this->title;
        $content = Html::beginTag($this->itemTagName, array_merge($this->itemOptions, ['data-id' => $item->$pk]));
        $content .= Html::tag('div', 'Drag', ['class' => 'dd-handle dd3-handle']);
        $content .= Html::tag('div', $item->$title, ['class' => 'dd3-content']);
        $content .= $this->renderEditButton($item->$pk);
        $content .= Html::endTag($this->itemTagName);
        return $content;
    }

    public function defaultItemOptions()
    {
        return [
            'class' => 'dd-item dd3-item'
        ];
    }

    public function defaultHtmlOptions()
    {
        return [
            'class' => 'dd-list',
        ];
    }

    public function registerTreeBundle()
    {
        $view = $this->getView();
        TreeBundle::register($view);
    }

    public function registerAssets()
    {
        $nameTree = $this->tableCategory;
        $treeEdit = $nameTree::TREE_EDIT;
        $categoryUpdate = $nameTree::CATEGORY_UPDATE;
        $categoryDelete = $nameTree::CATEGORY_DELETE;
        $script = <<<JS
(function (window, document, $, undefined) {
    $(function () {

        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output'),
                data;
            if (window.JSON) {
                data = window.JSON.stringify(list.nestable('serialize'));
                output.val(data);
                $.ajax({
                    url: '$treeEdit',
                    data: {
                        json: data
                    }
                })
            }
        };
        // activate Nestable for list 1
        $('#$this->id')
            .nestable({
                group: 1
            })
            .on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#$this->id').data('output', $('#$this->id-output')));

        $('.dd-edit i').on('click', function() {
            var id = $(this).parent().attr('data-id'),
                action,
                url,
                body = $('#changeCategory .modal-body'),
                title = $('#changeCategory .modal-title'),
                preloader =
                    '<div class="preloader text-center">' +
                        '<img src="/backend/web/themes/adminlte/img/preloader.gif">' +
                    '</div>';

            if($(this).hasClass('fa-pencil')) {
                action = '$categoryUpdate';
                title.html('Редактирование категории');
            }
            else if($(this).hasClass('fa-times')) {
                action = '$categoryDelete';
                title.html('Удаление категории');
            }

            body.html(preloader);
            $.ajax({
                url: action,
                data: { id: id },
                success: function (data) {
                    body.html(data);
                }
            });
        });
        $('.js-$this->id-action').on('click', function (e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });
    });
})(window, document, window.jQuery);
JS;
        $this->view->registerJs($script, View::POS_END);
    }

    private function hasChildren($category)
    {
        $children = $category->children()->all();
        if (count($children)) return $children;
        else return false;
    }

    private function hasParent($category)
    {
        $parent = $category->parent()->all();
        if (count($parent)) return $parent;
        else return false;
    }
}