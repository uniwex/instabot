<?php

namespace common\components\TreeWidget\models;

use Yii;
use common\components\UserClosureTable;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $description_ru
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property integer $sort
 * @property integer $active
 * @property string $slug
 * @property string $image
 * @property string $type
 */
class Category extends \yii\db\ActiveRecord
{
    CONST CATEGORY_UPDATE = '/admin/index.php?r=service-category/update-tree', // Редактирование категории
          CATEGORY_DELETE = '/admin/index.php?r=service-category/delete-tree', // Удаление категории
          TREE_EDIT = '/admin/index.php?r=service-category/edit-tree'; // Редактирование дерева

    public $new_owner = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => UserClosureTable::className(),
                'tableName' => 'category_tree'
            ]
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['description_ru'], 'string'],
            [['created_by', 'updated_by', 'sort', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_ru', 'slug', 'image', 'type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'description_ru' => 'Description Ru',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sort' => 'Sort',
            'active' => 'Active',
            'slug' => 'Slug',
            'image' => 'Image',
            'type' => 'Type',
        ];
    }

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }
}
