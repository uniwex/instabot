<?php

namespace common\components\TreeWidget\models;

use Yii;
use yii\db\ActiveQuery;
use valentinek\behaviors\ClosureTableQuery;

class CategoryQuery extends ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => ClosureTableQuery::className(),
                'tableName' => 'category_tree'
            ]
        ];
    }

    public function active($state = true)
    {
        return $this->andWhere(['active' => $state]);
    }

    public function slug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }
}