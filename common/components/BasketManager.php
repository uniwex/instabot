<?php
namespace common\components;
use yii\base\Component;
use Yii;


class BasketManager extends Component {

    private $_cart = [];


    /**
     * Инициализация
     */
    public function init()
    {
        $this->load();
    }

    /**
     * Добавить товар в корзину
     * @param $id
     * @param int $count
     */
    public function add($object)
    {
        array_push($this->_cart,$object);
        $this->save();
    }

    public function getCount()
    {
        return count($this->_cart);
    }

    /**
     * Сохранение корзины
     */
    public function save()
    {
        Yii::$app->session->set('basket',$this->_cart);
    }

    /**
     * Загрузка корзины
*/
    public function load()
    {
        $session = Yii::$app->session->get('basket');
        if ($session != null)
            $this->_cart = $session;
        else $this->_cart = [];
    }

    /**
     * Проверка на пустоту
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->_cart) == 0;
    }

    public function exist($id)
    {
        return array_key_exists($id, $this->_cart);
    }

    /**
     * @return array
     */
    public function listIds()
    {
        return array_keys($this->_cart);
    }


    /**
     * Очистка корзины
     */
    public function flush()
    {
        $this->_cart = [];
        $this->save();
    }

    /**
     * Удаление из корзины
     * @param $id
     * @param bool|false $count
     */
    public function delete($id)
    {
        if ($this->exist($id)) {
            unset($this->_cart[$id]);
            $this->save();
        }
    }

    public function getCountGoods(){
        return count($this->_cart);
    }

    public function getBasket(){
        return $this->_cart;
    }

    public function deleteService($id){
        foreach($this->_cart as $key => $cart) {

            if ($cart->service_id == $id) {
                unset($this->_cart[$key]);
            }
        }
        $this->save();
    }

    public function getPriceBasket() {
        $total = 0;
        foreach($this->_cart as $service) {
            $total += $service->getTotalPrice();
        }
        return $total;
    }
}