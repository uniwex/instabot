<?php
/**
 * Created by PhpStorm.
 * User: димон
 * Date: 27.01.2016
 * Time: 12:22
 */

namespace common\components;

use yii\base\Component;
use console\crawlers\VkCrawler;

class VkParser extends Component
{
    const USER = 0;
    const GROUP = 1;

    public static function parseWall($object = "http://vk.com/kladovayacomiksovv",$check_id = null){
        //$this->parseLink("https://vk.com/kladovayacomiksovv?w=wall-72884889_27725");
        $crawler = new VkCrawler();
        preg_match("#[https|http]\:\/\/vk\.com\/(.*)#",$object,$match);
        if (!isset($match[1])) return null;
        preg_match("#id([\d]+)#",$match[1],$matchID);
        if (isset($matchID[1])) {
            $object_id = $matchID[1];
            $object_type = self::USER;
        } else {
            preg_match("#club([\d]+)#",$match[1],$matchID);
            if (isset($matchID[1])) {
                $object_id = $matchID[1];
                $object_type = self::GROUP;
            } else {
                $data = $crawler->api('users.get', [
                    'user_ids' => $match[1],
                ]);
                if (isset($data['response'][0]['uid'])) {
                    $object_id = $data['response'][0]['uid'];
                    $object_type = self::USER;
                } else {
                    $data = $crawler->api('groups.getById', [
                        'group_ids' => $match[1],
                    ]);
                    if (isset($data['response'][0]['gid'])) {
                        $object_id = $data['response'][0]['gid'];
                        $object_type = self::GROUP;
                    } else {
                        return null;
                    }
                }
            }
        }

        if ($object_type) {
            $request_id = "-".$object_id;
        } else {
            $request_id = $object_id;
        }
        $data = $crawler->api('wall.get',[
            'owner_id' => $request_id
        ]);
        if (!isset($data['response'])) return null;
        $result = [];
        foreach($data['response'] as $post) {
            if (isset($post['id'])) {
                if ($check_id != null) {
                    if ($check_id < $post['id']) {
                        $result[] = "http://vk.com/wall" . $request_id . "_" . $post['id'];
                    }
                } else {
                    $result[] = "http://vk.com/wall" . $request_id . "_" . $post['id'];
                }
            }
        }
        return $result;
    }

    public static function parseLink($object) {
        if (preg_match("#wall(-?[\d]+_[\d]+)#",$object,$match)){
            preg_replace("#(%.*)#",'',$match[1]);
            $match[1] = str_replace('wall','',$match[1]);
            $explds = explode('_',$match[1]);
            $info = [];
            $info['gid'] = $explds[0];
            $info['post_id'] = $explds[1];
            $info['url'] = "http://vk.com/club".str_replace("-","",$explds[0]);
            return json_encode($info);
        } else return null;
    }
}