<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 13.12.2015
 * Time: 19:26
 */

namespace common\components;


use valentinek\behaviors\ClosureTable;

class UserClosureTable extends ClosureTable
{
    //TODO Change this method!!!

    public function prependTo($target)
    {
        $db = $this->owner->getDb();
        $childAttribute = $db->quoteColumnName($this->childAttribute);
        $parentAttribute = $db->quoteColumnName($this->parentAttribute);
        $depthAttribute = $db->quoteColumnName($this->depthAttribute);
        $tableName = $db->quoteTableName($this->tableName);
        $sql = (
            'INSERT INTO ' . $tableName . ' '
            . '('.$parentAttribute.','.$childAttribute.','.$depthAttribute.') '
            . 'VALUES ('.$this->owner->id.','.$target.','.$depthAttribute. '+1);'
        );
        return $db->createCommand($sql)->execute();
    }

    public function deleteChildren() {
        $db = $this->owner->getDb();
        return $db->createCommand()
            ->delete(
                $this->tableName,
                $this->childAttribute.' = :id and '.$this->childAttribute.' <> '.$this->parentAttribute,
                [':id' => $this->owner->primaryKey])
            ->execute();
    }

    public static function editTree($json, $table) {
        /*
         * @var $table yii\db\ActiveRecord
         */
        for($i = 0; $i < count($json); $i++) {
            $id = $json[$i]['id'];

            if(isset($json[$i]['children'])) {
                $children = $json[$i]['children'];
                for($j = 0; $j < count($children); $j++) {
                    $category = $table::findOne($children[$j]['id']);
                    $parent = (int) $category->parent()->count();
                    if(!isset($children[$j]['children'])) {
                        if($parent)
                            $category->moveTo($id);
                        else {
                            $parent = $table::findOne($id);
                            $parent->prependTo($children[$j]['id']);
                        }
                    }
                }
            }
            else {
                $category = $table::findOne($id);
                $category->deleteChildren();
            }
        }
    }
}