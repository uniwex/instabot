<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'account_type' => 1,
    'guest_id' => 1,
    'robokassa' => [
        'turboinst',
        'Aj7pFPLi09l5u1HWkyog',
        'dEjXXK1o7Oq82zxpM1NS'
    ],
    'robokassa_test' => [
        'turboinst',
        'vZJ8bOFpN79RvmGp18qX',
        'V4zVkWU7vu39DlG7PvUD'
    ]
];
