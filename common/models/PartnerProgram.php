<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partner_program".
 *
 * @property integer $referrer_id
 * @property integer $referral_id
 *
 * @property User $referral
 * @property User $referrer
 */
class PartnerProgram extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner_program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['referrer_id', 'referral_id'], 'required'],
            [['referrer_id', 'referral_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'referrer_id' => 'Referrer ID',
            'referral_id' => 'Referral ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferral()
    {
        return $this->hasOne(User::className(), ['id' => 'referral_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferrer()
    {
        return $this->hasOne(User::className(), ['id' => 'referrer_id']);
    }

    /**
     * @inheritdoc
     * @return PartnerProgramQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerProgramQuery(get_called_class());
    }
}
