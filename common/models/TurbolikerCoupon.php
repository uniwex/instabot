<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "turboliker_coupon".
 *
 * @property integer $id
 * @property string $coupon
 * @property integer $not_work
 */
class TurbolikerCoupon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'turboliker_coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['not_work'], 'integer'],
            [['coupon'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coupon' => 'Coupon',
            'not_work' => 'Not Work',
        ];
    }
}
