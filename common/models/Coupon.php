<?php

namespace common\models;

use Yii;

class Coupon extends \yii\db\ActiveRecord
{

     /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price'], 'required'],
            [['used'], 'integer'],
            [['coupon'], 'string', 'max' => 255],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coupon' => 'Купон',
            'price' => 'Стоимость'
        ];
    }

    public function beforeSave() {
        $key = md5(mktime()."citrb".rand(1,99999));
        $new_key = '';
        for($i=1; $i <= 25; $i ++ ){
            $new_key .= $key[$i];
            if ( $i%5==0 && $i != 25) $new_key.='-';
        }
        $this->coupon = strtoupper($new_key);
        return true;
    }
}
