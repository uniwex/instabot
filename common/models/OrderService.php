<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_service".
 *
 * @property integer $operation_id
 * @property integer $service_id
 *
 * @property Service $service
 * @property BillingOperationSale $operation
 */
class OrderService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operation_id', 'service_id'], 'required'],
            [['operation_id', 'service_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operation_id' => 'Operation ID',
            'service_id' => 'Service ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperation()
    {
        return $this->hasOne(BillingOperationSale::className(), ['id' => 'operation_id']);
    }

    /**
     * @inheritdoc
     * @return OrderServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderServiceQuery(get_called_class());
    }
}
