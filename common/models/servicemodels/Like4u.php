<?php
namespace common\models\servicemodels;

use common\helpers\InstagramHelper;
use common\models\TaskRun;
use common\helpers\StatusHelper;

/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 14.07.2015
 * Time: 12:54
 */
class Like4u extends BaseRunClass
{
    CONST
        logging = true,
        apiUrl = 'https://like4u.ru/';
    public
        $token = 'b833d65915937c47c03b6dbf7e98bf59',
        $error = [
        'login required' => 'Необходима авторизация пользователя: не передан токен или токен не существует.',
        'premium required' => 'Необходим премиум аккаунт.',
        'no money' => 'Недостаточно лайков на балансе аккаунта.',
        'no reals' => 'Недостаточно реалов на балансе аккаунта.',
        'api limit exceeded' => 'Превышен лимит на трату лайков. Лимиты на для каждого типа аккаунта можно посмотреть здесь: https://like4u.ru/users/premium',
        'Internal Server Error' => 'Internal Server Error'
    ];

    public function getUserInfo()
    {
        $response = $this->apiRequest('client/user_info.json?token=' . $this->token);
        if (isset($response['token']))
            return $response;
        else {
            $this->logging('Неверно указан токен');
            return false;
        }
    }

    public function apiRequest($method, $data = null)
    {
        if ($data) {
            $data['token'] = $this->token;
            $data = json_encode($data);
            $response = $this->send(self::apiUrl . $method, $data);
        } else $response = $this->send(self::apiUrl . $method . '?token=' . $this->token);
        //echo $method."\n";
        //echo $data."\n";
        //die(var_dump($response));
        /*$log = $response . "\n";
        file_put_contents('/var/www/insta/data/www/turbo-inst.ru/log.txt', $log, FILE_APPEND);*/
        $response = json_decode($response, 1);

        if (isset($response['error'])) {
            //$this->logging('Ошибка Like4u: '.$this->error[$response['error']]);
            return false;
        }
        return $response;
    }

    /*public function getStatistics()

    {
        $tasks = TaskRun::find()->joinWith(['service' => function ($service) {
            $service->joinWith('class');
        }])
            ->where(['not in', 'current_status',
                [
                    StatusHelper::FINISHED,
                    StatusHelper::ERROR,
                    StatusHelper::DECLARED,
                    StatusHelper::MODERATED,
                    StatusHelper::BANNED,
                    StatusHelper::CANCELED,
                    StatusHelper::AT_HANDLE_WORK
                ]
            ])
            ->andWhere('task_id <> :id', [':id' => 0])
            ->all();


        for ($i = 0; $i < count($tasks); $i++) {
            $this->task = $tasks[$i];
            $statistics = $this->apiRequest('instagram/tasks/' . $tasks[$i]->task_id . '.json');

            $task_info = json_decode($tasks[$i]->task_info, 1);
            $fields = json_decode($tasks[$i]->fields, 1);
            $totalCount = (int)$fields['count'];

            if ($statistics == false) {
                $this->logging($this->task->id." - statistic false");
                $instagram_helper = new InstagramHelper();
                $instagram_end = $instagram_helper->getCount($fields['field']);
                if (($instagram_end - $task_info['order_begin']) >= $totalCount) {
                    $task_info['order_end'] = $instagram_end;
                    $this->setStatus(StatusHelper::FINISHED);
                } else $this->setStatus(StatusHelper::AT_WORK);
                $totalLike = $totalCount;
                $complete = $instagram_end - $task_info['order_begin'];
            } else {
                $this->logging($this->task->id." - statistic true");
                $totalLike = (int)$task_info['max_count'];
                $complete = $totalCount - ($statistics['max_count'] - $statistics['current_count']);
                $this->logging($statistics['max_count'] . " && " . $statistics['current_count']. " total = ".$totalCount." complete = ".$complete);
                if ($statistics['paused'])
                    $this->setStatus(StatusHelper::PAUSED);
                elseif (($statistics['finished']) || ($totalCount <= $complete)) {
                    $instagram_helper = new InstagramHelper();
                    $task_info['order_end'] = $instagram_helper->getCount($fields['field']);
                    $this->setStatus(StatusHelper::FINISHED);
                } elseif ($statistics['suspended'])
                    $this->setStatus(StatusHelper::FREEZED);
                else
                    $this->setStatus(StatusHelper::AT_WORK);

            }
            $task_info['complete'] = $complete;
            $task_info['max_count'] = $totalLike;

            $tasks[$i]->task_info =json_encode($task_info);

            $tasks[$i]->save();
        }
    }*/

    public function send($url, $param = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($url));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json;'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36');

        if (isset($param))
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);

        $result = curl_exec($ch);

        $body = substr($result, curl_getinfo($ch, CURLINFO_HEADER_SIZE));
        curl_close($ch);

        return $body;
    }

    public function getStatistics()

    {
        $tasks = TaskRun::find()->joinWith(['service' => function ($service) {
            $service->joinWith('class');
        }])
            ->where(['not in', 'current_status',
                [
                    StatusHelper::FINISHED,
                    StatusHelper::ERROR,
                    StatusHelper::DECLARED,
                    StatusHelper::MODERATED,
                    StatusHelper::BANNED,
                    StatusHelper::CANCELED,
                    StatusHelper::AT_HANDLE_WORK
                ]
            ])
            ->andWhere('task_id <> :id', [':id' => 0])
            ->all();


        for ($i = 0; $i < count($tasks); $i++) {
            echo "1";
            $this->task = $tasks[$i];
            echo "2";
            $task_info = json_decode($tasks[$i]->task_info, 1);
            echo "3";
            $fields = json_decode($tasks[$i]->fields, 1);
            echo "4";
            $instagram_helper = new InstagramHelper;
            echo "5";
            $current_order = $instagram_helper->getCount($fields['field']);
            echo "6";
            if (!isset($task_info['order_begin'])) {
                echo "7";
                $task_info['order_begin'] = 0;
                echo "8";
                $task_info['order_end'] = $fields['count'];
                echo "9";
            }
            echo "10";
            $complete = $current_order - $task_info['order_begin'];
            echo "11";
            if($complete >= $fields['count'] ){
                echo "12";
                $this->setStatus(StatusHelper::FINISHED);
                echo "13";
                $task_info['order_end'] = $current_order;
                echo "14";
            }
            else{
                $this->setStatus(StatusHelper::AT_WORK);
                echo "15";
            }
            $task_info['complete'] = $complete;
            echo "16";
            $tasks[$i]->task_info = json_encode($task_info);
            echo "17";
            $tasks[$i]->save();
            echo "18";
        }
    }
    public function logging($text)
    {
        if (self::logging)
            echo $text . "\n";
    }
}