<?php
/**
 * Created by PhpStorm.
 * User: админ
 * Date: 15.12.2015
 * Time: 13:22
 */

namespace common\models\servicemodels;

use common\helpers\StatusHelper;
use Yii;
use yii\base\Object;
use common\models\RunModel;
use common\models\BillingAccount;

class BaseRunClass extends Object
{
    public $task;

    public function runMethod($num, $task)
    {

        if (method_exists($this, $num)) {

            $this->task = $task;

            if ($this->$num()) {

                return true; // Выполнился
            } else {
                return false; // Не выполнился
            }
        }
    }

    public function setStatus($status, $task = null)
    {
        if ($task == null) {
            $this->task->current_status = $status;
            $this->task->save();
        } else {
            $task->current_status = $status;
            $task->save();
        }
    }

    public function doTask()
    {
        $class = $this->task->service->class->runClass;
        $runMethod = $this->task->service->method->method;
        $result = $class->runMethod($runMethod, $this->task);
        if ($result != "emptydata") {
            if ($result) {
                $this->setStatus(StatusHelper::AT_WORK);
                $this->task->last_run = date("Y-m-d H:i:s", strtotime($this->task->last_run) + 3600);
                $this->task->save();
            } else {
                $this->task->error_count = $this->task->error_count++;
                $this->setStatus(StatusHelper::ERROR);
            }
        } else {
            $this->task->last_run = date("Y-m-d H:i:s", strtotime($this->task->last_run) + 3600);
            $this->task->save();
        }
    }

    public function doSubscribeTask($url, $service_id)
    {
        $fields = json_decode($this->task->fields);

        $fields->service_id = $service_id;
        $fields->field = $url;
        $fields->subscribe_flag = 1;
        $fields->user_id = $this->task->user_id;

        $runModel = new RunModel(['service_id' => $service_id]);
        $runModel = $runModel->loadAttributes(json_encode($fields));

        $account = BillingAccount::find()->where("owner_id=" . $this->task->user_id)->one();
        if ($account->accountBalance < $runModel->totalPrice) {
            $this->setStatus(StatusHelper::ERROR);
            return false;
        }

        $runModel->save();
        return true;
    }
}