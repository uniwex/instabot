<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 21.01.2016
 * Time: 16:26
 */

namespace common\models\servicemodels;


use common\models\Settings;
use common\models\User;

class Like4uInsta extends Like4u
{
    public
        $key, // Ключ массива, зависит от типа задачи
        $params = [],
        $cost = [
            'likes' => '1',
            'followers' => '3'
        ];

    /**
     * Создает задачу
     * @param string $method
     * @return bool|mixed
     */
    public function run($method)
    {

        $this->getParams();
        $response = $this->apiRequest('likes/instagram/' . $method, $this->params);

      /*  $log = 'Response: ' . var_export($response, 1) . "\n";
        $log .= var_export($this->params, 1) . "\n";*/
        if(isset($response['max_count'])) {
            $statistics = json_decode($this->task->task_info, 1);
            $statistics['max_count'] = (int) $response['max_count'] - (int) $response['current_count'];
            $this->task->task_info = json_encode($statistics);
            $this->task->task_id = (int) $response['id'];
            $this->task->save();
           /* if()
                $log .= 'Save Task Run' . "\n";
            file_put_contents('/var/www/insta/data/www/turbo-inst.ru/log.txt', $log, FILE_APPEND);*/
            return true;
        }
        else {
           /* $log .= 'No max_count' . "\n";
            file_put_contents('/var/www/insta/data/www/turbo-inst.ru/log.txt', $log, FILE_APPEND);*/
            return false;
        }
    }

    public function getParams()
    {
        /**
         * @var $user \common\models\User
         * @var $settings \common\models\Settings
         */

        $settings = Settings::findOne($this->task->service_id);
        //$user = User::findOne($this->task->user_id);

        $fields = json_decode($this->task->fields);

       /* $params2 = [
            $this->key => [
                'members_count' => $fields->count,
                'task_limit_attributes' => [
                    'min_followers' => '1',
                    'min_media' => '1',
                    'has_avatar' => '1',
                ]
            ]
        ];*/

      /*  if($settings && $user) {
            /*if($user->is_premium)
                $params2 = [
                    $this->key => [
                        'members_count' => floor($fields->count + ($fields->count * ($settings->premium_bonus / 100))),
                        'task_limit_attributes' => [
                            'min_followers' => "$settings->premium_followers",
                            'min_media' => "$settings->premium_photo",
                            'has_avatar' => '1',
                        ]
                    ]
                ];
            else*/
             /*   $params2 = [
                    $this->key => [

                        'task_limit_attributes' => [

                        ]
                    ]
                ];
        }*/

        $params = [
            $this->key => [
                'title' => 'TurboInst',
                'tag_list' => 'СЕРВИС',
                'url' => $fields->field,
                'members_count' => floor($fields->count + ($fields->count * ($settings->default_bonus / 100))),
                'task_limit_attributes' => [
                    'minute_1' => '',
                    'minutes_5' => $this->task->limit5 ? $this->task->limit5."" : '',
                    'hour_1' => $this->task->limit1 ? $this->task->limit1."" : '',
                    'hours_4' => '',
                    'day_1' => '',
                ],
                //'min_followers' => "$settings->premium_followers",
                //'min_media' => "$settings->premium_photo",
                'has_avatar' => '1',
            ]
        ];
        if ($settings->premium_followers != 0)
            $params[$this->key]['min_followers'] = $settings->premium_followers;
        if ($settings->premium_photo != 0)
            $params[$this->key]['min_media'] = $settings->premium_photo;


        $this->params =  array_merge_recursive($this->params,$params);
    }

    public function likes()
    {
        $method = 'likes.json';
        $this->key = 'ig_like_task';
        $this->params[$this->key]['cost'] = $this->cost['likes'];
        return $this->run($method);
    }

    public function followers()
    {

        $method = 'follows.json';
        $this->key = 'ig_follow_task';
        $this->params[$this->key]['cost'] = $this->cost['followers'];
        return $this->run($method);
    }
}