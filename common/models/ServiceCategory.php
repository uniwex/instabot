<?php

namespace common\models;

use common\components\UserClosureTable;
use common\behaviors\SlugBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "service_category".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $description_ru
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property integer $sort
 * @property integer $active
 * @property string $image
 * @property string $slug
 *
 * @property integer $new_owner
 * @property Service[] $services
 */
class ServiceCategory extends ActiveRecord
{

    CONST CATEGORY_UPDATE = '/admin/index.php?r=service-category/update-tree', // Редактирование категории
          CATEGORY_DELETE = '/admin/index.php?r=service-category/delete-tree', // Удаление категории
          TREE_EDIT = '/admin/index.php?r=service-category/edit-tree'; // Редактирование дерева

    //public $leaf;
    public $new_owner = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_category';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SlugBehavior::className(),
                'attribute' => 'title_ru',
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => UserClosureTable::className(),
                'tableName' => 'service_category_tree'
            ],
            [
                'class' => '\common\behaviors\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'height' => 300],
                ],
                'filePath' => '@webroot/uploads/service_category/images/[[pk]].[[extension]]',
                'fileUrl' => '/uploads/service_category/images/[[pk]].[[extension]]',
                'thumbPath' => '@webroot/uploads/service_category/images/[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/uploads/service_category/images/[[profile]]_[[pk]].[[extension]]',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['description_ru'], 'string'],
            [['created_by', 'updated_by', 'sort', 'active'], 'integer'],
            [['created_at', 'updated_at', 'new_owner'], 'safe'],
            [['title_ru', 'slug'], 'string', 'max' => 255],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png', 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'description_ru' => 'Description Ru',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sort' => 'Sort',
            'active' => 'Active',
            'image' => 'Картинка',
            'slug' => 'slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['parent_id' => 'id']);
    }

    public static function find()
    {
        return new ServiceCategoryQuery(static::className());
    }
}
