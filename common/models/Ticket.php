<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property integer $active
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property TicketMessage[] $ticketMessages
 *
 * @property string $message
 */
class Ticket extends ActiveRecord
{
    public $message;

    const STATUS_WAITING = 1;
    const STATUS_ANSWERED = 2;
    const STATUS_READ = 3;
    const STATUS_CLOSED = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'message'], 'required'],
            [['created_by', 'updated_by', 'active', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 512]
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => function(){
                    return Date('Y-m-d H:i:s',time()-3600);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'title' => 'Тема',
            'status' => 'Статус',
            'created_by' => 'Автор',
            'updated_by' => 'Изменил',
            'created_at' => 'Время создания',
            'updated_at' => 'Время изменения',
            'active' => 'Активно',
            'message' => 'Сообщение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id']);
    }

    public static function find()
    {
        return new TicketQuery(get_called_class());
    }

    public function createMessage(){
        $modelMessage = new TicketMessage(
            [
                'ticket_id' => $this->id,
                'message' => $this->message,
            ]
        );
        $modelMessage->save();
    }
}
