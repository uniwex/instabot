<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "service_queue".
 *
 * @property integer $id
 * @property string $type
 * @property integer $service_id
 */
class ServiceQueue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id'], 'integer'],
            [['type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'service_id' => 'Service ID',
        ];
    }
}
