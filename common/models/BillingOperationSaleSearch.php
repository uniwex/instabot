<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * BillingOperationSearch represents the model behind the search form about `common\models\BillingOperation`.
 */
class BillingOperationSaleSearch extends Model
{
    public $account_id;
    public $username;

    public $id;
    public $operation_sum;
    public $operation_time;
    public $notes;

    public $date_from;
    public $date_to;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'account_id'], 'integer'],
            [['operation_sum'], 'number'],
            [['username', 'operation_time', 'notes', 'account_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BillingOperationSale::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['operation_time' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['account' => function (ActiveQuery $q) {
            $q->joinWith(['owner']);
        }]);

        $query->andFilterWhere([
            'id' => $this->id,
            'user.id' => $this->username,
            'account_id' => $this->account_id,
            'operation_sum' => $this->operation_sum
        ]);


        $query->andFilterWhere(['like', 'notes', $this->notes]);

        if (!empty($this->operation_time)) {
            $date_from = trim(explode('/', $this->operation_time)[0]);
            $date_to = trim(explode('/', $this->operation_time)[1]);
            $query->andFilterWhere(['>=', 'operation_time', $date_from ? date('Y-m-d H:i:s', strtotime($date_from . ' 00:00:00')) : null])
                ->andFilterWhere(['<=', 'operation_time', $date_to ? date('Y-m-d H:i:s', strtotime($date_to . ' 23:59:59')) : null]);
        }

        return $dataProvider;
    }
}
