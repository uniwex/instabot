<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partner_config".
 *
 * @property integer $id
 * @property double $level1
 * @property double $level2
 * @property double $level3
 * @property double $level4
 * @property double $level5
 */
class PartnerConfig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner_config';
    }

    public function rules() {
        return [
            [['level1', 'level2', 'level3', 'level4', 'level5'], 'number', 'min' => 0]
        ];
    }

    public function attributeLabels() {
        return [
            'level1' => 'Процент отчислений 1 уровня',
            'level2' => 'Процент отчислений 2 уровня',
            'level3' => 'Процент отчислений 3 уровня',
            'level4' => 'Процент отчислений 4 уровня',
            'level5' => 'Процент отчислений 5 уровня'
        ];
    }

    /**
     * @inheritdoc
     * @return PartnerConfigQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartnerConfigQuery(get_called_class());
    }

    public function beforeSave() {
        for($i = 1; $i < 6; $i++) {
            if($this->{'level' . $i} == null) {
                $this->{'level' . $i} = 0;
            }
        }
        return true;
    }

    public static function checkRecord() {
        $model = PartnerConfig::find()->one();
        if($model != null)
            return $model;
        else
            return new PartnerConfig();
    }
}
