<?php

namespace common\models;

use valentinek\behaviors\ClosureTableQuery;
use Yii;
use yii\db\ActiveQuery;

class ServiceCategoryQuery extends ActiveQuery
{
    public function active($state = true)
    {
        return $this->andWhere(['active' => $state]);
    }

    public function slug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }
}
