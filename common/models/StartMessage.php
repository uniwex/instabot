<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "start_message".
 *
 * @property integer $id
 * @property string $tittle
 * @property string $text
 * @property string $action_button
 */
class StartMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'start_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'action_button'], 'string'],
            [['tittle'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tittle' => 'Заголовок модалки',
            'text' => 'Текст сообщения',
            'action_button' => 'Ссылка модалки',
        ];
    }
}
