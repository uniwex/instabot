<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "average_statistic".
 *
 * @property integer $id
 * @property integer $unique
 * @property integer $new_users
 * @property string $debit_users
 * @property string $debit_guest
 * @property string $total_debit
 * @property string $statistic_date
 */
class AverageStatistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'average_statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unique', 'new_users'], 'integer'],
            [['debit_users', 'debit_guest', 'total_debit'], 'number'],
            [['statistic_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unique' => 'Unique',
            'new_users' => 'New Users',
            'debit_users' => 'Debit Users',
            'debit_guest' => 'Debit Guest',
            'total_debit' => 'Total Debit',
            'statistic_date' => 'Statistic Date',
        ];
    }
}
