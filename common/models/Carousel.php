<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "carousel".
 *
 * @property integer $id
 * @property string $picture
 * @property string $title
 * @property string $button_text
 * @property string $button_class
 * @property string $button_href
 * @property integer $button_service
 * @property string $text
 * @property integer $sort
 */
class Carousel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'carousel';
    }

    public function behaviors()
    {
        return
            [
                [
                    'class' => '\common\behaviors\ImageUploadBehavior',
                    'attribute' => 'picture',
                    'filePath' => '@webroot/uploads/carousel/images/[[pk]].[[extension]]',
                    'fileUrl' => '/uploads/carousel/images/[[pk]].[[extension]]',
                ]
            ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['sort', 'button_service'], 'integer'],
            [['title', 'button_text', 'button_class', 'button_href'], 'string', 'max' => 255],
            ['picture', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
            ['actually','string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'picture' => 'Картинка',
            'title' => 'Заголовок',
            'button_text' => 'Текст кнопки',
            'button_class' => 'CSS класс',
            'button_href' => 'Ссылка на модальное окно',
            'button_service' => 'Услуга',
            'text' => 'Текст',
            'sort' => 'Сортировка',
            'actually' => 'Актуально до'
        ];
    }

    /**
     * @inheritdoc
     * @return CarouselQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CarouselQuery(get_called_class());
    }
}
