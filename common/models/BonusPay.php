<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bonus_pay".
 *
 * @property integer $id
 * @property string $sum
 * @property double $percent
 */
class BonusPay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus_pay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sum', 'percent'], 'required'],
            [['sum', 'percent'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Сумма пополнения',
            'percent' => 'Бонус к пополнению',
        ];
    }

    public static function getTotalSum($sum) {
        $model = BonusPay::find()->asArray()->all();
        for ($i = count($model) - 1; $i >= 0; $i--) {
            if ($model[$i]['sum'] > $sum) continue;
            else {
                $sum += $sum * $model[$i]['percent'] / 100;
                break;
            }
        }
        return $sum;
    }

    /**
     * @inheritdoc
     * @return BonusPayQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BonusPayQuery(get_called_class());
    }
}
