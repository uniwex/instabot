<?php
/**
 * Created by PhpStorm.
 * User: димон
 * Date: 24.01.2016
 * Time: 13:22
 */

namespace common\models;

use common\helpers\SendHelper;
use console\crawlers\VkCrawler;

class CheckVkGroup
{
    public $helper = null;
    public $login = "79250598812";
    public $password = "rOIzzSrb";
    public $token = "";
    public $user_id = "";
    public $groups_id = "tehosmotr_cherepovets35";
    public $fields = "city,country,place,description,wiki_page,members_count,counters,start_date,finish_date,can_post,can_see_all_posts,activity,status,contacts,links,fixed_post,verified,site,ban_info";
    public $model = null;


    private function parseUrl() {
        preg_match("#http\:\/\/vk\.com\/(.*)#",$this->model->field,$match);
        if (!isset($match[1])) return false;
        $this->groups_id = $match[1];
        return true;
    }

    private function loginVk(){
        $sh2 = $this->getSendHelper();
        $sh2->userAgent = 'Android-x86-1.6-r2 - Mozilla/5.0 (Linux; U; Android 1.6; en-us; eeepc Build/Donut) AppleWebKit/528.5+ (KHTML, like Gecko) Version/3.1.2 Mobile Safari/525.20.1';
        $url = "https://oauth.vk.com/token?grant_type=password&client_id=2274003&client_secret=hHbZxrka2uZ6jB1inYsH&username=$this->login&password=$this->password";
        $result = $sh2->request($url);

        $json = json_decode($result[1]);
        if (!isset($json->access_token)) {
            return false;
        }
        $this->token = $json->access_token;
        $this->user_id = $json->user_id;
        return true;
    }

    public function check() {
        if (!$this->loginVk()) {
            return json_encode(['result'=>'error: VK login']);
        }
        if (! $this->parseUrl()) {
            return json_encode(['result'=>'error: PARSE URL']);
        }
        $crawler = new VkCrawler();

        $crawler->setAccessToken($this->token);
        $crawler->setUserId($this->user_id);

        $data = $crawler->api('groups.getById', [
            'group_ids'=>$this->groups_id,
            'fields' => $this->fields,
        ]);
        if (!isset($data['response'])) return json_encode(['result'=>'error: NOT RESPONSE']);
        if (!isset($data['response'][0])) return json_encode(['result'=>'error: NOR RESPONSE[0]']);
        $groupInfo = $data['response'][0];
        $data = $crawler->api('wall.get',[
            'owner_id' => '-'.$groupInfo['gid'],
        ]);
        $count = $data['response'][0];

        if ($this->model->service->min_record_count > $count) return json_encode(['result'=>"error: В группе мало записей, минимум ".$this->model->service->min_record_count]);

        if ($this->model->service->min_members_count > $groupInfo['members_count']) return json_encode(['result'=>'error: В группе мало участников, минимум '.$this->model->service->min_members_count]);
        if ($this->model->service->stop_words == 1) {
            $description = $groupInfo['description'];
            $stop_words = StopWords::find()->all();
            foreach($stop_words as $word) {
                preg_match("#$word->word#",$description,$match);
                if (isset($match[1])) return json_encode(['result'=>"error: Ваша группа проверится модератором"]);
            }
        }
        return json_encode(['result'=>'ok']);
    }

    private function getSendHelper(){
        if ($this->helper == null) {
            $this->helper = new SendHelper();
            $this->helper->thisUser = "c".$this->login;
        }
        return $this->helper;
    }

}