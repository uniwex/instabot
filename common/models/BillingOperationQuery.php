<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[BillingOperation]].
 *
 * @see BillingOperation
 */
class BillingOperationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return BillingOperation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BillingOperation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}