<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "classes_method".
 *
 * @property integer $id
 * @property integer $id_class
 * @property string $title_ru
 * @property string $method
 *
 * @property Classes $idClass
 * @property FormCriteria[] $formCriterias
 * @property Service[] $services
 */
class ClassesMethod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'classes_method';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_class', 'title_ru'], 'required'],
            [['id_class'], 'integer'],
            [['title_ru', 'method'], 'string', 'max' => 255],
            [['id_class'], 'exist', 'skipOnError' => true, 'targetClass' => Classes::className(), 'targetAttribute' => ['id_class' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_class' => 'Класс',
            'title_ru' => 'Название метода',
            'method' => 'Программное название метода',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdClass()
    {
        return $this->hasOne(Classes::className(), ['id' => 'id_class']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormCriterias()
    {
        return $this->hasMany(FormCriteria::className(), ['id_method' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id_method' => 'id']);
    }
}
