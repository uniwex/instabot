<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "billing_currency".
 *
 * @property string $id
 * @property integer $amount_cnt
 * @property string $amount
 * @property string $current_base_rate
 * @property integer $base
 * @property string $created
 * @property string $updated
 * @property integer $created_by
 * @property integer $modified_by
 * @property integer $sort
 *
 * @property BillingAccountType[] $billingAccountTypes
 */
class BillingCurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['amount_cnt', 'base', 'created_by', 'modified_by', 'sort'], 'integer'],
            [['amount', 'current_base_rate'], 'number'],
            [['created', 'updated'], 'safe'],
            [['id'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount_cnt' => 'Amount Cnt',
            'amount' => 'Amount',
            'current_base_rate' => 'Current Base Rate',
            'base' => 'Base',
            'created' => 'Created',
            'updated' => 'Updated',
            'created_by' => 'Created By',
            'modified_by' => 'Modified By',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingAccountTypes()
    {
        return $this->hasMany(BillingAccountType::className(), ['currency_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return BillingOperationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BillingOperationQuery(get_called_class());
    }
}
