<?php
/**
 * Created by PhpStorm.
 * User: админ
 * Date: 15.12.2015
 * Time: 13:15
 */

namespace common\models;


interface RunClassInterface
{

    /**
     * @return mixed
     * start task
     */
    public function start($task);

    /**
     * @return mixed
     * stop task
     */
    public function stop($task);

    /**
     * @param $json
     * @return mixed
     */
    public function loadAttributes($json);

    /**
     * @return mixed
     */
    public function getFormPath();

    public function getService();

}