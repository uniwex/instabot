<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "main_page".
 *
 * @property integer $id
 * @property string $picture
 * @property string $button_text
 * @property string $button_class
 * @property string $text
 */
class MainPage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'main_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['picture', 'button_text', 'button_class'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'picture' => 'Picture',
            'button_text' => 'Button Text',
            'button_class' => 'Button Class',
            'text' => 'Text',
        ];
    }
}
