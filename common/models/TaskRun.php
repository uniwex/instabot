<?php
/**
 * Created by PhpStorm.
 * User: админ
 * Date: 15.12.2015
 * Time: 13:10
 */

namespace common\models;

/**
 * Class TaskRun
 * @package common\models
 */

use common\models\Service;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;

class TaskRun extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_run';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /*    'updatedAtAttribute' => 'updated_at',
               'createdAtAttribute' => 'created_at',*/
    //'value' => new Expression(['NOW()']),

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fields'], 'string'],
            ['email', 'email'],
            [['user_id', 'task_id', 'agregator_id', 'service_id', 'current_status', 'task_id', 'subscribe_flag', 'limit1', 'limit5'], 'integer'],
            [['run_time', 'task_info', 'subscribe_info'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => 'ID в Like4u',
            'user_id' => 'Пользователь',
            'service_id' => 'Услуга',
            'fields' => 'JSON дата',
            'run_time' => 'Время начала выполнения',
            'current_status' => 'Статус', // 10 = ручной
            'task_info' => 'Информация о задаче',
            'created_at' => 'Добавлена',
            'updated_at' => 'Изменена',
            'email' => 'Почта',
            'error_count' => 'Количество неудачных попыток',
            'subscribe_info' => 'Информация о подписке',
            'subscribe_flag' => 'Задача от подписки',
            'service_error' => 'Сервисы которые получили n ошибок',
            'agregator_id' => 'ID агрегатора',
            'limit1' => 'Лимит на 1 минуту',
            'limit5' => 'Лимит на 5 минут'
        ];
    }

    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}