<?php

namespace common\models;

use common\behaviors\SlugBehavior;
use Yii;
use yii\helpers\Html;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $slug
 * @property string $description_ru
 * @property string $content_ru
 * @property string $image
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property integer $active
 *
 * @property NewsCategoriesList[] $newsCategoriesLists
 * @property NewsCategories[] $categories
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class News extends \yii\db\ActiveRecord
{

    /**
     * Список категорий
     * @var array
     */
    public $category_list = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SlugBehavior::className(),
                'attribute' => 'title_ru',
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => '\common\behaviors\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'height' => 300],
                ],
                'filePath' => '@webroot/uploads/news/images/[[pk]].[[extension]]',
                'fileUrl' => '/uploads/news/images/[[pk]].[[extension]]',
                'thumbPath' => '@webroot/uploads/news/images/[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/uploads/news/images/[[profile]]_[[pk]].[[extension]]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['description_ru', 'content_ru'], 'string'],
            [['created_by', 'updated_by', 'active'], 'integer'],
            [['created_at', 'updated_at', 'category_list'], 'safe'],
            [['title_ru', 'slug'], 'string', 'max' => 255],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png','checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Название',
            'slug' => 'slug',
            'description_ru' => 'Краткая новость',
            'content_ru' => 'Полная новость',
            'image' => 'Картинка',
            'created_by' => 'Автор',
            'updated_by' => 'Редактор',
            'created_at' => 'Время создания',
            'updated_at' => 'Время изменения',
            'active' => 'Активность',
            'searchText' => 'Поиск по содержанию',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
/*    public function getNewsCategoriesLists()
    {
        return $this->hasMany(NewsCategoriesList::className(), ['news_id' => 'id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this
            ->hasMany(NewsCategories::className(), ['id' => 'category_id'])
            ->viaTable('news_categories_list', ['news_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // сбрасываем список категорий
        (new Query())
            ->createCommand()
            ->delete('{{%news_categories_list}}', 'news_id=:id', [':id' => $this->id])
            ->execute();

        // сохраняем список категорий
        if ($this->category_list) {

            foreach ($this->category_list as $k => $v) {
                $model = NewsCategories::find()->where('id=:id', [':id' => $v])->one();
                if ($model)
                    $this->link('categories', $model);
            }
        }
        // Yii::$app->cache->delete($this->getCacheKey());
    }

    public function getTagsNews() {
        $tags = $this->categories;
        for($i = 0; $i < count($tags); $i++) {
            if($i == 0) echo Html::tag('i', '', ['class' => 'icon-tags']);
            echo Html::a($tags[$i]->title_ru, ['news/index', 'NewsSearch[tags]' => $tags[$i]->id]);
            if($i < count($tags) - 1) echo ', ';
        }
    }

    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

}
