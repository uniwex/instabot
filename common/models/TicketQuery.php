<?php

namespace common\models;

use yii\db\ActiveQuery;

class TicketQuery extends ActiveQuery
{

    public function owner($ownerId)
    {
        return $this->andWhere(['created_by' => $ownerId]);
    }

    public function active($state = true)
    {
        return $this->andWhere(['active' => $state]);
    }
}
