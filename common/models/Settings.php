<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $service_id
 * @property double $default_bonus
 * @property double $premium_bonus
 * @property integer $premium_photo
 * @property integer $premium_followers
 *
 * @property Service $service
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id', 'default_bonus', 'premium_bonus', 'premium_photo', 'premium_followers'], 'required'],
            [['service_id', 'default_bonus', 'premium_bonus'], 'number'],
            [['service_id', 'premium_photo', 'premium_followers'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Услуга',
            'default_bonus' => 'Бонус для услуг',
            //'premium_bonus' => 'Бонус для премиум',
            'premium_photo' => 'Минимум фото',
            'premium_followers' => 'Минимум подписчиков',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }
}
