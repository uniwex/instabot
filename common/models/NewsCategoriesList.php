<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_categories_list".
 *
 * @property integer $news_id
 * @property integer $category_id
 *
 * @property NewsCategories $category
 * @property News $news
 */
class NewsCategoriesList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_categories_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'category_id'], 'required'],
            [['news_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'news_id' => 'News ID',
            'category_id' => 'Category ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(NewsCategories::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }

    /**
     * @inheritdoc
     * @return NewsCategoriesListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsCategoriesListQuery(get_called_class());
    }
}
