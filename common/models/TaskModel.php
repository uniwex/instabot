<?php
/**
 * Created by PhpStorm.
 * User: админ
 * Date: 15.12.2015
 * Time: 13:22
 */

namespace common\models;

use yii\base\Model;
use yii\helpers\Json;
use Yii;

class TaskModel extends Model
{
    public $run_time;
    public $service_id;

    public function save()
    {

        $task = TaskRun::findOne(['user_id' => Yii::$app->user->getId(), 'service_id' => $this->service_id]);
        $fields = Json::encode($this->attributes);
        if (isset($task)) {
            $task->run_time = Date('Y-m-d H:i',strtotime($this->run_time));
            $task->current_status = "Остановлена";
            $task->fields = $fields;
            $task->user_id = Yii::$app->user->getId();
        } else {
            $task = new TaskRun([
                'user_id' => Yii::$app->user->getId(),
                'service_id' => $this->service_id,
                'run_time' => Date('Y-m-d H:i',strtotime($this->run_time)),
                'current_status' => "Остановлена",
                'fields' => $fields
            ]);
        }

        if ($task->save()) return "YES";
        else return $task->getErrors();

    }

}