<?php

namespace common\models;

use yii\db\ActiveQuery;

class TicketMessageQuery extends ActiveQuery
{
    public function ticket($ticketId)
    {
        return $this->andWhere(['ticket_id' => $ticketId]);
    }

    public function owner($ownerId)
    {
        return $this->andWhere(['created_by' => $ownerId]);
    }

    public function active($state = true)
    {
        return $this->andWhere(['active' => $state]);
    }
}
