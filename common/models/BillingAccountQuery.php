<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[BillingAccount]].
 *
 * @see BillingAccount
 */
class BillingAccountQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return BillingAccount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BillingAccount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}