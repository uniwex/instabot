<?php

namespace common\models;

use common\behaviors\SettingsBehavior;
use common\models\RunModel;
use yii\db\Query;
use common\behaviors\SlugBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\models\Classes;
use common\models\ClassesMethod;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $title_ru
 * @property integer $parent_id
 * @property string $description_ru
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property integer $sort
 * @property integer $active
 * @property string $price
 * @property integer $min_count
 * @property string $image
 * @property string $slug
 * @property boolean $is_hit
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }


    public function behaviors()
    {
        return [
            [
                'class' => SettingsBehavior::className(),
            ],
            [
                'class' => SlugBehavior::className(),
                'attribute' => 'title_ru',
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => '\common\behaviors\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'height' => 300],
                ],
                'filePath' => '@webroot/uploads/service/images/[[pk]].[[extension]]',
                'fileUrl' => '/uploads/service/images/[[pk]].[[extension]]',
                'thumbPath' => '@webroot/uploads/service/images/[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/uploads/service/images/[[profile]]_[[pk]].[[exte/uploadsnsion]]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru', 'price', 'min_count', 'id_class'], 'required'],
            [['parent_id', 'created_by', 'updated_by', 'sort', 'active', 'min_count', 'type_debit', 'id_class',
                'id_method', 'id_news', 'min_members_count', 'min_record_count', 'stop_words',
                'sup_field', 'subscribe', 'only_logged', 'type_subscribe', 'is_hit'], 'integer'],
            [['description_ru', 'regular_for_field', 'field_name'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['price', 'criteria_price', 'price_unreg'], 'number'],
            [['title_ru', 'slug'], 'string', 'max' => 255],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png', 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Название',
            'parent_id' => 'Категория',
            'description_ru' => 'Описание услуги',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'sort' => 'Соритровка',
            'active' => 'Активна',
            'price' => 'Цена за 1',
            'min_count' => 'Минимальное количество для заказа',
            'image' => 'Изображение услуги',
            'slug' => 'Slug',
            'id_method' => 'Ид метода',
            'id_class' => 'Ид класса',
            'type_debit' => 'Тип списания',
            'regular_for_field' => 'Регулярка для поля',
            'criteria_price' => 'Цена одной критерии',
            'field_name' => 'Имя поля',
            'id_news' => 'Привязка новости',
            'price_unreg' => 'Цена для незарегестрированных',
            'min_members_count' => 'Минимальное количество участников(для групп)',
            'min_record_count' => 'Минимальное количество записей(для групп)',
            'stop_words' => 'Проверка групп на стоп слова',
            'sup_field' => 'Дополнительное поле',
            'subscribe' => 'Услуга подписка',
            'only_logged' => 'Только для зарегестрированных',
            'type_subscribe' => 'Как часто выполнять действие(Работает с подпиской)',
            'is_hit' => 'Хит продаж'
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function getClass()
    {
        return $this->hasOne(Classes::className(), ['id' => 'id_class']);
    }

    public function getMethod()
    {
        return $this->hasOne(ClassesMethod::className(), ['id' => 'id_method']);
    }

    public function returnModel()
    {
        return new RunModel(['service_id' => $this->id]);
    }

}
