<?php

namespace common\models;

use yii\db\ActiveQuery;

class NewsQuery extends ActiveQuery
{
    public function active($state = true)
    {
        return $this->andWhere(['active' => $state]);
    }
}
