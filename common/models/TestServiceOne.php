<?php
/**
 * Created by PhpStorm.
 * User: админ
 * Date: 15.12.2015
 * Time: 13:22
 */

namespace common\models;

use Yii;
use common\models\TaskModel;
use common\models\RunClassInterface;

class TestServiceOne extends TaskModel implements RunClassInterface
{
    const FORM_PATH = "//task_forms/_testserviceone";
    /**
     * @var $text string
     */
    public $text;

    public function rules()
    {
        return [
            [['text','run_time','service_id'],'required'],
            ['text', 'string', 'min' => 2, 'max' => 255],
            ['run_time','date'],
            ['service_id','integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'text'=>'Текст',
            'run_time'=>'Запустить',
            'service_id'=>'ID услуги'
        ];
    }

    public function loadAttributes($json) {
        return new TestServiceOne(json_decode($json,1));
    }

    public function start(){
        var_dump($this->attributes);
        //todo run CRON task
    }

    public function stop() {
        //todo stop CRON task
        echo "STOPPED";
    }

    /**
     * @return mixed
     */
    public function getFormPath()
    {
        return self::FORM_PATH;
    }

    public function getService(){
        $service = Service::findOne(['id'=>$this->service_id]);
        return $service;
    }

}