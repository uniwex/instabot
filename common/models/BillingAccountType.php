<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "billing_account_type".
 *
 * @property integer $id
 * @property string $currency_id
 * @property integer $base
 * @property integer $auto_creation
 * @property string $creation_time
 * @property integer $author_id
 * @property integer $sort
 * @property string $notes
 *
 * @property BillingAccount[] $billingAccounts
 * @property BillingCurrency $currency
 */
class BillingAccountType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_account_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency_id'], 'required'],
            [['base', 'auto_creation', 'author_id', 'sort'], 'integer'],
            [['creation_time'], 'safe'],
            [['notes'], 'string'],
            [['currency_id'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'currency_id' => 'Currency ID',
            'base' => 'Base',
            'auto_creation' => 'Auto Creation',
            'creation_time' => 'Creation Time',
            'author_id' => 'Author ID',
            'sort' => 'Sort',
            'notes' => 'Notes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingAccounts()
    {
        return $this->hasMany(BillingAccount::className(), ['account_type' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(BillingCurrency::className(), ['id' => 'currency_id']);
    }

    /**
     * @inheritdoc
     * @return BillingOperationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BillingOperationQuery(get_called_class());
    }
}
