<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "billing_register_balance".
 *
 * @property integer $id
 * @property string $operation_type
 * @property integer $operation_id
 * @property integer $account_id
 * @property string $debit
 * @property string $credit
 * @property string $balance
 *
 * @property User $account
 * @property BillingOperation $operation
 */
class BillingRegisterBalance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_register_balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operation_type', 'operation_id', 'account_id'], 'required'],
            [['operation_id', 'account_id'], 'integer'],
            [['debit', 'credit', 'balance'], 'number'],
            [['operation_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'operation_type' => 'Operation Type',
            'operation_id' => 'Operation ID',
            'account_id' => 'Account ID',
            'debit' => 'Debit',
            'credit' => 'Credit',
            'balance' => 'Balance',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(User::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperation()
    {
        return $this->hasOne(BillingOperation::className(), ['id' => 'operation_id']);
    }

    /**
     * @inheritdoc
     * @return BillingRegisterBalanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BillingRegisterBalanceQuery(get_called_class());
    }
}
