<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * TaskRunSearch represents the model behind the search form about `common\models\TaskRun`.
 */
class TaskRunSearch extends TaskRun
{

    public $username = null;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'service_id', 'current_status', 'error_count', 'subscribe_flag', 'task_id'], 'integer'],
            [['username','fields', 'run_time', 'task_info', 'created_at', 'updated_at', 'email', 'last_run', 'subscribe_info', 'service_error'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskRun::find()->orderBy('id desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $id = '';
        if ($this->username != null) {
            $user = User::find()->where("username = '" . $this->username."'")->one();
            if (isset($user)) {
                $id = $user->id;
            } else {
                $id = 0;
            }
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'user_id' => $this->user_id,
            'service_id' => $this->service_id,
            'run_time' => $this->run_time,
            'current_status' => $this->current_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'last_run' => $this->last_run,
            'error_count' => $this->error_count,
            'subscribe_flag' => $this->subscribe_flag,
            'task_id' => $this->task_id,
            'user_id' => $id,
        ]);

        $query->andFilterWhere(['like', 'fields', $this->fields])
            ->andFilterWhere(['like', 'task_info', $this->task_info])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'subscribe_info', $this->subscribe_info])
            ->andFilterWhere(['like', 'service_error', $this->service_error]);

        return $dataProvider;
    }
}
