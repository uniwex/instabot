<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PartnerConfig]].
 *
 * @see PartnerConfig
 */
class PartnerConfigQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PartnerConfig[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PartnerConfig|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}