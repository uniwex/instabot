<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "criteria".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $image
 * @property integer $type
 *
 * @property FormCriteria[] $formCriterias
 */
class Criteria extends \yii\db\ActiveRecord
{

    const SIMPLE_TEXT = 1;
    const SEX = 2;
    const COUNTRY = 3;
    const CITY = 4;
    const AGE = 5;

    public static function All_Criterias(){
        return [
            self::SIMPLE_TEXT => 'Обычный текст',
            self::SEX => 'Пол',
            self::COUNTRY => 'Страна',
            self::CITY => 'Город',
            self::AGE => 'Возраст'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'criteria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['type'], 'integer'],
            [['title_ru', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Title Ru',
            'image' => 'Image',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormCriterias()
    {
        return $this->hasMany(FormCriteria::className(), ['id_criteria' => 'id']);
    }

    public static function getOptionArray($type) {
        if ($type == self::SEX) {
            return [
                0 => 'Не выбран',
                1 => 'Женский',
                2 => 'Мужской'
            ];
        }
        if ($type == self::COUNTRY) {
            return [
                '' => 'Не выбран',
                1 => 'Россия',
                2 => 'Украина',
                3 => 'Беларусь'];
        }
        if ($type == self::CITY) {
            return [
                0 => [
                ],
                1 => [
                    1 => 'Москва',
                    2 => 'Санкт-Петербург'
                ],
                2 => [
                    314 => 'Киев'
                ],
                3 => [
                    282 => 'Минск'
                ]
            ];
        }
        if ($type == self::AGE) {
            return [
                '' => 'Не выбран',
                1 => 'Старше 18 лет',
                2 => 'Младше 18 лет'
            ];
        }
    }
}
