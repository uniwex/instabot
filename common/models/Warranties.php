<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "warranties".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $description_ru
 * @property string $button_link
 * @property string $image
 * @property integer $sort
 * @property integer $active
 */
class Warranties extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'warranties';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\common\behaviors\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'height' => 300],
                ],
                'filePath' => '@webroot/uploads/warranties/images/[[pk]].[[extension]]',
                'fileUrl' => '/uploads/warranties/images/[[pk]].[[extension]]',
                'thumbPath' => '@webroot/uploads/warranties/images/[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/uploads/warranties/images/[[profile]]_[[pk]].[[exte/uploadsnsion]]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['description_ru'], 'string'],
            [['sort', 'active'], 'integer'],
            [['title_ru', 'button_link'], 'string', 'max' => 255],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png','checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Название',
            'description_ru' => 'Описание',
            'button_link' => 'Ссылка кнопки проверить',
            'image' => 'Картинка',
            'sort' => 'Сортировка',
            'active' => 'Активность',
        ];
    }
}
