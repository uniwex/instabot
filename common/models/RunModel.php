<?php
/**
 * Created by PhpStorm.
 * User: димон
 * Date: 17.01.2016
 * Time: 16:10
 */

namespace common\models;

use common\helpers\InstagramHelper;
use common\helpers\StatusHelper;
use Yii;
use yii\base\Model;
use yii\helpers\Json;
use console\controllers\PeriodController;

class RunModel extends Model
{
    const FORM_PATH = "//task_forms/_service_form";
    const HandleTask = 2;
    const AutoTask = 1;
    const Error = 0;

    public $summary_price;
    public $name;
    public $field;
    public $count;
    public $service_id;
    public $user_id = null;
    public $limit1;
    public $limit5;
    public $rules;
    public $order_id;

    public function rules()
    {
        //die(var_dump($this->service));
        return [

            [['field', 'count', 'rules'], 'required'],
            ['count', 'integer'/*, 'min' => $this->service->min_count*/],
            [['limit1', 'limit5', 'service_id'], 'integer'],
            ['field', 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'count' => 'Количество',
            'field' => 'Поле',
            'name' => 'Название',
            'limit1' => 'Лимит на 1 час',
            'limit5' => 'Лимит на 5 минут',
            'rules' => 'Я ознакомлен с правилами и FAQ'
        ];
    }

    public function getTotalPrice()
    {
        $price = Yii::$app->user->isGuest ? $this->service->price_unreg : $this->service->price;
        $total = $price * $this->count;
        return $total;
    }

    public function loadAttributes($json)
    {
        return new RunModel(json_decode($json, 1));
    }

    public function run($task)
    {
        $service = $this->getService();
        $runMethod = $service->method->method;
        $runClass = $service->class->runClass;
        $runClass->runMethod($runMethod, $task);
        $task->current_status = 1;
        $task->save();
    }

    public function getService()
    {
        return Service::findOne(['id' => $this->service_id]);
    }

    public function setUserId()
    {
        $this->user_id = Yii::$app->user->isGuest ? Yii::$app->params['guest_id'] : Yii::$app->user->id;
    }

    public function save()
    {
        /* $task = TaskRun::findOne(['user_id' => Yii::$app->user->getId(), 'service_id' => $this->service_id]);*/
        $this->summary_price = $this->totalPrice;
        $fields = Json::encode([
            'summary_price' => $this->summary_price,
            'field' => $this->field,
            'count' => $this->count
        ]);

     /*   if (isset($task)) {
            $task->current_status = 0;
            $task->fields = $fields;
        } else {*/
            $instagramHelper = new InstagramHelper;
            $task = new TaskRun([
                'order_id' => $this->order_id,
                'service_id' => $this->service_id,
                'current_status' => 0,
                'fields' => $fields,
                'task_info' => json_encode([
                    'complete' => 0,
                    'status' => 'default',
                    'order_begin' => $instagramHelper->getCount($this->field),
                    'order_end' => 'null',
                ]),
                'subscribe_info' => null,
                'limit1' => $this->limit1,
                'limit5' => $this->limit5
            ]);
/*        }*/

        $this->setUserId();
        $task->user_id = $this->user_id;

        if ($task->save()) {
            if ($task->service->class->id == 1) {
                $task->current_status = StatusHelper::AT_HANDLE_WORK;
                $task->save();
                return true;
                //todo Ручное выполнение
            } else {
                /*$class = $task->service->class->runClass;
                $runMethod = $task->service->method->method;
                if ($class->runMethod($runMethod, $task)) {
                    $task->current_status = StatusHelper::AT_WORK;
                    $task->save();
                } else {
                    $task->error_count = $task->error_count++;
                    $task->current_status = StatusHelper::ERROR;
                    $task->save();
                    //$task->delete();
                }*/
                //PeriodController::actionSupertest($task->id);
                exec("php -f /var/www/insta/data/www/turbo-inst.ru/yii period/supertest ".$task->id."");
                return true;
            }
        } else return self::Error;

    }

    public function saveUnreg() {
        $this->summary_price = $this->totalPrice;
        $fields = Json::encode([
            'summary_price' => $this->summary_price,
            'field' => $this->field,
            'count' => $this->count
        ]);

        if (isset($task)) {
            $task->current_status = 0;
            $task->fields = $fields;
        } else {
            $instagramHelper = new InstagramHelper;
            $task = new TaskRun([
                'order_id' => $this->order_id,
                'service_id' => $this->service_id,
                'current_status' => 0,
                'fields' => $fields,
                'task_info' => json_encode([
                    'complete' => 0,
                    'status' => 'default',
                    'order_begin' => $instagramHelper->getCount($this->field),
                    'order_end' => 'null',
                ]),
                'subscribe_info' => null,
                'limit1' => $this->limit1,
                'limit5' => $this->limit5
            ]);
        }

        $this->setUserId();
        $task->user_id = $this->user_id;

        if ($task->save()) {
            if ($task->service->class->id == 1) {
                $task->current_status = StatusHelper::AT_HANDLE_WORK;
                $task->save();
                return true;
                //todo Ручное выполнение
            } else {
                /*$class = $task->service->class->runClass;
                $runMethod = $task->service->method->method;
                if ($class->runMethod($runMethod, $task)) {
                    $task->current_status = StatusHelper::AT_WORK;
                    $task->save();
                } else {
                    $task->error_count = $task->error_count++;
                    $task->current_status = StatusHelper::ERROR;
                    $task->save();
                    //$task->delete();
                }*/
                //PeriodController::actionSupertest($task->id);
                //exec("php -f /var/www/insta/data/www/turbo-inst.ru/yii period/supertest ".$task->id."");
                return true;
            }
        } else return false;
    }

}