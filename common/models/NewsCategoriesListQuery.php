<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[NewsCategoriesList]].
 *
 * @see NewsCategoriesList
 */
class NewsCategoriesListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NewsCategoriesList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NewsCategoriesList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}