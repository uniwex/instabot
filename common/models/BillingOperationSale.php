<?php

namespace common\models;

use common\behaviors\partner\BonusProgram;
use Yii;
use common\behaviors\OperationBehavior;
/**
 * This is the model class for table "billing_operation_sale".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $operation_sum
 * @property string $operation_time
 * @property string $notes
 * @property string $email
 * @property string $token
 *
 * @property BillingAccount $account
 * @property OrderService[] $orderServices
 * @property Service[] $services
 */
class BillingOperationSale extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => OperationBehavior::className()
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_operation_sale';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id'], 'required'],
            [['account_id'], 'integer'],
            [['operation_sum'], 'number'],
            [['operation_time'], 'safe'],
            [['notes', 'email', 'token'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код операции',
            'account_id' => 'Счет',
            'operation_sum' => 'Сумма операции',
            'operation_time' => 'Дата операции',
            'notes' => 'Примечание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(BillingAccount::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderServices()
    {
        return $this->hasMany(OrderService::className(), ['operation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])->viaTable('order_service', ['operation_id' => 'id']);
    }

    public function getUsername() {
        return $this->account->owner->username;
    }

    public function getCurrencyId() {
        return $this->account->accountType->currency_id;
    }

    /**
     * @inheritdoc
     * @return BillingOperationSaleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BillingOperationSaleQuery(get_called_class());
    }

    public function setEmail($email) {
        $this->email = $email;
        $this->token = User::getPassword(8, true, true, false);
    }
}
