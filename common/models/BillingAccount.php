<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "billing_account".
 *
 * @property integer $id
 * @property integer $account_type
 * @property string $owner_type
 * @property integer $owner_id
 * @property string $current_budget
 * @property integer $locked
 * @property string $date_locked
 * @property integer $author_id
 * @property integer $sort
 * @property string $notes
 *
 * @property float $accountBalance
 * @property float $debit
 * @property float $credit
 *
 * @property BillingAccountType $accountType
 * @property User $owner
 */
class BillingAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_type', 'owner_type', 'owner_id', 'author_id'], 'required'],
            [['account_type', 'owner_id', 'locked', 'author_id', 'sort'], 'integer'],
            [['current_budget'], 'number'],
            [['date_locked'], 'safe'],
            [['notes'], 'string'],
            [['owner_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_type' => 'Account Type',
            'owner_type' => 'Owner Type',
            'owner_id' => 'Owner ID',
            'current_budget' => 'Current Budget',
            'locked' => 'Locked',
            'date_locked' => 'Date Locked',
            'author_id' => 'Author ID',
            'sort' => 'Sort',
            'notes' => 'Notes',
        ];
    }

    /**
     * @inheritdoc
     * @return BillingAccountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BillingAccountQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountType()
    {
        return $this->hasOne(BillingAccountType::className(), ['id' => 'account_type']);
    }


    public function getAccountBalance()
    {
        return (float) BillingRegisterBalance::find()->where(['account_id'=> $this->id])->sum('debit-credit');
    }

    public function getDebit()
    {
        return (float) BillingRegisterBalance::find()->where(['account_id'=> $this->id])->sum('debit');
    }

    public function getCredit()
    {
        return (float) BillingRegisterBalance::find()->where(['account_id'=> $this->id])->sum('credit');
    }

    public function getAccountId($id = null)
    {

        $model = BillingAccount::find()->where([
            'account_type' => Yii::$app->params['account_type'],
            'owner_id' => $id == null ? Yii::$app->user->id : $id
        ])->one();
        if ($model == null) return false;
        return $model->id;
    }

    public function getViewBalance() {
        $result = '';
        $balance = (string) round($this->accountBalance);
        if(strlen($balance) <= 5) {
            for($i = strlen($balance); $i < 5; $i++) {
                $result .= '0';
            }
            $result .= $balance;
        }
        elseif (strlen($balance) > 5)
            $result .= '99999';

        return $result;
    }
}
