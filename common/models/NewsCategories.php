<?php

namespace common\models;

use common\behaviors\SlugBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "news_categories".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $slug
 * @property string $description_ru
 * @property string $image
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property integer $active
 *
 * @property NewsCategoriesList[] $newsCategoriesLists
 * @property News[] $news
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class NewsCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_categories';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SlugBehavior::className(),
                'attribute' => 'title_ru',
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => '\common\behaviors\ImageUploadBehavior',
                'attribute' => 'image',
                'thumbs' => [
                    'thumb' => ['width' => 400, 'height' => 300],
                ],
                'filePath' => '@webroot/uploads/news/category/images/[[pk]].[[extension]]',
                'fileUrl' => '/uploads/news/category/images/[[pk]].[[extension]]',
                'thumbPath' => '@webroot/uploads/news/category/images/[[profile]]_[[pk]].[[extension]]',
                'thumbUrl' => '/uploads/news/category/images/[[profile]]_[[pk]].[[exte/uploadsnsion]]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_ru'], 'required'],
            [['description_ru'], 'string'],
            [['created_by', 'updated_by', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title_ru', 'slug'], 'string', 'max' => 255],
            ['image', 'file', 'extensions' => 'jpeg, jpg, gif, png','checkExtensionByMimeType'=>false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_ru' => 'Название',
            'slug' => 'slug',
            'description_ru' => 'Описание',
            'image' => 'Изображение',
            'created_by' => 'Автор',
            'updated_by' => 'Редактор',
            'created_at' => 'Время создания',
            'updated_at' => 'Время изменения',
            'active' => 'Активно',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
/*    public function getNewsCategoriesLists()
    {
        return $this->hasMany(NewsCategoriesList::className(), ['category_id' => 'id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::className(), ['id' => 'news_id'])->viaTable('news_categories_list', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
