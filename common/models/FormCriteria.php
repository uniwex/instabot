<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "form_criteria".
 *
 * @property integer $id
 * @property integer $id_method
 * @property integer $id_criteria
 *
 * @property ClassesMethod $idMethod
 * @property Criteria $idCriteria
 */
class FormCriteria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'form_criteria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_method', 'id_criteria'], 'integer'],
            [['id_method'], 'exist', 'skipOnError' => true, 'targetClass' => ClassesMethod::className(), 'targetAttribute' => ['id_method' => 'id']],
            [['id_criteria'], 'exist', 'skipOnError' => true, 'targetClass' => Criteria::className(), 'targetAttribute' => ['id_criteria' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_method' => 'Id Method',
            'id_criteria' => 'Id Criteria',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMethod()
    {
        return $this->hasOne(ClassesMethod::className(), ['id' => 'id_method']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCriteria()
    {
        return $this->hasOne(Criteria::className(), ['id' => 'id_criteria']);
    }
}
