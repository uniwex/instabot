<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[OrderService]].
 *
 * @see OrderService
 */
class OrderServiceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return OrderService[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrderService|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}