<?php

namespace common\models;

use common\behaviors\OperationBehavior;
use Yii;

/**
 * This is the model class for table "billing_operation".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string $operation_sum
 * @property string $operation_time
 * @property string $payment_system
 * @property integer $author_id
 * @property string $notes
 *
 * @property BillingAccount $account
 * @property array $paymentType
 * @property string $username
 * @property string $currencyId
 */
class BillingOperation extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => OperationBehavior::className()
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_operation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'payment_system'], 'required'],
            [['account_id', 'author_id','agregator_id', 'payment_system'], 'number', 'integerOnly' => true],
            [['operation_sum'], 'number'],
            [['operation_time'], 'safe'],
            [['notes'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код операции',
            'account_id' => 'Счет пользователя',
            'operation_sum' => 'Сумма операции',
            'operation_time' => 'Дата операции',
            'payment_system' => 'Способ оплаты',
            'author_id' => 'Автор операции',
            'notes' => 'Примечание',
            'agreagator_id' => 'ID агрегатора'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(BillingAccount::className(), ['id' => 'account_id']);
    }

    public function getUsername() {
        return $this->account->owner->username;
    }

    public function getCurrencyId() {
        return $this->account->accountType->currency_id;
    }

    /**
     * @inheritdoc
     * @return BillingOperationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BillingOperationQuery(get_called_class());
    }
}
