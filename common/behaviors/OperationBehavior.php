<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 13.01.2016
 * Time: 10:15
 */

namespace common\behaviors;

use common\models\BillingAccount;
use common\models\BillingRegisterBalance;
use common\models\BonusPay;
use common\models\OrderService;
use Yii;
use yii\base\Behavior;

class OperationBehavior extends Behavior
{
    private $ownerId;

    public $itemList;

    CONST TABLE_PAYMENT = 'common\models\BillingOperation';
    CONST TABLE_PURCHASE = 'common\models\BillingOperationSale';

    public function getOwnerId()
    {
        if (!$this->ownerId) {
            Yii::$app->user->id ?
                $this->setOwnerId(Yii::$app->user->id) :
                $this->setOwnerId(Yii::$app->params['guest_id']);
        }

        return $this->ownerId;
    }

    public function setOwnerId($id)
    {
        $this->ownerId = $id;
    }

    public function getAccountId()
    {
        $model = BillingAccount::find()->where([
            'account_type' => Yii::$app->params['account_type'],
            'owner_id' => $this->getOwnerId()
        ])->one();
        if ($model == null) return false;
        return $model->id;
    }

    public function saveOperation($isOrder = false)
    {
        $transaction = Yii::$app->db->beginTransaction();

        // Заполняем BillingOperation/BillingOperationSale
        $this->owner->operation_time = date('Y-m-d H:i:s');
        $this->owner->account_id == null ? $this->owner->account_id = $this->getAccountId() : null;

        // Получаем счет
        $account = BillingAccount::findOne($this->owner->account_id);

        if ($this->owner->className() == self::TABLE_PAYMENT) {
            $this->owner->author_id = Yii::$app->user->id ? Yii::$app->user->id : Yii::$app->params['guest_id'];
            $this->owner->notes = 'Пополнение баланса';
            $this->owner->operation_sum = BonusPay::getTotalSum($this->owner->operation_sum);
            if ($this->owner->payment_system == 4)
                $this->owner->notes = 'Бонус от Партнерской программы';
        }
        else $this->owner->notes = 'Оплата заказа';

        // Проверяем баланс счета пользователя
        if ($account->owner_id != Yii::$app->params['guest_id'] &&
            $account->accountBalance < $this->owner->operation_sum &&
            $this->owner->className() == self::TABLE_PURCHASE)
            return false;

        if ($this->owner->save()) {
            $register = new BillingRegisterBalance();
            $register->operation_type = $this->owner->className();
            $register->operation_id = $this->owner->id;
            $register->account_id = $this->owner->account_id;

            $register->operation_type == self::TABLE_PAYMENT ?
                $register->debit = $this->owner->operation_sum :
                $register->credit = $this->owner->operation_sum;

            $balance = $account->accountBalance;
            $balance += ($register->debit - $register->credit);
            $register->balance = (float)$balance;
            $register->save();
            if (is_array($this->itemList)) {
                foreach ($this->itemList as $key => $values) {

                    $order = new OrderService();
                    $order->operation_id = $this->owner->id;
                    $order->service_id = $values->service->id;
                    $order->save();
                    //$values->save();
                }
            }
            else {
                if (isset($this->itemList)) {
                    $order = new OrderService();
                    $order->operation_id = $this->owner->id;
                    $order->service_id = $this->itemList->service->id;
                    $order->save();
                    //$this->itemList->save();
                }
            }

            $transaction->commit();
            return true;
        } else $transaction->rollBack();

        return false;
    }
}