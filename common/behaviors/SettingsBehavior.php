<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 11.02.2016
 * Time: 11:33
 */

namespace common\behaviors;

use common\models\Settings;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class SettingsBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'addSettings',
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteSettings'
        ];
    }

    public function deleteSettings()
    {
        Settings::findOne($this->owner->id)->delete();
    }

    public function addSettings()
    {
        $model = new Settings();
        $model->service_id = $this->owner->id;
        $model->default_bonus = 0;
        $model->premium_bonus = 0;
        $model->premium_followers = 0;
        $model->premium_photo = 0;
        $model->save();
    }
}