<?php
/**
 * Created by PhpStorm.
 * User: p2p13_000
 * Date: 12.01.2016
 * Time: 13:53
 */

namespace common\behaviors;

use Yii;
use common\models\BillingAccount;
use common\models\User;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class BillingBehavior extends Behavior
{
    public $primaryKeyUser = 'id';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'addBilling'
        ];
    }

    public function addBilling() {
        $billing = new BillingAccount();
        $billing->account_type = Yii::$app->params['account_type'];
        $billing->owner_type = User::className();
        $billing->owner_id = $this->owner->{$this->primaryKeyUser};
        $billing->author_id = 0;
        $billing->save();
    }
}